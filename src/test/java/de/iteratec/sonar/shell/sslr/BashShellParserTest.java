/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.sslr;

import com.google.gson.JsonSyntaxException;
import de.iteratec.sonar.shell.util.FileImporter;
import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static de.iteratec.sonar.shell.util.ShellDialect.KSH;

/**
 * tests grammar for bash-specific (but
 * not exclusive) features. On one hand,
 * it tests the grammar for correctly
 * building asts as specified in
 * ShellGrammar. On the other hand, it
 * also tests if dialect detection works
 * as intended.
 *
 * @author <a href=
 * "mailto:Navid.Noorshams@iteratec.de">Navid
 * Noorshams</a>
 */
public class BashShellParserTest
        extends AbstractParserTester {

    @DisplayName("bash-compliant if then else employing structured words in its tests")
    @Test
    public void t1() {
        String code = "if [ \"$animal\" = \"penguin\" ]; then\r\n"
                + "    echo \"Hmmmmmm fish... Tux happy!\"\r\n"
                + "  fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCorrectCountOfToken(3, "DOUBLE_QUOTED_WORD",
                code);
    }

    @DisplayName("bash-compliant if then else employing double brackets")
    @Test
    public void t2() {
        String code = "if [[ \"$animal\" == \"penguin\" ]]; then\r\n"
                + "    echo \"Hmmmmmm fish... Tux happy!\"\r\n"
                + "  fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(3, "DOUBLE_QUOTED_WORD",
                code);
        assertExistenceOfToken("DOUBLE_BRACKET_TEST", code);
        assertAbsenceOfToken("SINGLE_BRACKET_TEST", code);

    }

    @DisplayName("bash-compliant function call employing keyword 'function'")
    @Test
    public void t3() throws IOException {
        String code = "function foo {\r\n" + "echo foo\r\n"
                + "}";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertCorrectCountOfToken(1, "SIMPLE_COMMAND",
                code);
        assertCodeIsOfDialect(code, ShellDialect.BASH,
                ShellDialect.KSH, ShellDialect.ZSH);
    }

    @DisplayName("bash-compliant substring of variable-substitution")
    @Test
    public void t4()
            throws JsonSyntaxException, IOException {
        String code = "echo ${swag:1:2}";
        assertExistenceOfToken("BRACED_PARAMETER", code);
        assertExistenceOfToken("SUBSTRING", code);
        assertCorrectCountOfToken(2, "COLON", code);
        assertCodeIsOfDialect(code, ShellDialect.BASH,
                ShellDialect.KSH, ShellDialect.ZSH);
        assertCodeIsNotOfDialect(code, ShellDialect.POSIX);

    }

    @DisplayName("select clause")
    @Test
    public void t5() throws Throwable {
        String code = "select shuttle in columbia discovery iss sojuz; "
                + "do\n"
                + "    echo \"$shuttle selected\"\n"
                + "done";
        assertExistenceOfToken("SELECT_CLAUSE", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCodeIsOfDialect(code, ShellDialect.BASH, KSH,
                ShellDialect.ZSH);
    }

    @DisplayName("double-bracket-test")
    @Test
    public void t6() throws Throwable {
        String code = "if [[ \"hello world\" == \"hello world\" ]]; then \n"
                + "echo 'hello world!' \n" + "fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("DOUBLE_BRACKET_TEST", code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(1, "SINGLE_QUOTED_WORD",
                code);
        assertCodeIsOfDialect(code, ShellDialect.BASH, KSH,
                ShellDialect.ZSH);
        assertCodeIsNotOfDialect(code, ShellDialect.POSIX);
    }

    @DisplayName("unary bracket test")
    @Test
    public void t7() {
        String code = "if [ -w blabla ]; then\r\n"
                + "    echo \"Hmmmmmm fish... Tux happy!\"\r\n"
                + "  fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
    }

    @DisplayName("unary bracket test employing single quotes")
    @Test
    public void t8() {
        String code = "if [ -w 'blabla' ]; then\r\n"
                + "    echo \"Hmmmmmm fish... Tux happy!\"\r\n"
                + "  fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
    }

    @DisplayName("binary bracket test with unary tests")
    @Test
    public void t9() {
        String code = "if [ -w blabla -o -a blablabla ]; then\r\n"
                + "    echo \"Hmmmmmm fish... Tux happy!\"\r\n"
                + "  fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
        assertExistenceOfToken(
                "SINGLE_BRACKET_OR_EXPRESSION", code);
        assertCorrectCountOfToken(2, "UNARY_TEST_OPERATOR",
                code);
    }

    @DisplayName("double-bracket-test using pattern operator")
    @Test
    public void t10() {
        String code = "if [[ \"hallo\" =~ $pattern ]]; then\r\n"
                + "echo \"home sweet home\" \r\n" + "fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("DOUBLE_BRACKET_TEST", code);
        assertExistenceOfToken(
                "DOUBLE_BRACKET_BINARY_OPERATION", code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
    }

    /*
     * the if-condition has no semantics,
     * don't mind the nonsense
     *
     */
    @DisplayName("double-bracket-test using parenthesized OR with AND attached to it")
    @Test
    public void t11() {
        String code = "if [[ ( -w \"hallo\" || -w $pattern ) && $xo ]]; then\r\n"
                + "echo \"home sweet home\" \r\n" + "fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("DOUBLE_BRACKET_TEST", code);
        assertExistenceOfToken(
                "DOUBLE_BRACKET_OR_EXPRESSION", code);
        assertExistenceOfToken(
                "DOUBLE_BRACKET_AND_EXPRESSION", code);
        assertCorrectOrderOfTokens(
                "DOUBLE_BRACKET_AND_EXPRESSION",
                "DOUBLE_BRACKET_OR_EXPRESSION", code);
        assertCorrectOrderOfTokens(
                "DOUBLE_BRACKET_AND_EXPRESSION",
                "DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION",
                code);
        assertCorrectOrderOfTokens(
                "DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION",
                "DOUBLE_BRACKET_OR_EXPRESSION", code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
    }

    @DisplayName("array using special char")
    @Test
    public void t12() throws IOException {
        String code = "${hello[@]}";
        assertExistenceOfToken("ARRAY", code);
        assertCodeIsOfDialect(code, ShellDialect.BASH,
                ShellDialect.TCSH, ShellDialect.KSH,
                ShellDialect.ZSH);
        assertCodeIsNotOfDialect(code, ShellDialect.POSIX);
    }

    @DisplayName("array slicing")
    @Test
    public void t13() {
        String code = "${d[3]:4:3}";
        assertExistenceOfToken("ARRAY_OPERATION", code);
    }

    @DisplayName("array using number as index")
    @Test
    public void t14() {
        String code = "${hello[3]}";
        assertExistenceOfToken("NUMERIC_ARRAY", code);
    }

    @DisplayName("associative array")
    @Test
    public void t15() {
        String code = "${hello[hi]}";
        assertExistenceOfToken("ASSOCIATIVE_ARRAY", code);
    }

    @DisplayName("array with hash")
    @Test
    public void t16() {
        String code = "${#d[3]}";
        assertExistenceOfToken("ARRAY_OPERATION", code);
        assertExistenceOfToken("HASH", code);
    }

    @DisplayName("array employing bang")
    @Test
    public void t17() {
        String code = "${!d[3]}";
        assertExistenceOfToken("ARRAY_OPERATION", code);
        assertExistenceOfToken("BANG", code);
    }

    @DisplayName("bashrc2 found under tldp.org")
    @Test
    public void t18() {
        String code = FileImporter
                .getFile("file/bashrc2.sh").getContent();
        assertOk(code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("simple command separated with enter and using a keyword in dollar operator")
    @Test
    public void t19() {
        String code = "find . -type f -name \"${2:-*}\" -print0 | \\\n"
                + "    xargs -0 egrep --color=always -sn ${case} \"$1\" 2>&- | more\n";
        assertCorrectCountOfToken(3, "SIMPLE_COMMAND",
                code);
    }

    @DisplayName("some command substitution from bashrc2")
    @Test
    public void t20() {
        String code = "$(my_ps| awk '!/awk/ && $0~pat { print $1 }' pat=${!#} )";
        // the command-substition is a simple
        // command as well.
        assertCorrectCountOfToken(3, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(1, "SINGLE_QUOTED_WORD",
                code);

    }

    @DisplayName("arithmetic expression using dollar expansions using arrays")
    @Test
    public void t21() {
        String code = "local nbstars=$(( 20 * ${info[1]} / ${info[0]} ))\n";
        assertCorrectCountOfToken(3, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(1,
                "PARENTHESIZED_PARAMETER", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);

    }

    // (j=0;j<20;j++)
    @DisplayName("simple C-style-for-loop")
    @Test
    public void t22() {
        String code = "for ((j=0;j<20;j++)); do\n"
                + "           if [ ${j} -lt ${nbstars} ]; then\n"
                + "               out=$out\"*\"\n"
                + "            else\n"
                + "               out=$out\"-\"\n"
                + "            fi\n" + "     done\n";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("LOOP_CONDITION_C_STYLE",
                code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
        assertCorrectCountOfToken(4, "DOLLAR_CONSTRUCT",
                code);
    }

    @DisplayName("arithmetic expression using dollar expansions using arrays")
    @Test
    public void t23() {
        String code = "function my_ip() # Get IP adress on ethernet.\n"
                + "{\n"
                + "    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)\n"
                + "    echo ${MY_IP:-\"Not connected\"}\n"
                + "}";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertCorrectCountOfToken(5, "SIMPLE_COMMAND",
                code);
        assertExistenceOfToken("SINGLE_QUOTE", code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
    }

    @DisplayName("arithmetic expression using dollar expansions using arrays")
    @Test
    public void t24() {
        String code = "function repeat()       # Repeat n times command.\n"
                + "{\n" + "local i max\n"
                + "max=$1; shift;\n"
                + "for (( i = 1 ; i <= max ; i++ )); do  # --> C-like syntax\n"
                + "eval \"$@\";\n" + "done\n" + "}";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertExistenceOfToken("LOOP_CONDITION_C_STYLE",
                code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
    }

    @DisplayName("001-inpath.sh")
    @Test
    public void t25() {
        String code = FileImporter
                .getFile("file/001-inpath.sh").getContent();
        assertCorrectOrderOfTopLevelToken(
                "FUNCTION_DEFINITION,NEWLINE_LIST,"
                        + "FUNCTION_DEFINITION,NEWLINE_LIST,"
                        + "IF_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,"
                        + "EOL,CASE_CLAUSE,NEWLINE_LIST,"
                        + "SIMPLE_COMMAND",
                code);
    }

    @DisplayName("002-validalnum.sh")
    @Test
    public void t26() {
        String code = FileImporter
                .getFile("file/002-validalnum.sh")
                .getContent();
        assertCorrectOrderOfTopLevelToken(
                "FUNCTION_DEFINITION,NEWLINE_LIST,"
                        + "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,"
                        + "NEWLINE_LIST,IF_CLAUSE,"
                        + "NEWLINE_LIST,SIMPLE_COMMAND",
                code);
    }

    @DisplayName("003-normdate.sh")
    @Test
    public void t27() {
        String code = FileImporter
                .getFile("file/003-normdate.sh")
                .getContent();
        assertCorrectOrderOfTopLevelToken(
                "FUNCTION_DEFINITION,NEWLINE_LIST,IF_CLAUSE,"
                        + "NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,"
                        + "IF_CLAUSE,NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,"
                        + "SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND",
                code);
    }

    @DisplayName("004-nicenumber.sh")
    @Test
    public void t28() {
        String code = FileImporter
                .getFile("file/004-nicenumber.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("005-validint.sh")
    @Test
    public void t29() {
        String code = FileImporter
                .getFile("file/005-validint.sh")
                .getContent();
        assertCorrectCountOfToken(7, "IF_CLAUSE", code);
        assertCorrectCountOfToken(7, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(1, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(6, "ASSIGNMENT_WORD",
                code);
    }

    @DisplayName("006-validfloat.sh")
    @Test
    public void t30() {
        String code = FileImporter
                .getFile("file/006-validfloat.sh")
                .getContent();
        assertCorrectCountOfToken(11, "IF_CLAUSE", code);
        assertCorrectCountOfToken(7, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(1, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
    }

    @DisplayName("007-valid-date.sh")
    @Test
    public void t31() {
        String code = FileImporter
                .getFile("file/007-valid-date.sh")
                .getContent();
        assertCorrectCountOfToken(2, "FUNCTION_DEFINITION",
                code);
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertCorrectCountOfToken(5, "ELSE_PART", code);
        assertCorrectCountOfToken(7, "IF_CLAUSE", code);
        assertCorrectCountOfToken(18, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(7,
                "PARENTHESIZED_PARAMETER", code);
        assertCorrectCountOfToken(33, "DOLLAR_CONSTRUCT",
                code);

    }

    @DisplayName("tab-space preceding comment instead of white-space doesn't affect comment removal")
    @Test
    public void t32() {
        String code = "normdate=\"./003-normdate.sh\"	 # hack";
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertAbsenceOfToken("HASH", code);
        assertAbsenceOfToken("CMD_WORD", code);
    }

    @DisplayName("008-echon.sh")
    @Test
    public void t33() {
        String code = FileImporter
                .getFile("file/008-echon.sh").getContent();
        assertExistenceOfToken("FUNCTION_DEFINITION", code, true);
        assertCorrectCountOfToken(6, "SIMPLE_COMMAND",
                code);
    }

    @DisplayName("010-filelock.sh")
    @Test
    public void t34() {
        String code = FileImporter
                .getFile("file/010-filelock.sh")
                .getContent();
        assertCorrectCountOfToken(6, "ASSIGNMENT_WORD",
                code, true);
        assertCorrectCountOfToken(1, "WHILE_CLAUSE", code);
        assertCorrectCountOfToken(1, "CASE_CLAUSE", code);
        assertScriptHasBeenFullyParsed(code);
    }

    // this case has been modified, the
    // value of variable "esc" has been
    // changed
    @DisplayName("012-library.sh")
    @Test
    public void t35() {
        String code = FileImporter
                .getFile("file/012-library.sh")
                .getContent();
        assertCorrectCountOfToken(135, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(30, "IF_CLAUSE", code);
        assertCorrectCountOfToken(12, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(2, "CASE_CLAUSE", code);
        assertCorrectCountOfToken(82, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(29, "SINGLE_BRACKET_TEST",
                code);
    }

    @DisplayName("013-guessword.sh")
    @Test
    public void t36() {
        String code = FileImporter
                .getFile("file/013-guessword.sh")
                .getContent();
        assertCorrectCountOfToken(60, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(15,
                "PARENTHESIZED_PARAMETER", code);
        assertCorrectCountOfToken(4, "IF_CLAUSE", code);
        assertCorrectCountOfToken(2, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(1, "CASE_CLAUSE", code);
        assertCorrectCountOfToken(22, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(5, "SINGLE_BRACKET_TEST",
                code);
    }

    @DisplayName("100-hangman.sh")
    @Test
    public void t37() {
        String code = FileImporter
                .getFile("file/100-hangman.sh")
                .getContent();
        assertCorrectCountOfToken(55, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(5,
                "PARENTHESIZED_PARAMETER", code);
        assertCorrectCountOfToken(5, "IF_CLAUSE", code);
        assertCorrectCountOfToken(16, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(13, "SINGLE_BRACKET_TEST",
                code);
    }

    @DisplayName("088-unpacker.sh")
    @Test
    public void t38() {
        String code = FileImporter
                .getFile("file/088-unpacker.sh")
                .getContent();
        assertCorrectCountOfToken(48, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(10,
                "PARENTHESIZED_PARAMETER", code);
        assertCorrectCountOfToken(5, "IF_CLAUSE", code);
        assertCorrectCountOfToken(10, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(5, "SINGLE_BRACKET_TEST",
                code);
    }

    @DisplayName("command substitution in 088-unpacker.sh")
    @Test
    public void t39() {
        // TODO white-spaces in the awk-command
        // are being retained as white-spaces
        // instead of delimiters
        // How to deal with this?
        String code = "fname=\"$(awk '/^begin / {print $3}' $temp)\"";
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
    }

    @DisplayName("command substitution in 088-unpacker.sh employing sq in dq")
    @Test
    public void t60() {
        // TODO white-spaces in the awk-command
        // are being retained as white-spaces
        // instead of delimiters
        // How to deal with this?
        String code = "fname=\"$(awk 'beg%in')\"";
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);

    }

    @DisplayName("mvnw.sh")
    @Test
    public void t40() {
        String code = FileImporter.getFile("file/mvnw.sh")
                .getContent();
        assertCorrectOrderOfTopLevelToken(
                "IF_CLAUSE,EOL,LIST,"
                        + "EOL,CASE_CLAUSE,NEWLINE_LIST,"
                        + "IF_CLAUSE,NEWLINE_LIST,IF_CLAUSE,"
                        + "EOL,IF_CLAUSE",
                code);
    }

    @DisplayName("mvnw.sh")
    @Test
    public void t41() {
        String code = "# For Cygwin, ensure paths are in UNIX format before anything is touched\n"
                + "if $cygwin ; then\n"
                + "  [ -n \"$M2_HOME\" ] &&\n"
                + "    M2_HOME=`cygpath --unix \"$M2_HOME\"`\n"
                + "  [ -n \"$JAVA_HOME\" ] &&\n"
                + "    JAVA_HOME=`cygpath --unix \"$JAVA_HOME\"`\n"
                + "  [ -n \"$CLASSPATH\" ] &&\n"
                + "    CLASSPATH=`cygpath --path --unix \"$CLASSPATH\"`\n"
                + "fi\n";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(3, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(3,
                "BACKTICK_SUBSTITUTION", code);
        assertCorrectCountOfToken(7, "DOLLAR_CONSTRUCT",
                code);
        ShellParser.hasParsedAllTokens(code);
    }

    @DisplayName("066-exchangerate.sh")
    @Test
    public void t42() {
        String code = FileImporter
                .getFile("file/066-exchangerate.sh")
                .getContent();
        assertOk(code);
        assertCorrectOrderOfTopLevelToken(
                "FUNCTION_DEFINITION,NEWLINE_LIST,SIMPLE_COMMAND,"
                        + "EOL,SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND,NEWLINE_LIST,"
                        + "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,"
                        + "NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,NEWLINE_LIST,"
                        + "CASE_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,"
                        + "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,"
                        + "EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND",
                code);

    }

    @DisplayName("066-getexchrate.sh")
    @Test
    public void t43() {
        String code = FileImporter
                .getFile("file/066-getexchrate.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);
        assertCorrectOrderOfTopLevelToken(
                "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,"
                        + "NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,PIPE_SEQUENCE,"
                        + "NEWLINE_LIST,SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND",
                code);
    }

    @DisplayName("here document, using multiple lines employing io here and io redirect")
    @Test
    public void t44() {
        String code = "cat << \"EOF\"\n"
                + "Usage: $(basename $0) [-d c] [-t c] numeric value\n"
                + "Usage: $(basename $0) [-d c] [-t c] numeric value\n"
                + "EOF\n";
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfToken("IO_HERE", code);
        assertExistenceOfToken("HERE_DOCUMENT", code);
        assertCorrectCountOfToken(2, "HERE_DOCUMENT_LINE",
                code);

    }

    @Test
    public void t45() {
        String code = "ftp -n << EOF\n" + "open\n"
                + "EOF\n";
        assertExistenceOfToken("HERE_DOCUMENT", code);

    }

    @DisplayName("here document, using multiple lines employing io here and io redirect")
    @Test
    public void t46() {
        String code = "if [ $# -eq 0 ] ; then\n"
                + "  cat << \"EOF\" >&2\n"
                + "Usage: $(basename $0) [-d c] [-t c] numeric value\n"
                + "       -d EOF the decimal point delimiter (default '.')\n"
                + "       -t specifies the thousands delimiter (default ',')\n"
                +

                "EOF\n" + "  exit 1\n" + "fi\n";
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfToken("IO_HERE", code);
        assertExistenceOfToken("HERE_DOCUMENT", code);
        assertCorrectCountOfToken(3, "HERE_DOCUMENT_LINE",
                code);

    }

    @DisplayName("here document using the here document token in the here document")
    @Test
    public void t47() {
        String code = "cat << EOF\n"
                + "bla bla bla bla bla\n"
                + "bla bla EOF bla bla bla\n"
                + "EOF BLA bla bla EOF bla\n" + "EOF\n";
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfToken("IO_HERE", code);
        assertExistenceOfToken("HERE_DOCUMENT", code);
        assertCorrectCountOfToken(3, "HERE_DOCUMENT_LINE",
                code);

    }

    @DisplayName("papicli - test.sh")
    @Test
    public void t48() {
        String code = FileImporter
                .getFile("file/testPapiCli.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("papicli - completion.sh")
    @Test
    public void t49() {
        String code = FileImporter
                .getFile("file/papicli-completion.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("here document using the here document token in the here document")
    @Test
    public void t50() {
        String code = "    for word in \"${words[@]:1:$((cword-1))}\"\n"
                + // TODO this used to be delimited with a
                // newline instead of semicolon - case
                // is not covered by grammar
                "    do\n" + "        case \"${word}\" in\n"
                + "            booking | b | absence | ab | work-package | w | package | p)\n"
                + "                cmd=\"${word}\"\n"
                + "                ;;\n"
                + "            list | ls | add | a | change | c | remove | rm)\n"
                + "                op=\"${word}\"\n"
                + "                ;;\n"
                + "            a | c)\n"
                + "                opts=\"${operations}\"\n"
                + "                ;;\n"
                + "            --debug | -d | --help | -h | --version | -V | '')\n"
                + "                ;;\n"
                + "            *)\n"
                + "                params=(\"${params[@]}\" \"${word}\")\n"
                + "                ;;\n" + "        esac\n"
                + "    done\n";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("dollar construct employing an array and arithmetic expansion")
    @Test
    public void t51() {
        String code = "${words[@]:1:$((cword-1))}";
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
        assertExistenceOfToken("ARRAY", code);
        assertExistenceOfToken("ADDITIVE_EXPRESSION", code);

    }

    @DisplayName("dollar construct employing an array and arithmetic expansion")
    @Test
    public void t52() {
        String code = "    for h in $(seq --equal-width 08 19)\n"
                + "    do\n"
                + "        times=(${times[@]} $(seq --equal-width ${h}.00 0.15 ${h}.45 | sed 's/\\./:/g'))\n"
                + "    done\n";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("3 cases nested in one another")
    @Test
    public void t53() {
        String code = "        case \"${cmd}\" in\n"
                + "            absence | ab | booking | b )\n"
                + "                cparam=\"$((cword-3))\"\n"
                + "                case \"${op}\" in\n"
                + "                    list | ls)\n"
                + "                        opts=\"${resourceop1[cparam]}\"\n"
                + "                        ;;\n"
                + "                    add | a)\n"
                + "                        opts=\"${resourceop2[cparam]}\"\n"
                + "                        ;;\n"
                + "                    change | c | remove | rm)\n"
                + "                        opts=\"${resourceop3[cparam]}\"\n"
                + "                        ;;\n"
                + "                    '')\n"
                + "                        opts=\"${operations[@]}\"\n"
                + "                        ;;\n"
                + "                esac\n"
                + "                ;;\n"
                + "            work-package | w | package | p)\n"
                + "                case \"${op}\" in\n"
                + "                    list | ls)\n"
                + "                        opts=\"${resourceop4[cparam]}\"\n"
                + "                        ;;\n"
                + "                esac\n"
                + "        esac\n";
        assertCorrectCountOfToken(3, "CASE_CLAUSE", code);
        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("more elaborate if case employing bash io-redirect")
    @Test
    public void t54() {
        String code = "        if [[ -f \"${storagefile}\" ]]\n"
                + "        then\n"
                + "            local storage=\"$(cat \"${storagefile}\" 2> /dev/null)\"\n"
                + "            local bookingids=($(jq --raw-output '.bookings[]?.id?' <<< \"${storage}\" | sort --numeric))\n"
                + "            local bookingnotes=($(jq '.bookings[]?.note?' <<< \"${storage}\" | sort --unique))\n"
                + "            local packagenames=($(jq --raw-output '.workPackages[]? | \"\\(.id?) # \\(.label?)\"' <<< \"${storage}\" | sort --unique))\n"
                + "            local packagetasks=($(jq '.workPackages[]? | .task // empty' <<< \"${storage}\" | sort --unique))\n"
                + "        fi\n";
        assertCorrectCountOfToken(1, "IF_CLAUSE", code);
        // the first IO_REDIRECT indicated with
        // the 2> does not become an IO_REDIRECT
        // because it's in double quotes
        assertCorrectCountOfToken(4, "IO_REDIRECT", code);

        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("assignment word containing bash-confirm redirect")
    @Test
    public void t55() {
        String code = "local bookingnotes=($(jq '.bookings[]?.note?' <<< \"${storage}\" | sort --unique))\n";
        assertCorrectCountOfToken(1, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(1, "IO_REDIRECT", code);
        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("array using an additive expression")
    @Test
    public void t57() {
        String code = "prev=\"${words[cword-1]}\"\n";
        assertExistenceOfToken("ARRAY", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("ADDITIVE_EXPRESSION", code);
    }

    @DisplayName("papicli - actual file")
    @Test
    public void t59() {
        String code = FileImporter.getFile("file/papicli")
                .getContent();
        assertExistenceOfToken("SIMPLE_COMMAND", code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("giant here document from papicli")
    @Test
    public void t61() {
        String code = "print_help() {\n"
                + "    cat <<-EOT\n"
                + "${SCRIPT_NAME} ${VERSION} by ${MAINTAINER}\n"
                + "\n"
                + "Book your working efforts by CLI\n"
                + "\n"
                + "${SCRIPT_NAME} [options] [arguments]\n"
                + "               [b | booking] [ls | list] [startdate [enddate]]\n"
                + "               [b | booking] [a | add] date starttime endtime packageid [task | [note]]\n"
                + "               [b | booking] [rm | remove] id [date starttime endtime]\n"
                + "               [b | booking] [c | change] id date starttime endtime packageid [task | [note]]\n"
                + "               [a | absence] [ls | list] [startdate [enddate]]\n"
                + "               [u | user]\n"
                + "               [w | work-package | p | package] [ls | list] [withLegacy]\n"
                + "               plain [GET | PUT | POST | DELETE]\n"
                + "                   [user* | work-package | booking* | absence*]\n"
                + "                   [<HTTPie arguments]\n"
                + "\n" + "Options:\n"
                + "  -h, --help    print this help\n"
                + "  -d, --debug   debug mode (set -x)\n"
                + "\n" + "Arguments:\n"
                + "  st, status    show information about the current user, if he is logged in\n"
                + "\n"
                + "  li, login     use a prompt to retrieve a temporary authentication token\n"
                + "\n"
                + "  lo, logout    remove the temporary authentication token, invalidation\n"
                + "                is not possible yet\n"
                + "\n"
                + "  b, booking    booking resource\n"
                + "\n"
                + "  a, absence    absence resource\n"
                + "\n" + "  u, user       user resource\n"
                + "\n" + "  w, work-package, p, package\n"
                + "                work-package resource\n"
                + "\n"
                + "  plain         build requests directly by specifying HTTP methods and paths\n"
                + "\n"
                + "  doc           print the documentation provided by the API\n"
                + "\n"
                + "  cache         store temporary data associated with your account\n"
                + "                currently only used for dynamic autocompletion\n"
                + "\n" + "Parameters:\n"
                + "  Everything specified after a resource and operation is a parameter used as\n"
                + "  data either as GET or POST in the request. The following parameters can be\n"
                + "  specified with a dash ('-') to trigger the following default behavior:\n"
                + "  id            use date, starttime and endtime to find the id\n"
                + "  startdate     today\n"
                + "  enddate       today\n"
                + "  starttime     now\n"
                + "  endtime       now\n"
                + "  task          empty string\n"
                + "  note          empty string\n" + "\n"
                + "Examples:\n"
                + "  ${SCRIPT_NAME} status\n"
                + "  ${SCRIPT_NAME} login\n"
                + "  ${SCRIPT_NAME} plain get booking start==2018-10-20 end==2018-10-22\n"
                + "  ${SCRIPT_NAME} plain get work-package withLegacy==false\n"
                + "  ${SCRIPT_NAME} plain post booking date=2018-11-02 startTime=15:45 endTime=16:00 workPackage:='{\"id\":\"4284.3-3\"}' note=foo\n"
                + "  ${SCRIPT_NAME} plain delete booking/0815\n"
                + "  ${SCRIPT_NAME} user\n"
                + "  ${SCRIPT_NAME} work-package list\n"
                + "  ${SCRIPT_NAME} work-package list withLegacy\n"
                + "  ${SCRIPT_NAME} booking list -2days\n"
                + "  ${SCRIPT_NAME} booking list 2018-10-01 2018-10-25\n"
                + "  ${SCRIPT_NAME} absence list -30days\n"
                + "  ${SCRIPT_NAME} booking add yesterday 9:00 10:00 0815.1-1 \"Debugging\"\n"
                + "  ${SCRIPT_NAME} booking add - 08:00 09:00 0815.1-1 \"Meeting\" \"Sprint Planning\"\n"
                + "  ${SCRIPT_NAME} booking change yesterday 9 10 4711.1-1 \"Debugging\"\n"
                + "  ${SCRIPT_NAME} booking change - 08:00 09:00 4711.1-1 \"Meeting\" \"Debugging\"\n"
                + "  ${SCRIPT_NAME} booking change 2018-01-01_09:00_10:00_909_TYPE_JOBS_NAME_0815.1-1 - 9 12 0815.1-1 \"Refactoring\"\n"
                + "  ${SCRIPT_NAME} booking remove yesterday 9 10\n"
                + "  ${SCRIPT_NAME} booking remove - 08:00 09:00\n"
                + "  ${SCRIPT_NAME} booking remove 2018-01-01_09:00_10:00_909_TYPE_JOBS_NAME_0815.1-1\n"
                + "  ${SCRIPT_NAME} logout\n"
                + "  ${SCRIPT_NAME} doc\n" + "\n"
                + "Known bugs:\n"
                + "  when ZSH emulates Bash Completion, parameters after 'booking add' complete as\n"
                + "      unexpanded variable names, e.g. '\\${dates[@]}'\n"
                + "\n" + "Resources:\n"
                + "HTTPie      https://httpie.org/doc#examples\n"
                + "API doc     ${URL}/${SWAGGER_PATH}\n"
                + "GNU date    \\`info coreutils aqdate -n 'Day of week items'\\`\n"
                + "EOT\n" + "}";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertExistenceOfToken("HERE_DOCUMENT", code);
        assertScriptHasBeenFullyParsed(code);

    }

    @DisplayName("array using an additive expression")
    @Test
    public void t62() {
        String code = "storage() {\n"
                + "    local operation=\"${1}\"\n"
                + "    local data=\"${2:-{\\}}\"\n"
                + "    case \"${operation}\" in\n"
                + "        find)\n"
                + "            STORAGE_FILE=\"$(find \"${STORAGE_DIR}/\" -name \"${STORAGE_FILE_GLOB}\" \\\n"
                + "                -printf '%T@\\t%p\\n' 2> /dev/null | sort --numeric-sort | head -1 | cut -f2)\"\n"
                + "            ;;\n" + "        get)\n"
                + "            cat \"${STORAGE_FILE}\"\n"
                + "            ;;\n" + "        create)\n"
                + "            STORAGE_FILE=\"$(mktemp --tmpdir=\"${STORAGE_DIR}\" --suffix=-\"${SCRIPT_NAME}\")\"\n"
                + "            chmod go-rwx \"${STORAGE_FILE}\"\n"
                + "            echo -n \"${data}\" > \"${STORAGE_FILE}\"\n"
                + "            ;;\n" + "        remove)\n"
                + "            rm -f ${STORAGE_DIR}/${STORAGE_FILE_GLOB}\n"
                + "            STORAGE_FILE=\"\"\n"
                + "            ;;\n" + "        update)\n"
                + "            local datanew=\"$(jq --raw-output --compact-output --null-input \"$(storage get) * ${data}\")\"\n"
                + "            echo -n \"${datanew:-{\\}}\" > \"${STORAGE_FILE}\"\n"
                + "            ;;\n" + "    esac\n"
                + "    return ${?}\n" + "}\n";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("dollar construct using escaped braced parameter")
    @Test
    public void t63() {

        String code = "${2:-{\\}}";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("ESCAPED_SPECIAL_CHARACTER",
                code);

    }

    @DisplayName("dollar construct employing a number and a case variation")
    @Test
    public void t64() {
        String code = "${1^^}";
        assertExistenceOfToken("CASE_VARIATION", code);
    }

    @DisplayName("usage of multiple words in dollar construct")
    @Test
    public void t66() {
        String code = "${TOKEN:+Authorization:\"Bearer ${TOKEN}\"} \\\n";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken(
                "PARAMETER_EXPANSION_OPERATOR", code);

    }

    @DisplayName("global substitution dollar construct")
    @Test
    public void t65() {
        String code = "${1//,/|}\n";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("GLOBAL_SUBSTITUTION", code);

    }

    @DisplayName("io_redirect in double-quoted-context, single-quoted-word in double-quoted-command-substitution")
    @Test
    public void t67() {
        String code = "print_exit 0 \"Logged in as user $(jq --raw-output "
                + "'\"\\(.firstName) \\(.lastName)\"' <<< \"${response}\")\" || \\\n"
                + "print_exit \"${?}\" \"Logged out\"\n";
        assertCorrectCountOfToken(3, "SIMPLE_COMMAND",
                code);
        assertExistenceOfToken("AND_OR", code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertExistenceOfToken("IO_REDIRECT", code);
        // unfortunately, "${response}" does not
        // get parsed as
        // double-quoted-word-token,
        // because we are already in
        // double-quote-context
        assertCorrectCountOfToken(3, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(1,
                "MOCK_DOUBLE_QUOTED_WORD", code);

        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("case 67, in facilitated form")
    @Test
    public void t70() {
        String code = "echo \"blablabla $(bla '\"\\(.firstName) \\(.lastName)\"' <<< \"do re mi\")\"\n";
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        // unfortunately, "do re mi" does not
        // get parsed as double-quoted, because
        // we are already in
        // double-quote-context
        assertCorrectCountOfToken(1, "DOUBLE_QUOTED_WORD",
                code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("another script")
    @Test
    public void t68() {
        String code = FileImporter
                .getFile("./file/checkDockerDeps.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("more script to be parsed")
    @Test
    public void t71() {
        String code = FileImporter.getFile("./file/bla.sh")
                .getContent();
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("If then else using =~ in test")
    @Test
    public void t72() {
        String code = "if [[ $vendoDashUrl =~ [/\\\\]$ ]] # Checks if vendoDashUrl ends with /\n"
                + "then\n"
                + "    fullUrl=\"$vendoDashUrl\"reports/json\n"
                + "else\n"
                + "    fullUrl=\"$vendoDashUrl\"/reports/json\n"
                + "fi\n";
        assertExistenceOfToken("PATTERN", code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("testing path")
    @Test
    public void t73() {
        String code = "echo xo/xo";
        assertExistenceOfToken("PATH", code);
    }

    @DisplayName("path with dot")
    @Test
    public void t74() {
        String code = "echo ./xo";
        assertExistenceOfToken("PATH", code);
    }

    @DisplayName("no path in double-quoted-context.")
    @Test
    public void t75() {
        String code = "echo \"xo/xo\"";
        assertAbsenceOfToken("PATH", code);
    }

    @DisplayName("path with tilde expansion")
    @Test
    public void t76() {
        String code = "echo ~nnoorshams/xoxo";
        assertExistenceOfToken("TILDE_EXPANSION", code);
        assertExistenceOfToken("PATH", code);
    }

    @DisplayName("no path in this test case")
    @Test
    public void t77() {
        String code = "	echo xo";
        assertAbsenceOfToken("PATH", code);
    }

    @DisplayName("a_script.sh")
    @Test
    public void t78() {
        String code = FileImporter
                .getFile("./file/a_script.sh")
                .getContent();
        assertOk(code);
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName("a_script.sh -  configureGit-function")
    @Test
    public void t79() {
        String code = "\tconfigureGit() {\n" +
                "\t\techo \"#####################################\n" +
                "########  INSIDE configureGit ##############\n" +
                "###########################################\"\n" +
                "\t\targs=( \"$@\" )\n" +
                "\t\t#IFS='' _get_key_value args[@] \"--git-deploy-key\"\n" +
                "\t\tGIT_USERNAME=$(_get_key_value args[@] \"--git-username\")\n" +
                "\t\tGIT_USER_EMAIL=$(_get_key_value args[@] \"--git-user-email\")\n" +
                "\t\tPOC_DEPLOY_KEY=$(_get_key_value args[@] \"--git-deploy-key\")\n" +
                "    #init git\n" +
                "\t\tgit config --global user.email \"${GIT_USER_EMAIL}\"\n" +
                "\t\tgit config --global user.name \"${GIT_USERNAME}\"\n" +
                "\t\tgit config --global color.ui always\n" +
                "\t\tgit config --list\n" +
                "\t\teval $(ssh-agent -s)\n" +
                "\t#bash on commandline converts linebreaks into spaces without quotation marks\n" +
                "\t\tssh-add <(echo \"$POC_DEPLOY_KEY\" | base64 -d )\n" +
                "\t\tssh-add -L\n" +
                "\t\tssh -o \"StrictHostKeyChecking=no\" git@ssh.git.tech.rz.db.de\n" +
                "\t}\n";
        assertScriptHasBeenFullyParsed(code);
    }

    @DisplayName(("a_script.sh - configureGit-function - process substitution"))
    @Test
    public void t80() {
        String code = "ssh-add <(echo \"$POC_DEPLOY_KEY\" | base64 -d )\n";
        assertExistenceOfToken("PROCESS_SUBSTITUTION", code);
        assertCorrectCountOfToken(3, "SIMPLE_COMMAND", code);
    }


    @DisplayName(("process substitution employing an if-clause"))
    @Test
    public void t81() {
        String code = "wc <(if [ 1 -eq 1 ]; then ls -a; fi; ls)\n";
        assertExistenceOfToken("PROCESS_SUBSTITUTION", code);
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_BRACKET_TEST", code);
    }

    @DisplayName("c-style-loop with no semicolon, but newline")
    @Test
    public void t82() {
        String code = "\n" +
                "  for (( val=1; $val < $len ; foo ))\n" +
                "  do\n" +
                "    if [ $(perl -e \"print int\") -eq 1 ] ; then\n" +
                "      scrambled=$scrambled$(echo $match | cut -c$val)\n" +
                "    else\n" +
                "      scrambled=$(echo $match | cut -c$val)$scrambled\n" +
                "    fi\n" +
                "    val=$(( $val + 1 ))\n" +
                "  done";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("LOOP_CONDITION_C_STYLE", code);
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD", code);
    }

    @DisplayName("c-style-loop with multiple arguments in the loop components")
    @Test
    public void t83() {
        String code = "for (( i = 0, j = 0; i < 10; i++, j = i * i ))\n" +
                "do\n" +
                "    echo \"The square of $i is equal to $j\"\n" +
                "done";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertCorrectCountOfToken(5, "LOOP_COMPONENT", code);
    }

    @Test
    public void t85() {
        String code = "  for (( val=1; $val < $len ;))\n" +
                "  do\n" +
                "    echo xo\n" +
                "  done";
        assertExistenceOfToken("LOOP_CONDITION_C_STYLE", code);
        assertCorrectCountOfToken(2, "LOOP_COMPONENT", code);
    }

}
