/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.api.TokenType;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sonar.sslr.api.GenericTokenType.IDENTIFIER;

/**
 * State-based Identifier and Keyword channel. Extends sonarqube-implementation
 * twofold:
 * <p>
 * -Checks if current context is a double-quote context. if that is the case,
 * keywords simply get parsed as IDENTIFIER-tokens. -After an identifier or
 * keyword has been parsed, the parser also creates a token for subsequent
 * white-space. Intention of this is to delimit command name and the
 * multiplicity of subsequent command suffixes from one another.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class StatefulIdentifierAndKeywordChannel extends Channel<Lexer> {

	LexerState ls;

	private final Map<String, TokenType> keywordsMap = new HashMap<>();
	private final StringBuilder tmpBuilder = new StringBuilder();
	private final Matcher matcher;
	private final boolean caseSensitive;
	private final Token.Builder tokenBuilder = Token.builder();

	/**
	 * @throws java.util.regex.PatternSyntaxException if the expression's syntax
	 *                                                is invalid
	 */
	public StatefulIdentifierAndKeywordChannel(String regexp,
											   boolean caseSensitive, LexerState ls, TokenType[]... keywordSets) {
		for (TokenType[] keywords : keywordSets) {
			for (TokenType keyword : keywords) {
				String keywordValue = caseSensitive ? keyword.getValue()
						: keyword.getValue().toUpperCase();
				keywordsMap.put(keywordValue, keyword);
			}
		}
		this.caseSensitive = caseSensitive;
		matcher = Pattern.compile(regexp).matcher("");
		this.ls = ls;
	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {

		if (code.popTo(matcher, tmpBuilder) > 0) {

			String word = tmpBuilder.toString();
			String wordOriginal = word;
			if (!caseSensitive) {
				word = word.toUpperCase();
			}

			TokenType keywordType = keywordsMap.get(word);

			Token token;


			token = tokenBuilder
					.setType(keywordType == null || ls.isInDoubleQuotes()
							? IDENTIFIER : keywordType)
					.setValueAndOriginalValue(word, wordOriginal)
					.setURI(lexer.getURI())
					.setLine(code.getPreviousCursor().getLine())
					.setColumn(code.getPreviousCursor().getColumn()).build();
			lexer.addToken(token);

			tmpBuilder.delete(0, tmpBuilder.length());
			return true;
		}
		return false;
	}

}
