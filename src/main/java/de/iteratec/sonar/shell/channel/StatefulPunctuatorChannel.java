/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.api.TokenType;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * State-based Identifier and Keyword channel. Extends sonarqube-implementation
 * twofold:
 * <p>
 * 1. Checks if current context is a double-quote context. if that is the case,
 * keywords simply get parsed as IDENTIFIER-tokens. 2. After an identifier or
 * keyword has been parsed, the parser also creates a token for subsequent
 * white-space. Intention of this is to delimit command name and the
 * multiplicity of subsequent command suffixes from one another.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class StatefulPunctuatorChannel extends Channel<Lexer> {

	LexerState ls;

	private final int lookahead;
	private final TokenType[] sortedPunctuators;
	private final char[][] sortedPunctuatorsChars;
	private final Token.Builder tokenBuilder = Token.builder();
	private final StringBuilder tmpBuilder = new StringBuilder();


	private static class PunctuatorComparator implements Comparator<TokenType> {

		@Override
		public int compare(TokenType a, TokenType b) {
			if (a.getValue().length() == b.getValue().length()) {
				return 0;
			}
			return a.getValue().length() > b.getValue().length() ? -1 : 1;
		}

	}

	public StatefulPunctuatorChannel(LexerState ls, TokenType... punctuators) {
		sortedPunctuators = punctuators;
		Arrays.sort(sortedPunctuators, new PunctuatorComparator());

		sortedPunctuatorsChars = new char[sortedPunctuators.length][];

		int maxLength = 0;
		for (int i = 0; i < sortedPunctuators.length; i++) {
			sortedPunctuatorsChars[i] = sortedPunctuators[i].getValue()
					.toCharArray();
			maxLength = Math.max(maxLength, sortedPunctuatorsChars[i].length);
		}
		this.lookahead = maxLength;
		this.ls = ls;
	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {
		char[] next = code.peek(lookahead);
		for (int i = 0; i < sortedPunctuators.length; i++) {
			if (arraysEquals(next, sortedPunctuatorsChars[i])) {
				Token token = tokenBuilder.setType(sortedPunctuators[i])
						.setValueAndOriginalValue(
								sortedPunctuators[i].getValue())
						.setURI(lexer.getURI()).setLine(code.getLinePosition())
						.setColumn(code.getColumnPosition()).build();

				lexer.addToken(token);
				/*
				 * Advance the CodeReader stream by the length of the punctuator
				 */
				for (int j = 0; j < sortedPunctuatorsChars[i].length; j++) {
					code.pop();
				}
				//either << or <<-
				if (isHereDocumentPunctuator(token) && !ls.isInDoubleQuotes()) {
					Matcher matcher = Pattern.compile("[ \t]+").matcher("");

					if ((char) code.peek() == ' ') {
						code.popTo(matcher, tmpBuilder);
						tmpBuilder.delete(0, tmpBuilder.length());
					}
					if (!Character.isDigit(code.charAt(0))) {
						ls.setHereDocumentTokenRequired(true);
					}

				}


				// Previous character mustn't be an escape character?
				return true;
			}
		}

		return false;

	}

	private boolean isHereDocumentPunctuator(Token token) {
		return token.getValue().equals("<<") || token.getValue().equals("<<-");
	}

	/**
	 * Expected that length of second array can be less than length of first.
	 */
	private static boolean arraysEquals(char[] a, char[] a2) {
		int length = a2.length;
		for (int i = 0; i < length; i++) {
			if (a[i] != a2[i]) {
				return false;
			}
		}
		return true;
	}

}
