package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.st.Declaration;
import de.iteratec.sonar.shell.st.SymbolTable;
import de.iteratec.sonar.shell.st.SymbolTableBuilder;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static de.iteratec.sonar.shell.ast.ShellGrammar.FUNCTION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Rule(key = "SH000",
        name = "Variables are actually used",
        description = "Variable that is declared is also used",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class AllDeclaredVariablesAreReferencedCheck extends AbstractCheckTester {

    String message = "An unreferenced declaration has been detected. How could you?";

    ArrayList<PreciseIssue> issues = new ArrayList();


    @Override
    public boolean visitNode(AstNode node) {

        HashMap<AstNode, SymbolTable> symbolTables = SymbolTableBuilder.buildTables(node);

        return checkAllVariablesAreReferenced(symbolTables.values());

    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        HashMap<AstNode, SymbolTable> symbolTables = SymbolTableBuilder.buildTables(node);
        checkAllVariablesAreReferencedAndRaiseIssues(symbolTables.values());
        return issues;
    }

    private boolean checkAllVariablesAreReferenced(Collection<SymbolTable> tables) {
        return tables.stream().allMatch(table -> table.allVariablesReferenced());
    }

    private void checkAllVariablesAreReferencedAndRaiseIssues(Collection<SymbolTable> tables) {
        List<Declaration> unreferencedDeclarations = tables.stream()
                .flatMap(table -> table.getUnreferencedDeclarations().stream())
                .collect(Collectors.toList());

        unreferencedDeclarations.stream().map(declaration ->
                declaration.getNode().getType().equals(ShellGrammar.FUNCTION_DEFINITION) ?
                        declaration.getNode().getFirstDescendant(FUNCTION_NAME) :
                        declaration.getNode()
        ).forEach(unreferencedDeclaration ->
                issues.add(new PreciseIssue(IssueLocation.tokenLocation(unreferencedDeclaration, message))
                )
        );
    }

    @DisplayName("calling variable in different scopes works fine")
    @Test
    public void t3() {
        String s = "xyo=123\n" + "foo(){" + "var2='ox'\n"
                + "echo ${var2}\n" + "}\n"
                + "if [ 1 -eq 1 ]; then\n" + "foo \n" +
                "echo ${xyo}\n"
                + "else\n" + "echo ${xyo}\n" + "fi";

        assertThatCheckSucceeds(s);
    }

    @DisplayName("right number of scopes get detected")
    @Test
    public void t1() {
        String s = "xo=123 \n " + "foo(){" + "xo='ox'\n"
                + "echo xo\n" + "}\n"
                + "if test 1 -eq 1; then\n" + "echo xoxo\n"
                + "else\n" + "echo oxox\n" + "fi";
        assertEquals(4, ShellParser.create().parse(s).getDescendants(ShellGrammar.COMPOUND_LIST).size());
    }

    @DisplayName("Reference in function scope uses global declaration and leads to check success")
    @Test
    public void t2() {
        String s = "xo=1 \n " + "foo(){" + "echo ${xo}\n"
                + "}\n echo ${xo} \n echo ${xo}\n foo\n";

        assertThatCheckSucceeds(s);
    }

    @DisplayName("same scope in function works fine")
    @Test
    public void t8() {
        String s = "foo(){" + "xo=1 \n " + "echo ${xo}\n"
                + "}\n foo\n";
        assertThatCheckSucceeds(s);
    }

    @DisplayName("declared function that is not used in the script")
    @Test
    public void t16() throws Exception {
        String s = "foo(){" + "xo=1 \n " + "echo ${xo}\n"
                + "}\n";
        assertThatIssuesAreGenerated(s);
    }

    @DisplayName("same scope works fine")
    @Test
    public void t9() {
        String s = "xo=1 \n "
                + "echo ${xo} \n echo ${xo} \n echo ${xo}\n";
        assertThatCheckSucceeds(s);
    }

    @DisplayName("not an actual variable reference leads to failure")
    @Test
    public void t5() {
        String s = "xo=1 \n " + "foo(){" + "echo xo\n"
                + "}\n foo\n";
        assertThatCheckFails(s);
    }

    @Disabled
    @DisplayName("wrong sequence of declaration and reference")
    @Test
    public void t7() {
        String s = "foo(){\n" + "echo ${xo}\n" + "}\n"
                + "xo=1";
        assertThatCheckFails(s);
    }

    @DisplayName("adjacent functions done right")
    @Test
    public void t10() {
        String s = "foo(){\n" + "xo=1; echo ${xo}\n" + "}\n"
                + "foo\n"
                + "bar(){\n" + "x=1; echo ${x}\n" + "}\n" +
                "bar\n";
        assertThatCheckSucceeds(s);
    }

    @Test
    public void t6() {
        String s = "" +
                "xyo=123\n" +
                "foo(){" +
                "xo='ox'\n"
                + "uwu=xoxoxo\n"
                + "echo ${xo}\n"
                + "echo ${uwu}\n" +
                "}\n" +
                "if [ 1 -eq 1 ]; then\n"
                + "echo ${xyo}\n" +
                "else\n" + "axaxa=31\n" + "echo ${xyo}\n"
                + "echo ${axaxa}\n" +
                "foo\n" + "fi";
        assertThatCheckSucceeds(s);

    }

    @DisplayName("arithmetic expansion")
    @Test
    public void t11() {
        String code = "a=1; b=2; echo $((a+b));";
        assertThatCheckSucceeds(code);
    }

    @DisplayName("arithmetic expansion fails")
    @Test
    public void t12() {
        String code = "a=1; b=2; c=3; echo $((a+b));";
        assertThatCheckFails(code);

    }

    @DisplayName("arithmetic expansion in new scope")
    @Test
    public void t14() {
        String code = "a=1; b=2; my_function(){\n echo $((a+b));\n}\n my_function\n";
        assertThatCheckSucceeds(code);
    }

    @DisplayName("arithmetic expansion in new scope fails")
    @Test
    public void t13() {
        String code = "a=1; b=2; c=3; my_function(){\n echo $((a+b));\n}\n";
        assertThatCheckFails(code);

    }


}
