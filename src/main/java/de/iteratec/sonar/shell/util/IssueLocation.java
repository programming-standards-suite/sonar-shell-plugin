package de.iteratec.sonar.shell.util;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.Token;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;

/**
 * Allows generation of issue locations with the respective static functions
 */
public abstract class IssueLocation {

    public static final int UNDEFINED_OFFSET = -1;

    public static final int UNDEFINED_LINE = 0;

    private String message;

    private IssueLocation(@Nullable String message) {
        this.message = message;
    }

    /**
     * generates an issue on the file
     *
     * @param message the message that shall be indicated by the issue
     * @return
     */
    public static IssueLocation atFileLevel(String message) {
        return new FileLevelIssueLocation(message);
    }

    /**
     * generates an issue on the line
     *
     * @param message    the message that shall be indicated by the issue
     * @param lineNumber the line-number of the issue
     * @return
     */
    public static IssueLocation atLineLevel(String message, int lineNumber) {
        return new LineLevelIssueLocation(message, lineNumber);
    }

    /**
     * generates an issue that ranges from starting node until the passed end node
     *
     * @param startNode the node of the token that indicates the start of issue
     * @param endNode   the node of the token that indicates the end of issue
     * @param message   the message that shall be indicated by the issue
     * @return
     */
    public static IssueLocation preciseLocation(AstNode startNode, AstNode endNode, String message) {
        return new PreciseIssueLocation(startNode, endNode, message);
    }

    /**
     * generates an issue that ranges over all tokens contained in the passed node
     *
     * @param startNode the node of the token that indicates the start of issue
     * @param message   the message that shall be indicated by the issue
     * @return
     */
    public static IssueLocation preciseLocation(AstNode startNode, @Nullable String message) {
        return new PreciseIssueLocation(startNode, message);
    }

    /**
     * generates an issue that ranges only on the token of the passed node
     *
     * @param startNode the node of the token that indicates the start and end of issue
     * @param message   the message that shall be indicated by the issue
     * @return
     */
    public static IssueLocation tokenLocation(AstNode startNode, @Nullable String message) {
        return new TokenLevelLocation(startNode, message);
    }

    @CheckForNull
    public String message() {
        return message;
    }

    public abstract int startLine();

    public abstract int startLineOffset();

    public abstract int endLine();

    public abstract int endLineOffset();

    private static class PreciseIssueLocation extends IssueLocation {

        private final Token firstToken;
        private final TokenLocation lastTokenLocation;

        public PreciseIssueLocation(AstNode node, @Nullable String message) {
            super(message);
            this.firstToken = node.getToken();
            this.lastTokenLocation = new TokenLocation(node.getLastToken());
        }

        public PreciseIssueLocation(AstNode startNode, AstNode endNode, String message) {
            super(message);
            this.firstToken = startNode.getToken();
            this.lastTokenLocation = new TokenLocation(endNode.getLastToken());
        }

        @Override
        public int startLine() {
            return firstToken.getLine();
        }

        @Override
        public int startLineOffset() {
            return firstToken.getColumn();
        }

        @Override
        public int endLine() {
            return lastTokenLocation.endLine();
        }

        @Override
        public int endLineOffset() {
            return lastTokenLocation.endLineOffset();
        }

    }

    private static class LineLevelIssueLocation extends IssueLocation {

        private final int lineNumber;

        public LineLevelIssueLocation(String message, int lineNumber) {
            super(message);
            this.lineNumber = lineNumber;
        }

        @Override
        public int startLine() {
            return lineNumber;
        }

        @Override
        public int startLineOffset() {
            return UNDEFINED_OFFSET;
        }

        @Override
        public int endLine() {
            return lineNumber;
        }

        @Override
        public int endLineOffset() {
            return UNDEFINED_OFFSET;
        }

    }

    private static class FileLevelIssueLocation extends IssueLocation {

        public FileLevelIssueLocation(String message) {
            super(message);
        }

        @Override
        public int startLine() {
            return UNDEFINED_LINE;
        }

        @Override
        public int startLineOffset() {
            return UNDEFINED_OFFSET;
        }

        @Override
        public int endLine() {
            return UNDEFINED_LINE;
        }

        @Override
        public int endLineOffset() {
            return UNDEFINED_OFFSET;
        }

    }

    private static class TokenLevelLocation extends IssueLocation {
        private final Token token;

        public TokenLevelLocation(AstNode node, String message) {
            super(message);
            this.token = node.getToken();
        }

        public int startLine() {
            return this.token.getLine();
        }

        public int startLineOffset() {
            return this.token.getColumn();
        }

        public int endLine() {
            return this.token.getLine();
        }

        public int endLineOffset() {
            return token.getValue().length() + this.token.getColumn();
        }
    }

}