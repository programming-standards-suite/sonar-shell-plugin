package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.List;

@Rule(key = "SH004",
        name = "The word local is used",
        description = "The word local is used",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class NoUsageOfLocalCheck extends AbstractCheckTester {

    ArrayList<PreciseIssue> issues = new ArrayList();

    String message = "You used local, but that's forbidden. how could you?";

    @Override
    public boolean visitNode(AstNode token) {
        List<AstNode> simpleCommands = AstUtils.getAllOccurencesOf("SIMPLE_COMMAND", token);
        return checkNoUsageOfLocal(simpleCommands);
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        List<AstNode> simpleCommands = AstUtils.getAllOccurencesOf("SIMPLE_COMMAND", node);
        checkNoUsageOfLocalAndRaiseIssues(simpleCommands);
        return issues;
    }

    private void checkNoUsageOfLocalAndRaiseIssues(List<AstNode> simpleCommands) {
        for (AstNode simpleCommand : simpleCommands) {
            if (simpleCommand
                    .getFirstChild()
                    .getTokenValue()
                    .equals("local")) {

                PreciseIssue issue = new PreciseIssue(IssueLocation.tokenLocation(simpleCommand.getFirstChild(), message));
                issues.add(issue);
            }
        }

    }

    private boolean checkNoUsageOfLocal(List<AstNode> simpleCommands) {

        for (AstNode simpleCommand : simpleCommands) {
            if (simpleCommand
                    .getFirstChild()
                    .getTokenValue()
                    .equals("local")) {

                return false;

            }
        }
        return true;
    }

}
