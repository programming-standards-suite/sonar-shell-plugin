#!/bin/bash
set -x
test -z "${KEEP_GOING}" && set -e

DEFAULT_PACKAGE_ID=4284.3-3
USER_ID=
COMMAND="./papicli ${DEBUG:+--debug}"
TODAY="$(date +%F --date today)"
YESTERDAY="$(date +%F --date yesterday)"

get_id() {
    echo -n "$(date +%F --date ${1})_$(date +%R --date ${2})_$(date +%R --date ${3})_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID}"
}

before() {
    ${COMMAND} status || ${COMMAND} login
    USER_ID=$(${COMMAND} raw GET user | jq --raw-output '.employeeId')
}

raw() {
    ${COMMAND} raw GET user
    ${COMMAND} raw GET booking start==2018-01-01 end==2018-01-10
    ${COMMAND} raw GET booking start==${YESTERDAY} end==${TODAY}
    ${COMMAND} raw GET work-package withLegacy==false
    ${COMMAND} raw GET absence start==2018-01-01 end==2018-01-31
    ${COMMAND} raw GET absence start==${YESTERDAY} end==${TODAY}
    ${COMMAND} raw POST booking date=${YESTERDAY} startTime=03:00 endTime=04:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task= note=test1
    ${COMMAND} raw POST booking date=${TODAY} startTime=04:00 endTime=05:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task= note=test2
    ${COMMAND} raw POST booking date=${TODAY} startTime=05:00 endTime=06:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task=Meeting note=test3
    ${COMMAND} raw POST booking date=${TODAY} startTime=03:00 endTime=04:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task= note=test4
    ${COMMAND} raw POST booking id=${YESTERDAY}_03:00_04:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID} date=${TODAY} startTime=02:00 endTime=03:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task= note=test5
    ${COMMAND} raw POST booking id=${TODAY}_03:00_04:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID} date=${TODAY} startTime=03:00 endTime=04:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task=Meeting note=test6
    ${COMMAND} raw POST booking id=${TODAY}_04:00_05:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID} date=${TODAY} startTime=04:00 endTime=05:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task=Meeting note=test7
    ${COMMAND} raw POST booking id=${TODAY}_05:00_06:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID} date=${TODAY} startTime=05:00 endTime=06:00 'workPackage:={"id":"'${DEFAULT_PACKAGE_ID}'"}' task=Meeting note=test8
    ${COMMAND} raw DELETE booking/${TODAY}_02:00_03:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID}
    ${COMMAND} raw DELETE booking/${TODAY}_03:00_04:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID}
    ${COMMAND} raw DELETE booking/${TODAY}_04:00_05:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID}
    ${COMMAND} raw DELETE booking/${TODAY}_05:00_06:00_${USER_ID}_TYPE_JOBS_NAME_${DEFAULT_PACKAGE_ID}
}

parameters() {
    ${COMMAND} --help
    ${COMMAND} --version
}

reading() {
    ${COMMAND} doc
    ${COMMAND} user
    ${COMMAND} booking list
    ${COMMAND} booking list last-week today
    ${COMMAND} booking list 1-day-ago
    ${COMMAND} work-package list
    ${COMMAND} absence list
}

writing() {
    ${COMMAND} booking add yesterday 3 4 ${DEFAULT_PACKAGE_ID} test1
    ${COMMAND} booking add today 3 4 ${DEFAULT_PACKAGE_ID} test2
    ${COMMAND} booking add - 4 5 ${DEFAULT_PACKAGE_ID} test3
    ${COMMAND} booking add - 5 6 ${DEFAULT_PACKAGE_ID} "Meeting" test4
    ${COMMAND} booking change $(get_id yesterday 3 4) today 2 3 ${DEFAULT_PACKAGE_ID} test5
    ${COMMAND} booking change - today 3 4 ${DEFAULT_PACKAGE_ID} test6
    ${COMMAND} booking change $(get_id today 4 5) today 4 5 ${DEFAULT_PACKAGE_ID} test7
    ${COMMAND} booking change - - 5 6 ${DEFAULT_PACKAGE_ID} "Meeting" test8
}

removing() {
    ${COMMAND} booking remove $(get_id today 2 3)
    ${COMMAND} booking remove - today 3 4
    ${COMMAND} booking remove $(get_id today 4 5)
    ${COMMAND} booking remove - - 5 6
}

after() {
    ${COMMAND} logout
}

all() {
    parameters
    before
    raw
    reading
    writing
    removing
    after
}

eval "${@:-all}" > $(test -z $DEBUG && echo "/dev/null" || echo "/dev/stdout")

if [[ $? -eq 0 ]]
then
    echo "All tests succeeded"
    exit 0
else
    echo "Tests failed"
    exit 1
fi

