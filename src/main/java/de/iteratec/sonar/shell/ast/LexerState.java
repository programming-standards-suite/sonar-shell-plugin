/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.ast;

import java.util.ArrayList;
import java.util.List;

/**
 * State of the lexer that gets changed during lexing. Deals with the creation of here-documents,
 * various things regarding double quotes (e.g. retaining white spaces between tokens instead of throwing them out,
 * turning keywords into punctuators...) and generation of single quoted words.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class LexerState {

    private boolean inDoubleQuotes = false;

    private boolean inSingleQuotes = false;

    private boolean hereDocumentTokenRequired = false;

    /**
     * Usage of list is required simply because multiple here documents in a row can be started.
     * See section274b() in PosixShellParserTest. for an example
     */
    private List<String> hereDocumentTokenValueList = new ArrayList();

    /**
     * @return the inDoubleQuotes
     */
    public boolean isInDoubleQuotes() {
        return inDoubleQuotes;
    }

    /**
     * @param inDoubleQuotes the inDoubleQuotes to set
     */
    public void setInDoubleQuotes(boolean inDoubleQuotes) {
        this.inDoubleQuotes = inDoubleQuotes;
    }

    /**
     * @return the inSingleQuotes
     */
    public boolean isInSingleQuotes() {
        return inSingleQuotes;
    }

    /**
     * @param inSingleQuotes the inSingleQuotes to set
     */
    public void setInSingleQuotes(boolean inSingleQuotes) {
        this.inSingleQuotes = inSingleQuotes;
    }

    public boolean isHereDocumentTokenRequired() {
        return hereDocumentTokenRequired;
    }

    public void setHereDocumentTokenRequired(boolean hereDocumentTokenRequired) {
        this.hereDocumentTokenRequired = hereDocumentTokenRequired;
    }

    public List<String> getHereDocumentTokenValueList() {
        return this.hereDocumentTokenValueList;
    }

}
