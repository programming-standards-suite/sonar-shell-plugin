/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.sslr;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class AstUtilsTest extends AbstractParserTester {

    @DisplayName("three simple commands with a repeat")
    @Test
    public void t1() {

        String code = "echo a; repeat 4 do \n echo b \n done; echo c;";

        AstNode tree = ShellParser.parse(code);

        assertEquals(3,
                AstUtils.getAllOccurencesOf("SIMPLE_COMMAND", tree).size());
        assertEquals(1,
                AstUtils.getAllOccurencesOf("REPEAT_CLAUSE", tree).size());

    }

    @DisplayName("two if then elses")
    @Test
    public void t2() {
        String code = "if [ true ]; \n then echo bla \n fi; if [ true ]; \n then echo bla \n fi;";

        AstNode tree = ShellParser.parse(code);

        assertEquals(2,
                AstUtils.getAllOccurencesOf("SINGLE_BRACKET_TEST", tree).size());
        assertEquals(4,
                AstUtils.getAllOccurencesOf("SIMPLE_COMMAND", tree).size());
        assertEquals(2, AstUtils.getAllOccurencesOf("IF_CLAUSE", tree).size());

    }

    @DisplayName("Detects dialect of repeat do properly")
    @Test
    public void t3() throws Throwable {
        String code = "echo a; repeat 4 do \n echo b \n done; echo c;";

        Set<ShellDialect> expectedDialects = new HashSet<>();
        expectedDialects.add(ShellDialect.ZSH);
        assertEquals(expectedDialects, ShellParser.detectDialect(code));

        Set<ShellDialect> notExpectedDialects = new HashSet<>();
        notExpectedDialects.add(ShellDialect.POSIX);

        assertNotEquals(notExpectedDialects, ShellParser.detectDialect(code));

    }

    @DisplayName("Function collection works")
    @Test
    public void t4() {
        String code = "echo a; someFunction x; anotherFunction mo; echo c;";
        AstNode root = ShellParser.parse(code);
        assertEquals(4, AstUtils.getAllFunctionReferencesInScope(root).size());
    }


    @DisplayName("assignments do not get collected as functions")
    @Test
    public void t5() {
        String code = "a=1; b=2; c=3;";
        AstNode root = ShellParser.parse(code);
        assertEquals(0, AstUtils.getAllFunctionReferencesInScope(root).size());
    }


    @DisplayName("function call in between assignments gets collected")
    @Test
    public void t6() {
        String code = "a=1; echo mumble; c=3;";
        AstNode root = ShellParser.parse(code);
        assertEquals(1, AstUtils.getAllFunctionReferencesInScope(root).size());
    }


}
