/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package de.iteratec.sonar.shell.ast;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.GenericTokenType;
import com.sonar.sslr.api.TokenType;
import com.sonar.sslr.impl.Lexer;
import com.sonar.sslr.impl.channel.BlackHoleChannel;
import com.sonar.sslr.impl.channel.RegexpChannel;
import de.iteratec.sonar.shell.channel.*;

/**
 * This is the class for the lexical analysis of shell scripts.
 *
 * @author Gerd Neugebauer <mailto:Gerd.Neugebauer@iteratec.de>
 */
public final class ShellLexer {

    /**
     * reserves specific words as keywords. this is necessary because
     * it invalidates their usage in other contexts aside of intended usage, e.g.
     * an incomplete if-clause (like 'if true then') could be parsed a simple command
     * with if as the command word. keep in mind that if a keyword is getting added, it also
     * needs to be added into the KEYWORD-branch of {@link ShellGrammar}. Else,
     * commands like 'echo if true then' wouldn't parse, even though they are valid.
     */
    public enum Keywords implements TokenType {

        CASE("case"), //
        COPROC("coproc"), //
        DO("do"), //
        DONE("done"), //
        ELIF("elif"), //
        ELSE("else"), //
        ESAC("esac"), //
        FI("fi"), //
        FOR("for"), //
        FUNCTION("function"), //
        IF("if"), //
        IN("in"), //
        SELECT("select"), //
        THEN("then"), //
        TIME("time"), //
        UNTIL("until"), //
        WHILE("while"), //
        REPEAT("repeat"), //
        END("end"), //
        ENDIF("endif"), //
        SWITCH("switch"), //
        BREAKSW("breaksw"), //
        ENDSW("endsw"), //
        DEFAULT("default");

        public static String[] keywordValues() {

            Keywords[] keywordsEnum = Keywords.values();
            String[] keywords = new String[keywordsEnum.length];
            for (int i = 0; i < keywords.length; i++) {
                keywords[i] = keywordsEnum[i].getValue();
            }
            return keywords;
        }

        private final String value;

        private Keywords(String value) {

            this.value = value;
        }

        @Override
        public String getName() {

            return name();
        }

        @Override
        public String getValue() {

            return value;
        }

        @Override
        public boolean hasToBeSkippedFromAst(AstNode node) {

            return false;
        }

    }

    public enum Literals implements TokenType {

        INTEGER, //Number that has more than one digit
        DIGIT, //Number that has only a single digit
        OCTAL, //Number that is prefixed with a zero, with all subsequent digits ranging from 0-7
        HEXADECIMAL, //Number that is prefixed with an 0x, with all subsequent digits ranging from 0-15
        DELIMITER, //space between two tokens containing no characters
        WHITE_SPACE, //a delimiter in double-quote-context
        HERE_DOCUMENT_TOKEN,
        ESCAPED_WHITE_SPACE; //

        @Override
        public String getName() {

            return name();
        }

        @Override
        public String getValue() {

            return name();
        }

        @Override
        public boolean hasToBeSkippedFromAst(AstNode node) {

            //if delimiter, then remove!
            return this.name().equals("DELIMITER");

        }

    }

    public enum Punctuators implements TokenType {

        ESCAPED_ESCAPE("\\\\"),
        ESCAPED_DOLLAR("\\$"),
        ESCAPE("\\"), //
        DOLLAR("$"), //
        BANG("!"), //
        PIPE("|"), //
        AND("&&"), //
        OR("||"), //
        SLASH("/"), //
        DOUBLE_SEMI_AND(";;&"),
        SEMICOLON_AND(";&"),
        AMPERSAND("&"), //
        COLON(":"), //
        SEMICOLON(";"), //
        PAREN_L("("), //
        PAREN_R(")"), //
        BRACE_L("{"), //
        BRACE_R("}"), //
        DOUBLE_BRACKET_L("[["), //
        BRACKET_L("["), //
        DOUBLE_BRACKET_R("]]"), //
        BRACKET_R("]"), //
        EQUALS("="), //
        DOUBLE_COMMA(",,"),
        COMMA(","), //
        ADD("+"), //
        INCREMENT("++"), //
        SUB("-"), //
        DECREMENT("--"), //
        MUL("*"), //
        DIV("/"), //
        EQ("=="), //
        NE("!="), //
        LT("<"), //
        LE("<="), //
        GT(">"), //
        GE(">="), //
        DOUBLE_GT(">>"), //
        DOUBLE_LT("<<"), //
        PIPE_AND("|&"), //
        GREATER_AND(">&"), //
        LESS_AND("<&"), //
        LESS_GREATER("<>"), //
        DOUBLE_LESS_DASH("<<-"), //
        DOUBLE_GREATER_DASH(">>-"), //
        DOUBLE_SEMI(";;"), //
        CLOBBER(">|"), //
        DOUBLE_PERCENTAGE("%%"), //
        PERCENTAGE("%"), //
        DOUBLE_HASH("##"), //
        HASH("#"), //
        COLON_SUB(":-"), //
        COLON_ADD(":+"), //
        COLON_QMARK(":?"), //
        QMARK("?"), //
        COLON_EQUALS(":="), //
        AT_SIGN("@"), //
        DOUBLE_POWER("^^"),
        POWER("^"), //
        TILDE("~"), //
        DOUBLE_LESS_EQUALS("<<="), //
        DOUBLE_GREATER_EQUALS(">>="), //
        MUL_EQUALS("*="), //
        ADD_EQUALS("+="), //
        SLASH_EQUALS("/="), //
        SUB_EQUALS("-="), //
        EQUALS_TILDE("=~"), //
        PERCENTAGE_EQUALS("%="), //
        AMPERSAND_EQUALS("&="), //
        POWER_EQUALS("^="), //
        PIPE_EQUALS("|="), //
        DOUBLE_QUOTE("\""), //
        BACKTICK("`"), //
        SINGLE_QUOTE("'"),
        TRIPLE_LT("<<<");

        private final String value;

        Punctuators(String value) {

            this.value = value;
        }

        @Override
        public String getName() {

            return name();
        }

        @Override
        public String getValue() {

            return value;
        }

        @Override
        public boolean hasToBeSkippedFromAst(AstNode node) {

            return false;
        }

    }

    /**
     * The method <code>create</code> assembles a lexer.
     *
     * @return the new lexer
     */
    public static Lexer create() {

        LexerState lexerState = new LexerState();

        return Lexer.builder().withFailIfNoChannelToConsumeOneCharacter(true)
                .withChannel(new QuoteChannel(lexerState))
                .withChannel(new StatefulCommentRegexpChannel("[\t\r\n ]+#[ ]*.*", lexerState))
                .withChannel(new RegexpChannel(Literals.DIGIT,
                        "\\b[0-9]\\b"))
                .withChannel(new RegexpChannel(Literals.OCTAL,
                        "\\b[0][0-7]+\\b"))
                .withChannel(new RegexpChannel(Literals.HEXADECIMAL,
                        "\\b0x[0-9a-eA-E]+\\b"))
                .withChannel(new RegexpChannel(Literals.INTEGER,
                        "\\b[1-9][0-9]+\\b"))
                .withChannel(new BlackHoleChannel(
                        "\\\\[\\s]+"))
                .withChannel(new SingleQuoteIdentifierChannel(".+?(?=')", lexerState))
                .withChannel(new StatefulPunctuatorChannel(lexerState, Punctuators.values()))
                .withChannel(new HereDocumentChannel("[0-9ßä.öa-zA-Z_][ßöa-zA-Z_0-9.]*", lexerState))
                .withChannel(new StatefulIdentifierAndKeywordChannel(
                        "[0-9ßä.öa-zA-Z_][ßöa-zA-Z_0-9.]*", true, lexerState,
                        Keywords.values()))
                .withChannel(new StatefulDelimiterChannel("[ \t]+", lexerState))
                .withChannel(new StatefulBlackHoleChannel("[ \t\r]+",
                        "[ \n\t\r]+", lexerState))
                .withChannel(new RegexpChannel(GenericTokenType.EOL,
                        "\n"))
                .build();

    }

}
