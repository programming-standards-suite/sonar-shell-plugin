package de.iteratec.sonar.shell.st;

public enum DeclarationType {

    FUNCTION, VARIABLE

}
