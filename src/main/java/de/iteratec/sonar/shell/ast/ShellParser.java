/*
 * SonarSource Language Recognizer
 * Copyright (C) 2010-2017 SonarSource SA
 * mailto:info AT sonarsource DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.iteratec.sonar.shell.ast;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.Grammar;
import com.sonar.sslr.api.Token;
import com.sonar.sslr.impl.Lexer;
import com.sonar.sslr.impl.Parser;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.ShellDialect;
import de.iteratec.sonar.shell.util.ShellFeature;
import de.iteratec.sonar.shell.util.ShellFeatures;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class ShellParser {

    private static final Parser<Grammar> P = ShellParser.create();

    public static Parser<Grammar> create() {
        return Parser.builder(ShellGrammar.create())
                .withLexer(ShellLexer.create()).build();

    }

    public static AstNode parseFile(String filePath) {

        File file = FileUtils.toFile(ShellParser.class.getResource(filePath));
        if (file == null || !file.exists()) {
            throw new AssertionError(
                    "The file \"" + filePath + "\" does not exist.");
        }

        return P.parse(file);

    }

    public static AstNode parse(String code) {

        return P.parse(code);

    }

    /**
     * Detects dialect based on the features found in container-class ShellFeatures.java.
     * Either relies on the mere presence of a token/specific keyword inside of
     * an affected branch OR the structure of some branch to
     *
     * @param code script that is to be turned to a tree
     * @return the dialects this tree could belong to. If empty set is returned, then
     * two features belonging to two different dialects have been used.
     * @throws IOException due to usage of GSON
     */
    @SuppressWarnings("Duplicates")
    public static Set<ShellDialect> detectDialect(String code)
            throws IOException {

        AstNode root = P.parse(code);

        return detectDialect(root);
    }


    public static Set<ShellDialect> detectDialect(AstNode root)
            throws IOException {

        Set<ShellDialect> potentialDialects = new HashSet<>(
                Arrays.asList(ShellDialect.values()));
        potentialDialects = new HashSet<>(
                potentialDialects);

        ShellFeatures features = new ShellFeatures();
        features.getFeaturesFromJson();
        for (ShellFeature f : features.getFeatures()) {

            if (astContainsFeatureDefiningElement(root, f)) {
                // mere presence of affected element is enough for indication
                // of a dialect
                onlyKeepDialectsThatSupportThisFeature(potentialDialects, f);

            } else {
                List<AstNode> detectedBranches = AstUtils.getAllOccurencesOf(
                        f.getAffectedElement(), root);
                if (!detectedBranches.isEmpty()) {
                    //ast contains occurences of the feature
                    for (AstNode branch : detectedBranches) {

                        Set<String> stringifiedBranch = stringifyBranch(branch.getChildren());

                        if (branchMatchesToFeature(f, stringifiedBranch)) {
                            onlyKeepDialectsThatSupportThisFeature(potentialDialects, f);
                        }
                    }
                }
            }

        }

        return potentialDialects;
    }


    /**
     * a feature defining element is one whose mere occurence indicates the presence of the feature.
     *
     * @param root
     * @param f
     * @return
     */
    private static boolean astContainsFeatureDefiningElement(AstNode root, ShellFeature f) {
        return f.getComponentsOfAffectedElements().isEmpty()
                && f.getIndicatingKeyword() == null
                && AstUtils.containsToken(f.getAffectedElement(), root);
    }

    private static boolean branchMatchesToFeature(ShellFeature f, Set<String> stringifiedBranch) {

        HashSet<String> componentsOfAffectedElementSet = new HashSet<>(
                f.getComponentsOfAffectedElements());

        if (stringifiedBranch
                .contains(f.getIndicatingKeyword())) {
            // feature is found in AST AND the indicating keyword has been detected
            return true;
        } else if (componentsOfAffectedElementSet
                .equals(stringifiedBranch)) {
            //structure of affected element matches the detected branch
            return true;
        } else {
            return false;
        }
    }

    private static void onlyKeepDialectsThatSupportThisFeature(Set<ShellDialect> potentialDialectsUnbacked, ShellFeature f) {
        potentialDialectsUnbacked
                .retainAll(f.getSupportedDialects());
    }

    private static Set<String> stringifyBranch(List<AstNode> branch) {
        return branch.stream()
                .map(x -> x.getName().equals("IDENTIFIER")
                        ? x.getTokenValue()
                        : x.getName())
                .collect(Collectors.toSet());
    }

    /**
     * takes the script and attempts to fully parse it.
     *
     * @param code script that is to be parsed
     * @return the tree if the script has been parsed without any remains
     */
    public static AstNode attemptFullParse(String code) {

        if (hasParsedAllTokens(code)) {
            return P.parse(code);
        } else {
            return null;
        }
    }

    private ShellParser() {

    }

    /**
     * compares lexer output with with parser output. For this, it compares
     * the number of generated tokens after lexing with the volume of the tree.
     * This is indicated by the {@link AstNode#getToIndex()} used on the root.
     * Since it is an index, we add a constant of one to make it comparable with
     * the size.
     *
     * @param code script that is to be lexed and parsed
     * @return true if all tokens have been built into the tree
     */
    public static boolean hasParsedAllTokens(String code) {

        Lexer sl = ShellLexer.create();
        List<Token> tokens = sl.lex(code);

        AstNode tree = P.parse(code);

        return (tokens.size()) == tree.getToIndex() + 1;

    }

}
