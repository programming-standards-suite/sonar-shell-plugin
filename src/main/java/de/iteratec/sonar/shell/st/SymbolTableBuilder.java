package de.iteratec.sonar.shell.st;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.util.AstUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public abstract class SymbolTableBuilder {

    public static HashMap<AstNode, SymbolTable> buildTables(AstNode root) {

        HashMap<AstNode, SymbolTable> symbolTables = new HashMap();

        Stack<AstNode> stack = new Stack();

        stack.addAll(root.getChildren());

        AstNode rootScope = root.getFirstDescendant(ShellGrammar.COMPLETE_COMMANDS);
        symbolTables.put(rootScope, new SymbolTable(null, rootScope.getDescendants(ShellGrammar.COMPOUND_LIST), 0));

        //building the very first symbol table for the root scope found at complete_commands
        while (!stack.isEmpty()) {
            AstNode currentNode = stack.pop();
            if (!currentNode.is(ShellGrammar.COMPOUND_LIST)) {

                if (currentNode.is(ShellGrammar.ASSIGNMENT_LEFT_SIDE)) {

                    SymbolTable st = symbolTables.get(rootScope);
                    st.addVariableDeclaration(currentNode);
                    symbolTables.put(rootScope, st);

                } else if (currentNode.is(ShellGrammar.FUNCTION_DEFINITION)) {

                    SymbolTable st = symbolTables.get(rootScope);
                    st.addFunctionDeclaration(currentNode);
                    symbolTables.put(rootScope, st);

                }

                if (currentNode.hasChildren()) {
                    stack.addAll(currentNode.getChildren());
                }
            }
        }

        List<AstNode> scopes = root.getDescendants(ShellGrammar.COMPOUND_LIST);

        for (AstNode scope : scopes) {
            AstNode parent = AstUtils.findParentScope(scope);
            SymbolTable parentST = symbolTables.get(parent);
            SymbolTable scopeST = new SymbolTable(parentST, scope.getDescendants(ShellGrammar.COMPOUND_LIST), parentST.getComplexity() + 1);
            symbolTables.put(scope, scopeST);
        }

        stack.addAll(scopes);

        //building all subsequent scopes
        while (!stack.isEmpty()) {

            AstNode currentNode = stack.pop();
            if (!currentNode.is(ShellGrammar.COMPOUND_LIST)) {

                if (currentNode.is(ShellGrammar.ASSIGNMENT_LEFT_SIDE)) {

                    AstNode parent = AstUtils.findParentScope(currentNode);
                    SymbolTable parentST = symbolTables.get(parent);
                    parentST.addVariableDeclaration(currentNode);
                    symbolTables.put(parent, parentST);

                } else if (currentNode.is(ShellGrammar.FUNCTION_DEFINITION)) {

                    AstNode parent = AstUtils.findParentScope(currentNode);
                    SymbolTable st = symbolTables.get(rootScope);
                    st.addFunctionDeclaration(currentNode);
                    symbolTables.put(parent, st);

                }


                stack.addAll(currentNode.getChildren());

            }

        }

        collectReferencesToDeclarations(symbolTables);
        return symbolTables;

    }

    private static void collectReferencesToDeclarations(HashMap<AstNode, SymbolTable> symbolTables) {
        for (Map.Entry<AstNode, SymbolTable> scopeToSymbolTable : symbolTables.entrySet()) {
            AstNode nodeOfScope = scopeToSymbolTable.getKey();
            List<AstNode> referenceNodes = AstUtils.getAllVariableReferencesInScope(nodeOfScope);
            for (AstNode referenceNode : referenceNodes) {
                scopeToSymbolTable.getValue().checkDeclarationOfReference(referenceNode);
            }
            referenceNodes = AstUtils.getAllFunctionReferencesInScope(nodeOfScope);
            for (AstNode referenceNode : referenceNodes) {
                scopeToSymbolTable.getValue().checkDeclarationOfReference(referenceNode);
            }
        }
    }

}
