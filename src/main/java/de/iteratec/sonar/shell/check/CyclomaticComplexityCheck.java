package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.util.ComplexityCalculator;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Rule(key = "SH002",
        name = "Cyclomatic complexity check",
        description = "Evaluates the complexity of a script against a magic number limit of 7",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
//TODO include this measure so that it is registered as measure
public class CyclomaticComplexityCheck extends AbstractCheckTester {

    static final int MAGIC_COMPLEXITY_LIMIT = 7;

    @Override
    public boolean visitNode(AstNode node) {
        return ComplexityCalculator.calculateComplexity(node, 0) < MAGIC_COMPLEXITY_LIMIT;
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        List<PreciseIssue> issues = new ArrayList<>();

        if (ComplexityCalculator.calculateComplexity(node, 0) > MAGIC_COMPLEXITY_LIMIT) {
            issues.add(
                    new PreciseIssue(
                            IssueLocation.atFileLevel("The complexity of this script is too high, please reduce.")
                    )
            );
        }
        return issues;
    }

    static Set<String> enums = Arrays
            .asList(ShellGrammar.values()).stream()
            .map(shellEnum -> shellEnum.toString())
            .collect(Collectors.toSet());

    @Test
    public void t1() {
        Assertions.assertTrue(enums.contains("IF_CLAUSE"));
    }

    @Test
    public void t2() {
        AstNode ast = ShellParser.create().parse("xo=0;");
        Assertions
                .assertTrue(enums.contains(ast.getName()));
    }

    @Test
    public void t3() {
        String code = "while true; do \n echo 'Something'; done";
        assertEquals(ComplexityCalculator.calculateComplexity(ShellParser.create().parse(code), 0), 1);
        assertThatCheckSucceeds(code);
    }


    @Test
    public void t4() {
        String code = "if true; then \n echo xo \n if true; then \n echo lol \n fi \n elif true; then \n echo ox\n fi";
        assertEquals(ComplexityCalculator.calculateComplexity(ShellParser.create().parse(code), 0), 3);
        assertThatCheckSucceeds(code);
    }

    @Test
    public void t5() {
        String code = "case $i in\r\n" //
                + "        (0) set -- ;;\r\n"
                + "        (1) set -- \"$args0\" ;;\r\n"
                + "        (2) set -- \"$args0\" \"$args1\" ;;\r\n"
                + "        (3) set -- \"$args0\" \"$args1\" \"$args2\" ;;\r\n"
                + "        (4) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" ;;\r\n"
                + "        (5) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" ;;\r\n"
                + "        (6) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" ;;\r\n"
                + "        (7) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" ;;\r\n"
                + "        (8) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" \"$args7\" ;;\r\n"
                + "        (9) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" \"$args7\" \"$args8\" ;;\r\n"
                + "    esac";
        assertEquals(ComplexityCalculator.calculateComplexity(ShellParser.parse(code), 0), 3);
        assertThatCheckSucceeds(code);
    }


}
