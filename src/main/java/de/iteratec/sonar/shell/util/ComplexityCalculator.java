package de.iteratec.sonar.shell.util;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellGrammar;

import java.util.Arrays;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class ComplexityCalculator {

    static Set<String> enums = Arrays
            .asList(ShellGrammar.values()).stream()
            .map(shellEnum -> shellEnum.toString())
            .collect(Collectors.toSet());


    public static int calculateComplexity(AstNode root, int i) {

        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);
        System.out.println(AstUtils.stringifyTree(root));
        int complexity = i;

        while (!queue.isEmpty()) {

            AstNode node = queue.poll();

            if (enums.contains(node.getName())) {

                switch (Enum.valueOf(ShellGrammar.class,
                        node.getName())) {
                    case CASE_CLAUSE:
                        complexity++;
                        break;
                    case CASE_ITEM:
                        complexity++;
                        break;
                    case FOR_CLAUSE:
                        complexity++;
                        break;
                    case DEFAULT_CASE:
                        complexity++;
                        break;
                    case ELSE_PART:
                        complexity++;
                        break;
                    case IF_CLAUSE:
                        complexity++;
                        break;
                    case UNTIL_CLAUSE:
                        complexity++;
                        break;
                    case WHILE_CLAUSE:
                        complexity++;
                        break;
                    case FUNCTION_DEFINITION:
                        complexity++;
                        break;
                    default:
                        break;
                }
            }
            queue.addAll(node.getChildren());

        }
        return complexity;
    }

}
