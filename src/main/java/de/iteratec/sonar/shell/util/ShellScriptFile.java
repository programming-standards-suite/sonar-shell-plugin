/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 *
 */
public class ShellScriptFile implements ShellScriptFileInterface {

    File file;

    ShellDialect dialect;

    ShellScriptFile(File file) {
        this.file = file;

        // TODO dynamically assign dialect type
        dialect = ShellDialect.POSIX;

    }

    /* (non-Javadoc)
     * @see de.iteratec.sonar.shell.sslr.ScriptInterface#getName()
     */
    @Override
    public String getName() {
        return file.getName();
    }

    /* (non-Javadoc)
     * @see de.iteratec.sonar.shell.sslr.ScriptInterface#getContent()
     */
    @Override
    public ShellDialect getDialect() {
        return this.dialect;
    }

    /* (non-Javadoc)
     * @see de.iteratec.sonar.shell.sslr.ScriptInterface#getDialect()
     */
    @Override
    public String getContent() {
        try {
            return new String(Files.readAllBytes(file.toPath()),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Cannot read " + file, e);
        }
    }

}
