/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
package de.iteratec.sonar.shell.util;

/**
 * This enumeration names some of the shell script dialects.
 *
 * @author <a href="mailto:Gerd.Neugebauer@iteratec.de">Gerd Neugebauer</a>
 */
public enum ShellDialect {

    POSIX, //
    SH, //
    BASH, //
    CSH, //
    TCSH, //
    KSH, //
    KSH93, //
    PDKSH, //
    ASH, //
    ZSH, //
    FISH

}
