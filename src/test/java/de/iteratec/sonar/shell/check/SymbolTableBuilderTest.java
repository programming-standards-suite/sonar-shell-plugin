package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.Grammar;
import com.sonar.sslr.impl.Parser;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.st.SymbolTable;
import de.iteratec.sonar.shell.st.SymbolTableBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashMap;

import static de.iteratec.sonar.shell.ast.ShellGrammar.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SymbolTableBuilderTest {

    Parser<Grammar> parser;

    @BeforeEach
    public void setup() {
        parser = ShellParser.create();
    }

    @DisplayName("correct number of scopes get collected")
    @Test
    public void t1() {
        String code = "a=1;b=2;cool_function(){\n c=101\n}";
        HashMap map = SymbolTableBuilder.buildTables(parser.parse(code));
        assertEquals(2, map.size());
    }

    @DisplayName("correct number of scopes get collected")
    @Test
    public void t2() {
        String code = "a=3;\n if [ $BRANCH_NAME == \"develop\" ] ; then\n echo ${a}\n fi\n";
        HashMap<AstNode, SymbolTable> map = SymbolTableBuilder.buildTables(parser.parse(code));
        assertEquals(3, map.size());
        Collection<SymbolTable> tables = map.values();
        assertTrue(tables.stream().allMatch(entry ->
                entry.allVariablesReferenced()
        ));
    }

    @DisplayName("correct number of scopes get collected")
    @Test
    public void t3() {
        String code = "a=3;\n functionny() {\n if [ $BRANCH_NAME == \"develop\" ] ; then\n echo ${a} \n fi\n }\n functionny\n";
        AstNode root = parser.parse(code);
        HashMap<AstNode, SymbolTable> map = SymbolTableBuilder.buildTables(root);
        assertEquals(4, map.size());
        Collection<SymbolTable> tables = map.values();
        assertTrue(tables.stream().allMatch(entry ->
                entry.allVariablesReferenced()
        ));
        assertEquals(0, map.get(root.getFirstChild(COMPLETE_COMMANDS)).getComplexity());
        AstNode testNode = root.getFirstDescendant(COMPOUND_LIST).getFirstDescendant(COMPOUND_LIST);
        assertTrue(testNode.hasDescendant(SINGLE_BRACKET_TEST));
        assertEquals(2, map.get(testNode).getComplexity());
    }


}