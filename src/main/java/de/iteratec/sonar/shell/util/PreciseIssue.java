package de.iteratec.sonar.shell.util;

import javax.annotation.Nullable;
import java.util.*;

public class PreciseIssue {

    private final IssueLocation primaryLocation;
    private Integer cost;
    private final List<IssueLocation> secondaryLocations;

    public PreciseIssue(IssueLocation primaryLocation) {
        this.primaryLocation = primaryLocation;
        this.secondaryLocations = new ArrayList<>();
    }

    @Nullable
    public Integer cost() {
        return cost;
    }

    public PreciseIssue withCost(int cost) {
        this.cost = cost;
        return this;
    }

    public IssueLocation primaryLocation() {
        return primaryLocation;
    }

    public PreciseIssue secondary(IssueLocation issueLocation) {
        secondaryLocations.add(issueLocation);
        return this;
    }

    public List<IssueLocation> secondaryLocations() {
        return secondaryLocations;
    }

    public static <T> Set<T> immutableSet(T... el) {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(el)));
    }
}