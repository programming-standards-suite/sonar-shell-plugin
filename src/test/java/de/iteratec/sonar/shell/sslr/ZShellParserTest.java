/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.sslr;

import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class ZShellParserTest extends AbstractParserTester {

	@DisplayName("ZShell-compliant repeat call employing keyword 'REPEAT'")
	@Test
	public void t1() throws Exception, IOException {
		String code = "repeat 10 do \n echo 'hi' \n done";
		assertCodeIsNotOfDialect(code, ShellDialect.TCSH);

	}

	@DisplayName("ZShell-compliant shift calls")
	@Test
	public void t2() throws Throwable {
		String code = "shift; shift 10;";
		assertExistenceOfToken("SIMPLE_COMMAND", code);
		// shift <<NUMBER>> is not tcsh-compliant
		assertCodeIsOfDialect(code, ShellDialect.ZSH, ShellDialect.BASH,
				ShellDialect.KSH);
	}
}
