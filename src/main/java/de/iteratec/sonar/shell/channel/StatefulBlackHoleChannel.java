/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Black-hole channel which has specifically been created to remove newline characters in
 * quoted contexts. These cause the parser to get stuck in the process of AST generation.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class StatefulBlackHoleChannel extends Channel<Lexer> {

	private LexerState ls;

	private Matcher regularMatcher;

	private Matcher alternativeMatcher;

	public StatefulBlackHoleChannel(String regularRegExp,
									String alternativeRegExp, LexerState ls) {
		regularMatcher = Pattern.compile(regularRegExp).matcher("");
		alternativeMatcher = Pattern.compile(alternativeRegExp).matcher("");
		this.ls = ls;
	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {
		if (ls.isInDoubleQuotes()) {
			return code.popTo(alternativeMatcher,
					EmptyAppendable.INSTANCE) != -1;
		} else {
			return code.popTo(regularMatcher, EmptyAppendable.INSTANCE) != -1;
		}
	}

	private static class EmptyAppendable implements Appendable {

		private static final Appendable INSTANCE = new EmptyAppendable();

		@Override
		public Appendable append(CharSequence csq) {
			return this;
		}

		@Override
		public Appendable append(char c) {
			return this;
		}

		@Override
		public Appendable append(CharSequence csq, int start, int end) {
			return this;
		}
	}

}
