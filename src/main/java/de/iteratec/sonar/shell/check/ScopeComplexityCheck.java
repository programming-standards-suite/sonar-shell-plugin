package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.st.SymbolTable;
import de.iteratec.sonar.shell.st.SymbolTableBuilder;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Rule(key = "SH007",
        name = "Scope complexity check",
        description = "checks if the script does not have too many scopes",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class ScopeComplexityCheck extends AbstractCheckTester {

    private List<PreciseIssue> issues = new ArrayList<>();

    @Override
    public boolean visitNode(AstNode node) {
        return false;
    }

    int magicComplexityLimit = 4;

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        HashMap<AstNode, SymbolTable> tables = SymbolTableBuilder.buildTables(node);

        int maxScopeComplexity = 0;
        for (SymbolTable table : tables.values()) {
            int complexity = table.getComplexity();
            if (maxScopeComplexity < complexity) {
                maxScopeComplexity = complexity;
            }
        }

        if (magicComplexityLimit < maxScopeComplexity) {
            PreciseIssue issue = new PreciseIssue(IssueLocation.atFileLevel("This script has too high scope complexity. please reduce."));
            for (Map.Entry<AstNode, SymbolTable> scopeToTable : tables.entrySet()) {
                if (!scopeToTable.getKey().is(ShellGrammar.COMPLETE_COMMANDS)) {
                    issue.secondary(IssueLocation.preciseLocation(scopeToTable.getKey(), "Complexity: " + scopeToTable.getValue().getComplexity()));
                }
            }
            issues.add(issue);
        }

        return issues;
    }

    @Test
    public void t1() {
        String code = "" +
                "xyo=123\n" +
                "foo(){" +
                "xo='ox'\n"
                + "uwu=xoxoxo\n"
                + "echo ${xo}\n"
                + "echo ${uwu}\n" +
                "}\n" +
                "if [ 1 -eq 1 ]; then\n"
                + "echo ${xyo}\n" +
                "else\n" + "axaxa=31\n" + "echo ${xyo}\n"
                + "echo ${axaxa}\n" +
                "foo\n" + "fi";
        assertThatIssuesAreGenerated(code);
    }
}
