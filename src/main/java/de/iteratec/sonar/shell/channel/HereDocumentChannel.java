/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import static de.iteratec.sonar.shell.ast.ShellLexer.Literals.HERE_DOCUMENT_TOKEN;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * generates the content of single quotes. if in single-quote-context, simply turns all
 * subsequent token into a single word, until the closing single-quote has been detected.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class HereDocumentChannel extends Channel<Lexer> {

	LexerState ls;

	private final StringBuilder tmpBuilder = new StringBuilder();
	private final Matcher matcher;
	private final Token.Builder tokenBuilder = Token.builder();

	/**
	 * @throws java.util.regex.PatternSyntaxException if the expression's syntax
	 *                                                is invalid
	 */
	public HereDocumentChannel(String regexp, LexerState ls) {
		matcher = Pattern.compile(regexp).matcher("");
		this.ls = ls;
	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {

		if (ls.isHereDocumentTokenRequired() || !ls.getHereDocumentTokenValueList().isEmpty()) {

			StringBuilder potentialHereToken = new StringBuilder();

			//end peeking when whitespace or newline has been detected
			code.peekTo(toMatch -> ' ' == toMatch || '\n' == toMatch, potentialHereToken);

			if (ls.getHereDocumentTokenValueList().contains(potentialHereToken.toString())) {

				code.popTo(matcher, tmpBuilder);

				String word = tmpBuilder.toString();
				String wordOriginal = word;

				Token token;
				int tokenCount = lexer.getTokens().size();
				String lastTokenValue = lexer.getTokens().get(tokenCount - 1).getValue();

				char nextTokenValue = (char) code.peek();

				if (lastTokenValue.equals("\n") && nextTokenValue == '\n') {
					//only '\n HERE_DOCUMENT_TOKEN \n' can be the end of the here document
					token = tokenBuilder
							.setType(HERE_DOCUMENT_TOKEN)
							.setValueAndOriginalValue(word, wordOriginal)
							.setURI(lexer.getURI())
							.setLine(code.getPreviousCursor().getLine())
							.setColumn(code.getPreviousCursor().getColumn()).build();
					lexer.addToken(token);
					ls.getHereDocumentTokenValueList().remove(word);
					if (ls.getHereDocumentTokenValueList().isEmpty()) {
						ls.setHereDocumentTokenRequired(false);
					}
					potentialHereToken.delete(0, potentialHereToken.length());
					tmpBuilder.delete(0, tmpBuilder.length());
					return true;
				}
			}

			if (ls.isHereDocumentTokenRequired()) {
				code.popTo(matcher, tmpBuilder);

				String word = tmpBuilder.toString();
				String wordOriginal = word;

				Token token;
				// following token needs to be a here document
				token = tokenBuilder
						.setType(HERE_DOCUMENT_TOKEN)
						.setValueAndOriginalValue(word, wordOriginal)
						.setURI(lexer.getURI())
						.setLine(code.getPreviousCursor().getLine())
						.setColumn(code.getPreviousCursor().getColumn()).build();
				ls.setHereDocumentTokenRequired(false);
				ls.getHereDocumentTokenValueList().add(token.getValue());
				lexer.addToken(token);

				potentialHereToken.delete(0, potentialHereToken.length());
				tmpBuilder.delete(0, tmpBuilder.length());
				return true;
			}
			potentialHereToken.delete(0, potentialHereToken.length());
		}

		return false;
	}


}
