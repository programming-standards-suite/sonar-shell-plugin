package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;
import org.sonar.check.RuleProperty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static de.iteratec.sonar.shell.ast.ShellParser.detectDialect;

@Rule(key = "SH006",
        name = "Dialect checking",
        description = "Assures that the script conforms to a specific dialect of a shell.",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class ScriptIsOfDialectCheck extends AbstractCheckTester {

    String message = "This script is not conforming to the passed dialect.";

    final static String DEFAULT_DIALECT = "BASH";

    @RuleProperty(
            key = "dialect",
            description = "Checks if the usage of the constructs in the script" +
                    "are compliant to a specific shell dialect. " +
                    "The user may select the dialect that the script should conform to by customizing the rule." +
                    "Allowed values are BASH, TCSH, ZSH, POSIX.",
            defaultValue = "" + DEFAULT_DIALECT
    )
    String expectedDialect = DEFAULT_DIALECT;

    ArrayList<PreciseIssue> issues = new ArrayList();


    @Override
    public boolean visitNode(AstNode node) {
        return true;
    }

    private boolean isDialect(String possibleDialect) {
        for (ShellDialect dialect : ShellDialect.values()) {
            if (dialect.toString().equals(possibleDialect))
                return true;
        }
        PreciseIssue issue = new PreciseIssue(IssueLocation.atFileLevel(
                "The passed dialect in the customized rule in " +
                        "rule SH006 has not been recognized by the system. There might be a " +
                        "typo or it does not exist. Please, try again, using either BASH, POSIX, TCSH or ZSH " +
                        "as value."));
        issues.add(issue);
        return false;
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        Set<String> detectedDialects = null;
        if (isDialect(expectedDialect)) {
            try {
                detectedDialects = detectDialect(node).stream().map(enumDialect
                        -> enumDialect.toString()
                ).collect(Collectors.toSet());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (!detectedDialects.contains(expectedDialect)) {
                PreciseIssue issue = new PreciseIssue(IssueLocation.atFileLevel("This script is not of the expected dialect."));
                issues.add(issue);
            }
        }
        return issues;
    }


    @DisplayName("checking generic shell script should match all")
    @Test
    public void t1() {
        String code = "a=1; b=2; c=3; my_function(){\n echo $((a+b));\n}\n";
        assertThatNoIssuesAreGenerated(code);

    }

    @DisplayName("repeat do echo xo done is only allowed in ZSh")
    @Test
    public void t2() {
        String code = "repeat 100 do \n echo 'lol'\n done\n";
        assertThatIssuesAreGenerated(code);
    }


}
