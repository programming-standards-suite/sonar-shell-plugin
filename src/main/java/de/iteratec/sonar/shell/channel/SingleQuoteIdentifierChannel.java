/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;
import de.iteratec.sonar.shell.util.ChannelUtils;
import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sonar.sslr.api.GenericTokenType.IDENTIFIER;

/**
 * generates the content of single quotes. if in single-quote-context, simply turns all
 * subsequent token into a single word, until the closing single-quote has been detected.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class SingleQuoteIdentifierChannel extends Channel<Lexer> {

	LexerState ls;

	private final StringBuilder tmpBuilder = new StringBuilder();
	private final Matcher matcher;
	private final Token.Builder tokenBuilder = Token.builder();

	/**
	 * @throws java.util.regex.PatternSyntaxException if the expression's syntax
	 *                                                is invalid
	 */
	public SingleQuoteIdentifierChannel(String regexp, LexerState ls) {
		matcher = Pattern.compile(regexp).matcher("");
		this.ls = ls;
	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {

		if (ls.isInSingleQuotes() && code.popTo(matcher, tmpBuilder) > 0) {

			//in order to deal with cases like 'hello \' world'
			while (!ChannelUtils.confirmUnescapedCharacter(code)) {
				code.pop(tmpBuilder);
				code.popTo(matcher, tmpBuilder);
			}

			String word = tmpBuilder.toString();
			String wordOriginal = word;

			Token token = tokenBuilder
					.setType(IDENTIFIER)
					.setValueAndOriginalValue(word, wordOriginal)
					.setURI(lexer.getURI())
					.setLine(code.getPreviousCursor().getLine())
					.setColumn(code.getPreviousCursor().getColumn()).build();

			lexer.addToken(token);

			tmpBuilder.delete(0, tmpBuilder.length());
			return true;

		}
		

		return false;
	}

}
