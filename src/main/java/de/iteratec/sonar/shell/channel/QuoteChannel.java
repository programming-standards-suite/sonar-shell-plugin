/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.api.TokenType;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;
import de.iteratec.sonar.shell.ast.ShellLexer.Punctuators;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import static de.iteratec.sonar.shell.util.ChannelUtils.confirmUnescapedCharacter;

/**
 * Switches lexer state, depending on if a properly unescaped
 * single/double-quote gets detected. Does not consume any characters. Assures
 * that the respective quote context can only occur if the other context is not
 * active, e.g. no single-quoted-word while in a double-quoted-word.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class QuoteChannel extends Channel<Lexer> {

	char ch;

	private LexerState lexerState;

	/**
	 * @param lexerState
	 */
	public QuoteChannel(LexerState lexerState) {
		this.lexerState = lexerState;
	}

	@Override
	public boolean consume(CodeReader code, Lexer output) {
		int line = code.getLinePosition();
		int column = code.getColumnPosition();

		ch = (char) code.peek();

		if (ch == '\"' && !lexerState.isInSingleQuotes()
				&& confirmUnescapedCharacter(code)) {

			addToken("\"", Punctuators.DOUBLE_QUOTE, code, output, line,
					column);

			//if lexer was in double-quote-context, we found a closing double-quote
			//therefore turn off double-quote-context, else turn it on
			lexerState.setInDoubleQuotes(!lexerState.isInDoubleQuotes());

			return true;
		} else if (ch == '\'' && !lexerState.isInDoubleQuotes()
				&& confirmUnescapedCharacter(code)) {

			addToken("\'", Punctuators.SINGLE_QUOTE, code, output, line,
					column);

			//if lexer was in single-quote-context, we found a closing single-quote
			//therefore turn off single-quote-context, else turn it on
			lexerState.setInSingleQuotes(!lexerState.isInSingleQuotes());

			return true;
		} else {
			return false;
		}

	}

	/**
	 * Auxiliary method that helps building of tokens
	 *
	 * @param originalValue
	 * @param type
	 * @param code
	 * @param output
	 * @param line
	 * @param column
	 */
	private void addToken(String originalValue, TokenType type, CodeReader code,
						  Lexer output, int line, int column) {
		output.addToken(Token.builder().setLine(line).setColumn(column)
				.setURI(output.getURI()).setValueAndOriginalValue(originalValue)
				.setType(type).build());

		code.pop();
	}

}
