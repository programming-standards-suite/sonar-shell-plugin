#!/usr/bin/env bash

WORKING_DIR="$(pwd)"
DOCKER_DIR="$(pwd)"

function log()
{
    echo "${1}" | tee --append "${WORKING_DIR}/checkDockerDeps/${2:-result.log}"
}

function debug()
{
    cat &>> "${WORKING_DIR}/checkDockerDeps/${1:-debug.log}"
}

function checkImage()
{
    local lineNumber="${1}"
    local check="${2}"
    local line=$(sed --quiet --regexp-extended "${lineNumber} s#(^[[:blank:]]*|[[:blank:]]*$)##gp" "${DOCKER_DIR}/Dockerfile")
    local lineId=$(echo -n "${line}" | sed --regexp-extended "s#[^[:alnum:]]+#_#g")
    log "--------------"
    log "Alter Dockerfile at ${DOCKER_DIR}/Dockerfile"
    cp "${DOCKER_DIR}/"Dockerfile{,.bck}
    log "Remove line #${lineNumber} '${line}' from Dockerfile at ${DOCKER_DIR}/Dockerfile"
    sed --in-place "${lineNumber}d" "${DOCKER_DIR}/Dockerfile"
    log "Run check ${check}"
    exit="${?}"
    log "Exit code is ${exit}"
    if [ "${exit}" == "0" ]
    then
        log "Check succeeded for ${lineId}"
        CHECK_SUCCEEDED="${CHECK_SUCCEEDED} ${lineId}"
    else
        log "Check failed for ${lineId} with exit code ${exit}"
        CHECK_FAILED="${CHECK_FAILED} ${lineId}"
    fi
    log "Reset Dockerfile at ${DOCKER_DIR}/Dockerfile"
    cp "${DOCKER_DIR}/"Dockerfile{.bck,}
}

function checkContainer()
{
    local dep="${1}"
    local replaceDep="${2}"
    local check="${3}"
    log "--------------"
    log "Remove dependency ${dep} inside container"
    yum erase -y "${dep}" &> "${WORKING_DIR}/checkDockerDeps/${dep}.log"
    if [ "${dep}" != "${replaceDep}" ]
    then
        log "Install replacement ${replaceDep}"
        yum install -y "${replaceDep}" &> "${WORKING_DIR}/checkDockerDeps/${replaceDep}.log"
    fi
    eval "( ${check} )" &>> "${WORKING_DIR}/checkDockerDeps/${replaceDep}.log"
    exit="${?}"
    log "Exit code is ${exit}"
    if [ "${exit}" == "0" ]
    then
        log "Check succeeded for ${replaceDep}"
        CHECK_SUCCEEDED="${CHECK_SUCCEEDED} ${replaceDep}"
    else
        log "Check failed for ${replaceDep} with exit code ${exit}"
        CHECK_FAILED="${CHECK_FAILED} ${replaceDep}"
    fi
    if [ "${dep}" != "${replaceDep}" ]
    then
        log "Remove replacement ${replaceDep}"
        yum erase -y "${replaceDep}" &>> "${WORKING_DIR}/checkDockerDeps/${replaceDep}.log"
    fi
    log "Reinstall ${dep}"
    yum install -y "${dep}" &>> "${WORKING_DIR}/checkDockerDeps/${dep}.log"
}

function printHelp()
{
    local filename="$(basename ${0})"
    echo "Alter a Docker container or image to determine dependencies needed to pass the specified verification test"
    echo "Usage: ${filename} [-C working-directory] [-c [-r replace-statement] | -i | -h] -v verification-test alter-arguments..."
    echo "Mode:"
    echo "    -c    check dependencies by uninstalling the packages passed as 'alter-arguments'"
    echo "          use this mode INSIDE a Docker container or pass the verification test as 'docker exec'"
    echo "    -i    remove lines identified by line numbers and passed as 'alter-arguments' from a Dockerfile"
    echo "          this mode will remove one line at a time and executes the given verification test"
    echo "          use this mode OUTSIDE a Docker container, but let the verification test build the image and start the container with the actual test"
    echo "Options:"
    echo "    -C    working directory with a Dockerfile to switch into"
    echo "    -r    expression for 'sed' used to alter the dependency read from 'alter-arguments' for checking the possibility to substitute the dependency"
    echo "          only valid in combination with '-c'"
    echo "    -v    verification statement to check the outcome of the operation performed by a given mode"
    echo "    -h    show this help"
    echo "Examples:"
    echo "    ${filename} -c -v 'docker exec name make build' dep1 dep2"
    echo "    ${filename} -c -v 'docker exec name make build' -r 's/$/-devel/g' dep1 dep2"
    echo "    ${filename} -c -v 'docker exec name make build' \$(cat deps.lst)"
    echo "    ${filename} -i -v 'docker build -t name . && docker run --rm name make build' 15 17"
    echo "    ${filename} -i -v 'docker build -t name . && docker run --rm name make build' \$(seq 15 17)"
    echo "    ${filename} -C some-dir/ -i -v 'docker build -t name . && docker run --rm name make build' \$(seq 15 17)"
}
while getopts "C:civ:r:h" opt
do
    case "${opt}" in
        C)
            WORKING_DIR="$(pwd)"
            DOCKER_DIR=$(readlink -m "${OPTARG}")
            ;;
        c)
            MODE="container"
            ;;
        i)
            MODE="image"
            ;;
        v)
            CHECK_STMT="${OPTARG}"
            ;;
        r)
            REPLACE_DEP="${OPTARG}"
            ;;
        h)
            printHelp
            exit 0
            ;;
        \?)
            printHelp
            exit 1
            ;;
    esac
done

shift "$((${OPTIND} - 1))"
ALTER_ARGS=${@}

mkdir --parents "${WORKING_DIR}/checkDockerDeps/"
pushd . &> /dev/null
cd "${DOCKER_DIR}/"
log "--------------"
log "$(date)"
printHelp | head -1
log "Writing logs to ${WORKING_DIR}/checkDockerDeps/"

case "${MODE}" in
    container)
        for dep in ${ALTER_ARGS[@]}
        do
            if [ -n "${REPLACE_STMT}" ]
            then
                replaceDep="$(echo ${dep} | sed --regexp-extended ${REPLACE_STMT})"
            else
                replaceDep="${dep}"
            fi
            checkContainer "${dep}" "${replaceDep}" "${CHECK_STMT}"
        done
        ;;
    image)
        for lineNumber in ${ALTER_ARGS[0]}
        do
            checkImage "${lineNumber}" "${CHECK_STMT}"
        done
        ;;
    *)
        printHelp
        exit 1
esac

popd &> /dev/null

log
log "Check succeeded for: ${CHECK_SUCCEEDED}"
log "Check failed for: ${CHECK_FAILED}"
log "Logs have been written to ${WORKING_DIR}/checkDockerDeps/"
