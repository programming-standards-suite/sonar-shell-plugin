/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.sslr;

import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class TCShellParserTest extends AbstractParserTester {

	@DisplayName("TCShell-compliant repeat call employing keyword 'REPEAT'")
	@Test
	public void t1() throws Exception {
		String code = "repeat 10 echo 'hi'";
		assertExistenceOfToken("REPEAT_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH);
	}

	@DisplayName("TCShell-compliant repeat call employing keyword 'REPEAT'")
	@Test
	public void t2() throws Exception {
		String code = "shift";
		assertExistenceOfToken("SIMPLE_COMMAND", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH, ShellDialect.BASH,
				ShellDialect.ZSH, ShellDialect.KSH);
	}

	@DisplayName("TCShell-compliant while loop")
	@Test
	public void t3() throws Exception {
		String code = "while ($k < $j)\r\n echo xo \n end";
		assertExistenceOfToken("WHILE_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH, ShellDialect.POSIX);
	}

	@DisplayName("TCShell-compliant if (...) then ... endif")
	@Test
	public void t4() throws Exception {
		String code = "if (true) then \n echo xo \n endif";
		assertExistenceOfToken("IF_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH, ShellDialect.POSIX);
	}

	@DisplayName("if (...) then ... else ... endif")
	@Test
	public void t5() throws Exception {
		String code = "if (true) then \n echo xo \n else \n echo ox \n endif";
		assertExistenceOfToken("IF_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH, ShellDialect.POSIX);
	}

	@DisplayName("if (...) then ... else if (...) then ... else ... endif")
	@Test
	public void t6() throws Exception {
		String code = "if (true) then \n echo xo \n else if "
				+ "(false) then \n echo ox \n else \n echo xxx \n endif";
		assertExistenceOfToken("IF_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH, ShellDialect.POSIX);
	}

	@DisplayName("if (...) then ... else if (...) then ... endif")
	@Test
	public void t7() throws Exception {
		String code = "if (true) then \n echo xo \n else if "
				+ "(false) then \n echo ox \n endif";
		assertExistenceOfToken("IF_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.BASH, ShellDialect.POSIX);
	}

	@DisplayName("tcsh-compliant switch case")
	@Test
	public void t8() throws Exception {
		String code = "switch ($swag)\n case Asd98: \n shift \n breaksw \n endsw";
		assertExistenceOfToken("CASE_CLAUSE", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.ASH, ShellDialect.BASH,
				ShellDialect.POSIX, ShellDialect.ZSH);

	}

	@DisplayName("tcsh-compliant switch case with two cases")
	@Test
	public void t9() throws Exception {
		String code = "switch ($swag)\n" //
				+ " case Asd98: \n" //
				+ "     shift \n" //
				+ "     breaksw \n" //
				+ " case blablabla: \n" //
				+ "     echo '.' \n" //
				+ "     breaksw \n" //
				+ " endsw"; //
		assertCorrectCountOfToken(1, "CASE_CLAUSE", code);
		assertCorrectCountOfToken(2, "CASE_ITEM", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.ASH, ShellDialect.BASH,
				ShellDialect.POSIX, ShellDialect.ZSH);

	}

	@DisplayName("tcsh-compliant switch case employing a default case")
	@Test
	public void t10() throws Exception {
		String code = "switch ($swag)\n" //
				+ " case Asd98: \n" //
				+ "     shift \n" //
				+ "     breaksw \n" //
				+ " default: \n" //
				+ "     echo \"${hello}\" \n" //
				+ "     breaksw \n" //
				+ " endsw"; //
		assertExistenceOfToken("CASE_CLAUSE", code);
		assertExistenceOfToken("DEFAULT_CASE", code);
		assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT", code);
		assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
		assertCodeIsOfDialect(code, ShellDialect.TCSH);
		assertCodeIsNotOfDialect(code, ShellDialect.ASH, ShellDialect.BASH,
				ShellDialect.POSIX, ShellDialect.ZSH);
	}

}
