/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Container for the feature list that is supposed to be read in json format
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class ShellFeatures {

    static Gson g;

    List<ShellFeature> features;

    public ShellFeatures() throws JsonSyntaxException {
        //this method is empty because initialization is usually done via GSON
    }

    /**
     * @return the features
     */
    public List<ShellFeature> getFeatures() {
        return features;
    }

    public void getFeaturesFromJson() throws IOException {
        File initialFile = new File("features.json");
        InputStream targetStream = new FileInputStream(initialFile);
        g = new Gson();
        StringBuilder textBuilder = new StringBuilder();
        try (Reader reader = new BufferedReader(new InputStreamReader
                (targetStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }
        }
        features = g.fromJson(textBuilder.toString(),
                ShellFeatures.class).getFeatures();

    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (ShellFeature f : features) {
            builder.append(f.toString());
            if (features.indexOf(f) != features.size() - 1) {
                builder.append(", ");
            }
        }
        builder.append("]");
        return builder.toString();
    }

}
