package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.st.Declaration;
import de.iteratec.sonar.shell.st.SymbolTable;
import de.iteratec.sonar.shell.st.SymbolTableBuilder;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Rule(key = "SH005",
        name = "Referenced a variable before declaring it",
        description = "There was a reference to a variable before it was actually declared.",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class UsageAfterDeclarationCheck extends AbstractCheckTester {

    List<PreciseIssue> issues = new ArrayList();

    String message = "you referenced to a declaration before actually declaring it. how could you?";

    @Override
    public boolean visitNode(AstNode node) {
        HashMap<AstNode, SymbolTable> symbolTables = SymbolTableBuilder.buildTables(node);

        for (Map.Entry<AstNode, SymbolTable> map : symbolTables.entrySet()) {
            AstNode nodeOfScope = map.getKey();
            List<AstNode> references = AstUtils.getAllVariableReferencesInCurrentScope(nodeOfScope);

            return references.stream().allMatch(reference ->
                    this.verifyUsageAfterDeclaration(map.getValue(), reference)
            );
        }

        return false;
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        HashMap<AstNode, SymbolTable> symbolTables = SymbolTableBuilder.buildTables(node);

        for (Map.Entry<AstNode, SymbolTable> scopeToSymbolTable : symbolTables.entrySet()) {
            verifyUsagesAfterDeclarations(scopeToSymbolTable.getKey(), scopeToSymbolTable.getValue());
        }

        return issues;
    }

    void verifyUsagesAfterDeclarations(AstNode scopeNode, SymbolTable table) {
        List<AstNode> references = AstUtils.getAllVariableReferencesInCurrentScope(scopeNode);

        references.forEach(reference ->
                this.verifyUsageAfterDeclarationAndRaiseIssues(table, reference)
        );

    }

    boolean verifyUsageAfterDeclaration(SymbolTable table, AstNode reference) {

        if (table.containsDeclaration(reference.getTokenValue())) {

            Declaration declaration = table.getDeclaration(reference.getTokenValue());
            if (declaration.getLine() < reference.getTokenLine())
                return true;

            SymbolTable ancestorTable = table.getPreceedingTable();

            while (ancestorTable != null) {
                if (ancestorTable.containsDeclaration(reference.getTokenValue())) {
                    declaration = ancestorTable.getDeclaration(reference.getTokenValue());
                    return declaration.getLine() < reference.getTokenLine();
                } else {
                    ancestorTable = ancestorTable.getPreceedingTable();
                }
            }
        }
        return false;
    }


    void verifyUsageAfterDeclarationAndRaiseIssues(SymbolTable table, AstNode reference) {
        Declaration declaration;

        if (table.containsDeclaration(reference.getTokenValue())) {
            declaration = table.getDeclaration(reference.getTokenValue());
            if (declaration.getLine() < reference.getTokenLine())
                return;
        }
        SymbolTable ancestorTable = table.getPreceedingTable();

        while (ancestorTable != null) {
            if (ancestorTable.containsDeclaration(reference.getTokenValue())) {
                declaration = ancestorTable.getDeclaration(reference.getTokenValue());
                if (declaration.getLine() < reference.getTokenLine())
                    return;
            }
            ancestorTable = ancestorTable.getPreceedingTable();
        }

        issues.add(new PreciseIssue(IssueLocation.preciseLocation(reference, message)));
    }

    @DisplayName("Reference before declaration in different scopes")
    @Test
    public void t1() {
        String s = "foo(){\n" + "echo ${xo}\n" + "}\n"
                + "xo=1;";
        assertThatThisManyIssuesAreGenerated(1, s);
    }


    @DisplayName("Reference before declaration in different scopes")
    @Test
    public void t7() {
        String s =
                "if [ $# -eq 0 ]; then\n" +
                        "\n" +
                        "  echo \"Usage: $0 title\" >&2\n" +
                        "\n" +
                        "  exit 1\n" +
                        "\n" +
                        "else \n" +
                        "\n" +
                        "  echo -ne \"\\033]0;$1\\007\"\n" +
                        "\n" +
                        "fi\n" +
                        "\n" +
                        "echo ${xoxxx}\n" +
                        "\n" +
                        "xoxxx=1";
        printTree(s);
        assertThatThisManyIssuesAreGenerated(1, s);
    }


    @DisplayName("Reference after declaration in different scopes")
    @Test
    public void t2() {
        String s = "xo=1 \n foo(){\n" + "echo ${xo}\n";
        assertThatCheckSucceeds(s);
        assertThatNoIssuesAreGenerated(s);
    }

    @DisplayName("Reference before declaration in same scope")
    @Test
    public void t3() {
        String s = "echo ${xo}\n"
                + "xo=1";
        assertThatCheckFails(s);
        assertThatThisManyIssuesAreGenerated(1, s);
    }

    @DisplayName("arithmetic expansion with messed up sequence")
    @Test
    public void t4() {
        String code = "a=1; my_function(){\n echo $((a+b));\n}\n b=2;";
        assertThatThisManyIssuesAreGenerated(1, code);

    }

    @DisplayName("arithmetic expansion employing proper sequence")
    @Test
    public void t5() {
        String code = "a=1; b=3; my_function(){\n echo $((a+b))\n}\n c=2\n";
        assertThatNoIssuesAreGenerated(code);
    }


}
