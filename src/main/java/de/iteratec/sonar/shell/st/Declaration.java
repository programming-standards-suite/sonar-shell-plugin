package de.iteratec.sonar.shell.st;

import com.sonar.sslr.api.AstNode;

import java.util.HashSet;

public class Declaration {

    AstNode declaration;

    HashSet<AstNode> references = new HashSet();

    DeclarationType type;

    public Declaration(AstNode declaration) {
        this.declaration = declaration;
    }

    public Declaration(AstNode declaration, DeclarationType type) {
        this.declaration = declaration;
        this.type = type;
    }


    public void addReference(AstNode reference) {
        references.add(reference);
    }

    public String getName() {
        return declaration.getTokenValue();
    }

    public int getLine() {
        return declaration.getTokenLine();
    }

    public boolean isReferenced() {
        return references.size() > 0;
    }

    public AstNode getNode() {
        return this.declaration;
    }

}
