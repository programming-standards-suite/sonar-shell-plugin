/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package de.iteratec.sonar.shell.util;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.GenericTokenType;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.ast.ShellParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import static de.iteratec.sonar.shell.ast.ShellGrammar.*;

/**
 * Utilities for inspecting AST nodes.
 *
 * @author <a href="mailto:Gerd.Neugebauer@iteratec.de">Gerd Neugebauer</a>
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public final class AstUtils {

    /**
     * Number of spaces that each indentation level expands to when pretty-printing.
     */
    public static final int INDENT_WIDTH = 2;

    static final String IDENTIFIER = "IDENTIFIER";

    static final String LINEBREAK = "LINEBREAK";

    /**
     * for a given node, finds respective scope. a scope is either a compound list or
     * the node complete commands. The latter denotes the root scope.
     *
     * @param variable
     * @return
     */
    public static AstNode findParentScope(AstNode variable) {
        AstNode parent = variable.getParent();

        while (!parent.getType()
                .equals(ShellGrammar.COMPOUND_LIST)
                && !parent.getType().equals(
                ShellGrammar.COMPLETE_COMMANDS)) {
            parent = parent.getParent();
        }
        return parent;
    }

    /**
     * return all variables that can be
     * found from the given scope in the current or all subsequent scopes
     * <p>
     * an identifier is a variable if it is
     * found in a dollar construct
     *
     * @param node node of syntax tree
     * @return the names of all variable
     * references
     */
    public static List<String> returnAllSubsequentVariablesAsString(
            AstNode node) {
        List<AstNode> referenceNodes = node
                .getDescendants(DOLLAR_CONSTRUCT);

        return referenceNodes.stream().map(reference -> node.getDescendants(GenericTokenType.IDENTIFIER))
                .flatMap(List::stream).map(identifierNode -> identifierNode.getTokenValue())
                .collect(Collectors.toList());
    }

    public static List<AstNode> getAllVariableReferencesInScope(AstNode node) {

        return node.getDescendants(DOLLAR_CONSTRUCT).stream()
                .flatMap(variable -> variable.getDescendants(GenericTokenType.IDENTIFIER).stream())
                .collect(Collectors.toList());

    }

    public static List<AstNode> getAllVariableReferencesInCurrentScope(AstNode node) {

        List<AstNode> referenceNodes = node.getDescendants(DOLLAR_CONSTRUCT).stream()
                .flatMap(variable -> variable.getDescendants(GenericTokenType.IDENTIFIER).stream())
                .collect(Collectors.toList());

        return referenceNodes.stream().filter(referenceNode ->
                findParentScope(referenceNode).equals(node)
        ).collect(Collectors.toList());

    }


    public static List<AstNode> getAllFunctionReferencesInScope(AstNode node) {
        return node.getDescendants(SIMPLE_COMMAND).stream().filter(statement ->
                !statement.hasDescendant(ASSIGNMENT_WORD))
                .map(function -> function.getFirstChild())
                .collect(Collectors.toList());
    }

    /**
     * Writes out the indentation prefix for a line.
     */
    protected void printIndent(Appendable buffer, int indentLevel) throws IOException {
        for (int i = 0; i < indentLevel * INDENT_WIDTH; i++) {
            buffer.append(' ');
        }
    }

    /**
     * prints ast in console, used to validate structure
     *
     * @param root the root of the ast
     */
    public static void printTree(AstNode root) {
        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (current.hasChildren()
                    && !current.getName().equals(LINEBREAK)) { // Remove
                // annoying
                // linebreaks
                List<AstNode> children = current.getChildren();
                for (AstNode child : children) {
                    StringBuilder string = new StringBuilder();
                    if (child.getName().equals(IDENTIFIER)) { // child.getChildren().isEmpty()
                        string.append(current.getName()).append(" -> ")
                                .append(child.getTokenValue());
                    } else {
                        string.append(current.getName()).append(" -> ")
                                .append(child.getName());
                    }
                    queue.add(child);
                }
            }

        }
    }

    /**
     * prints ast in console Return an adjacency-list-conforming structure.
     *
     * @param root the root of the ast
     */
    public static String stringifyTree(AstNode root) {
        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        StringBuilder motherString = new StringBuilder();

        buildTreeToString(queue, motherString);
        return motherString.toString();
    }

    public static String stringifyTree(String root) {

        AstNode node = ShellParser.create().parse(root);

        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(node);

        StringBuilder motherString = new StringBuilder();

        buildTreeToString(queue, motherString);
        return motherString.toString();
    }

    private static void buildTreeToString(Queue<AstNode> queue, StringBuilder motherString) {
        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (current.hasChildren()
                    && !current.getName().equals(LINEBREAK)) { // Remove
                // annoying
                // linebreaks
                List<AstNode> children = current.getChildren();
                StringBuilder fatherAndChildrenString = new StringBuilder();


                appendTokenValueOrTokenName(current, "->", fatherAndChildrenString);

                StringBuilder childrenString = new StringBuilder();

                childrenString.append("(");

                for (AstNode child : children) {
                    appendTokenValueOrTokenName(child, "", childrenString);

                    if (children.lastIndexOf(child) + 1 != children.size()) {
                        childrenString.append(", ");
                    }
                    queue.add(child);
                }
                childrenString.append(")\n");
                fatherAndChildrenString.append(childrenString.toString());
                motherString.append(fatherAndChildrenString.toString());
            }

        }
    }


    private static void appendTokenValueOrTokenName(AstNode node, String optionalString, StringBuilder builder) {
        if (node.getName().equals(IDENTIFIER)
                || node.getName().equals("INTEGER")
                || node.getName().equals("DIGIT")) {
            builder.append(node.getTokenValue() + optionalString);
        } else {
            builder.append(node.getName() + optionalString);
        }
    }

    /**
     * checks if ast contains node with the specified value
     *
     * @param name value that is to be searched for
     * @param root to-be-parsed tree
     * @return true if value specified under name has been found, else false
     */
    public static boolean containsValue(String name, AstNode root) {
        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (!current.getTokenValue().equals(name)) { // Remove
                if (current.hasChildren()) {
                    List<AstNode> children = current.getChildren();
                    for (AstNode child : children) {
                        queue.add(child);
                    }
                }
            } else {
                return true;
            }
        }

        return false;

    }

    /**
     * checks if ast contains node with the specified value
     *
     * @param token to-be-detected token
     * @param root  to-be-parsed tree
     * @return true if value specified under name has been found, else false
     */
    public static boolean containsToken(String token, AstNode root) {

        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (!current.getName().equals(token)) { // Remove
                if (current.hasChildren()) {
                    List<AstNode> children = current.getChildren();
                    for (AstNode child : children) {
                        queue.add(child);
                    }
                }
            } else {
                return true;
            }
        }

        return false;

    }

    /**
     * retrieves all occurenecs of a token from an AST
     *
     * @param token token that is to be detected
     * @param root  root of ast
     * @return a list containing the occurences of the token
     * in the tree with all their respective children
     */
    public static List<AstNode> getAllOccurencesOf(String token, AstNode root) {
        List<AstNode> nodes = new ArrayList();

        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (!current.getName().equals(token)) { // Remove

            } else {
                nodes.add(current);
            }
            if (current.hasChildren()) {
                List<AstNode> children = current.getChildren();
                for (AstNode child : children) {
                    queue.add(child);
                }
            }

        }

        return nodes;
    }

    /**
     * counts occurences of a given token
     *
     * @param token to be counted token
     * @param root  to be searched tree
     * @return number of occurences of the token in ast
     */
    public static int countOccurencesOfToken(String token, AstNode root) {

        Queue<AstNode> queue = new LinkedBlockingQueue();
        queue.add(root);

        int count = 0;

        while (!queue.isEmpty()) {
            AstNode current = queue.poll();
            if (current.getName().equals(token)) { // Remove
                count++;
            }
            if (current.hasChildren()) {
                List<AstNode> children = current.getChildren();
                for (AstNode child : children) {
                    queue.add(child);
                }
            }
        }

        return count;

    }

    /**
     * @param tree
     * @return
     */
    public static AstNode getCompleteCommands(AstNode tree) {
        return AstUtils.getAllOccurencesOf("COMPLETE_COMMANDS", tree).get(0);
    }

}
