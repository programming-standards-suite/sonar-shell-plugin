/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.GenericTokenType;
import com.sonar.sslr.api.Token;
import com.sonar.sslr.api.TokenType;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.LexerState;
import de.iteratec.sonar.shell.ast.ShellLexer.Literals;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * deals with shell comments by removing them. regards a comment as the construct indicated by
 * a white space followed by a hash-sign, e.g. #hi. Disregards this structure when in double-quote-
 * or single-quote-context.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class CommentRegexpRemoverChannel extends Channel<Lexer> {

	private Matcher matcher;
	LexerState ls;

	public CommentRegexpRemoverChannel(String regexp, LexerState ls) {
		matcher = Pattern.compile(regexp).matcher("");
		this.ls = ls;
	}

	/* (non-Javadoc)
	 * @see org.sonar.sslr.channel.Channel#consume(org.sonar.sslr.channel.CodeReader, java.lang.Object)
	 */
	@Override
	public boolean consume(CodeReader code, Lexer output) {
		StringBuilder temp = new StringBuilder();
		List<Token> tokens = output.getTokens();
		if (!tokens.isEmpty()) {

			TokenType type = tokens.get(tokens.size() - 1).getType();
			if ((type.equals(Literals.DELIMITER)
					|| type.equals(GenericTokenType.EOL))
					&& (!ls.isInDoubleQuotes()) && code.popTo(matcher, temp) > 0) {
				// Previous token is either delimiter or eol,
				// therefore we have a valid comment and the subsequent
				// characters can be removed
				return true;

			}
		} else if ((char) code.charAt(0) == '#' && code.popTo(matcher, temp) > 0) {
			// No tokens detected yet, but first character indicates a
			// comment

			return true;

		}
		return false;
	}

}
