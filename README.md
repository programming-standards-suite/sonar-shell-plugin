# SonarQube ShellScript Plug-In

The Shell Script Plug-In analyzes shell scripts of various dialects (BaSh, ASh, CSh, 
TCSh, ZSh and anything strictly posix) and produces findings if the respective rules fire.

As of now, only the step before the actual analyzing is supported:
The parsing of scripts (input as a string or as a file) into abstract syntax trees. 
This takes place after lexing the code. Both of these components have been realized 
with the _SonarSource Language Recognizer_.

## Lexer 
The lexer takes shell-scripts as input and turns them into a series of token. This process is called lexing.
Lexing is realized mainly through channels. The lexer is state based.

### The lexing process

The lexer takes shell-script-code and turns it into a list of tokens.
The input can be fed through a string or by using an actual file. The class FileImporter.java has an auxiliary static function, 
which simply needs a path to the to-be-lexed file. 

### State-based Lexer

It is important to note that this lexer here is state-based. This state is subject to change depending on the tokens it detects while lexing. As of now, the lexer maintains the following things as state.
The state is retained in LexerState.java, while it is being changed through the channels.


####  Double-Quote-Context: 

The lexer sets itself into double-quote-context as soon as it detects an unescaped double quote has not set itself into 
double-quote-context before. Subsequently, it unsets the double-quote-context as soon as it detects an unescaped double 
quote (the _terminating double-quote) while in double-quote-context. 
It is to note that while the double-quote-context is set, the single-quote context cannot be set. If in 
double-quote-context, the lexer disables specific constructs (like single-quoted word generation or tilde-expansion) 
and turns keywords into identifiers. Furthermore, white spaces are being retained as tokens, which are 
named _WHITE_SPACE_. In regular context (neither double quote nor single quote), white spaces simply get discarded.
As of now, due to parameter substitution done through the dollar-operator and the likes, white spaces in the double 
quote context need to be distinctly retained as tokens inside of abstract syntax tree.

#### Single-Quote-Context: 

Initiation and revoking of the single quote-context is analogous to the double-quote-context. 
Its effect is much stricter than the double-quote-context. 
Nearly all constructs and special chars, like parameter substitutions, get disabled here. 
If the single-quote-context is set, a specific channel turns all subsequent token into a single IDENTIFIER token, 
until it detects the terminating single quote. An exception is found in constructs like "$(echo 'he%llo')", 
where we are technically in a double-quote-context, 
yet need to parse this as single-quoted word due to new context implied in the dollar sign.

#### HereDocumentToken required: 

Turns the next IDENTIFIER-Token into a Here-Document-Token. This is set when either << or <<- in a non-quoted-context is detected.
If a specific Here-Document-Token is occuring for the first time, it gets stored in the HereDocumentTokenValue-List. 

#### HereDocumentTokenValue-List:

Contains IDENTIFIERS that have been turned into a Here-Document-Tokens. A element from this list gets removed as soon as it gets detected
as the first token in a new line, ending with a newline.


### Parser

The parser comes into action after the tokens have been generated. The stream of tokens gets matched to a
previously defined grammar. The outcome of said process is, if the token stream adheres 
to the grammar, an abstract syntax tree. After creation, the tree gets evaluated on having used all tokens and the
different shell dialects dialects it could conform to.

#### The Grammar
The grammar is defined in ShellGrammar.java. For this, the grammar-API of sslr is employed. The Grammar API
requires the definition of the components with rules. A Grammar rule key is something that becomes part of an abstract
syntax tree, if a shell-script contains it. Each Grammar ruley key, when declared, needs to be defined through a rule.
Each rule gets built into the grammar and is then used to match against the string. 

For instance, a simple command like 'echo hello world' contains the 
Grammar rule key SIMPLE_COMMAND. But at the same time, 'echo' is a Grammar rule key called CMD_NAME, while 'hello' and 'world' both 
become Grammar rule key called CMD_SUFFIX. 



It tries to unify the grammar of multiple dialects, which is why features
from different languages 



## Other

