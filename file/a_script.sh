#!/usr/bin/env bash

set -e

C_ERROR=1
C_SUCCESS=0
C_MISSING_PARAMETER=2


  ##    ####   ####  ###### #####  #####      #  ###### #####  #####   ####  #####   ####      ####   ####  #    # #    #   ##   #    # #####   ####
 #  #  #      #      #      #    #   #       #   #      #    # #    # #    # #    # #         #    # #    # ##  ## ##  ##  #  #  ##   # #    # #
#    #  ####   ####  #####  #    #   #      #    #####  #    # #    # #    # #    #  ####     #      #    # # ## # # ## # #    # # #  # #    #  ####
######      #      # #      #####    #     #     #      #####  #####  #    # #####       #    #      #    # #    # #    # ###### #  # # #    #      #
#    # #    # #    # #      #   #    #    #      #      #   #  #   #  #    # #   #  #    #    #    # #    # #    # #    # #    # #   ## #    # #    #
#    #  ####   ####  ###### #    #   #   #       ###### #    # #    #  ####  #    #  ####      ####   ####  #    # #    # #    # #    # #####   ####
function error() {
    (>&2 echo -e "$@")
    exit $C_ERROR
}


help() {
	echo "This is the help message"
}

_show_help_if_set() {
    args=("$@")
    if is_string_in_array args[@] "--help"; then
       help
       exit ${C_SUCCESS}
    fi
    return ${C_SUCCESS}
}

is_string_in_array()
{
    declare -a argAry1=("${!1}")

    local _string=$2
    [[ -z ${_string} ]] && return $C_MISSING_PARAMETER

    for var in ${args[@]}; do
        if [[ "${var}" == "${_string}" ]];
        then
            return $C_SUCCESS
        fi
    done
    return $C_ERROR
}

#Ensures that a parameter is given and that it has a value
param_value_must_exist() {
	declare -a argAry1=("${!1}")
	local _paramName=$2
	[[ -z ${_paramName} ]] && error "ERROR: param_value_must_exist() key: ${_paramName} is not defined"
	_paramValue=$(_get_key_value $1 "${_paramName}")
	[[ -z ${_paramValue} ]] && error "Value of the parameter ${_paramName} is not defined"
	return 0
}

###############################################
#######Retrieves the value of a given key  ####
### Used to retrieve a given parameters value #
### Stops after finding the first match    ####
###############################################
function _get_key_value()
{
    IFS='' declare -a argAry1=("${!1}")
    local _string=$2
    [[ -z ${_string} ]] && echo "_get_key_value has been called with no key to search" >&2 && return $C_MISSING_PARAMETER

		for i in "${!argAry1[@]}"; do
			if [[ "${argAry1[$i]}" == "${_string}" ]] && [[ $i -lt $((${#argAry1[@]} -1)) ]];
			then
				##TODO must consume the string until "--" appears
				((i++))
				_value="${argAry1[$i]}"
				((i++))
					while  [[ $i -lt $((${#argAry1[@]} -1)) ]] && ( [[ ${argAry1[$i]} != --* ]] || [[ ${argAry1[$i]} == "-----BEGIN" ]] || [[ ${argAry1[$i]} == "-----END" ]] );
					do
						_value="$_value ${argAry1[$i]}"
						((i++))
					done
					echo "$_value"
					return $C_SUCCESS
			fi
		done

		error "Key $_string could not be found"
		exit $C_ERROR
}

function _get_key_value_optional()
{
     IFS='' declare -a argAry1=("${!1}")
    local _string=$2
    [[ -z ${_string} ]] && echo "_get_key_value has been called with no key to search" >&2 && return $C_MISSING_PARAMETER

		for i in "${!argAry1[@]}"; do
			if [[ "${argAry1[$i]}" == "${_string}" ]] && [[ $i -lt $((${#argAry1[@]} -1)) ]];
			then
				##TODO must consume the string until "--" appears
				((i++))
				_value="${argAry1[$i]}"
				((i++))
					while  [[ $i -lt $((${#argAry1[@]} -1)) ]] && ( [[ ${argAry1[$i]} != --* ]] || [[ ${argAry1[$i]} == "-----BEGIN" ]] || [[ ${argAry1[$i]} == "-----END" ]] );
					do
						_value="$_value ${argAry1[$i]}"
						((i++))
					done
					echo "$_value"
					return $C_SUCCESS
			fi
		done
		echo ""
		return $C_ERROR
}

waitForMergeRequest() {
	echo "###########################"
	echo "# Wait for Merge request  #"
	echo "###########################"
    # Extract the host where the server is running, and add the URL to the APIs
    [[ $HOST =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

    #MergeRequestID is created by createMergeRequest.sh
    #echo MR_ID: ${MR_ID}

    statusCall="curl --silent \"${HOST}${CI_PROJECT_ID}/merge_requests/${MR_ID}\" --header \"PRIVATE-TOKEN:${PRIVATE_TOKEN}\""
    # Require a list of all the merge request and take a look if there is already
    # one with the same source branch
    mr=`eval ${statusCall}`
    status="$(echo ${mr} | jq -r .state)"
    #echo ${mr} | jq .

    try=0
    #retry as long as it is not merged or closed
    while [[ "${status}" = "opened" ]] && [[ ${try} -lt ${maxRetries} ]]; do
        set +e
        ((try++))
        echo "Deployment not yet ready due to merge status '${status}', waiting ${sleepTime} seconds for ${try} of ${maxRetries} retries"
        sleep ${sleepTime}
        mr=`eval ${statusCall}`
        status="$(echo ${mr} | jq -r .state)"
        #echo ${mr} | jq -r .
    done

    #for debugging
    #echo "Merge Request:  ${mr}"
    #check state
    echo "Merge Request ID=${MR_ID}"
	status="$(echo ${mr} | jq -r .state)"
    echo "Merge status=${status}"
	description="$(echo ${mr} | jq -r .description)"
    echo "description = $description"
    #echo "${mr}" | jq . 
    [[ "${status}" = "closed" ]] \
        && echo "Deployment failed due to GitOps merge status '${status}'" && exit 1;
    [[ "${status}" = "merged" ]] \
        && echo "Merge/Deployment succesful due to GitOps merge status '${status}'"
}

createMergeRequest() {
	echo "###########################"
	echo "# create Merge Request    #"
	echo "###########################"
    # Extract the host where the server is running, and add the URL to the APIs
    [[ $HOST =~ ^https?://[^/]+ ]] && HOST_API="${BASH_REMATCH[0]}/api/v4/projects/" || { echo "\$HOST beginnt nicht mit http(s)://"; false; }

	echo "HOST_API $HOST_API"
	echo "CI_PROJECT_ID $CI_PROJECT_ID"

    # Look which is the default branch
    #TARGET_BRANCH=`curl --silent "${HOST}${CI_PROJECT_ID}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)['default_branch'])"`;

    # The description of our new MR, we want to remove the branch after the MR has
    # been closed
    BODY="{
        \"id\": ${CI_PROJECT_ID},
        \"source_branch\": \"${FeatureBranchName}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"${FeatureBranchName}\",
        \"assignee_id\":\"${GIT_USER_ID}\"
    }";
	echo "Prepared Request for MergeRequest: $BODY"

    # Require a list of all the merge request and take a look if there is already
    # one with the same source branch
	
    LISTMR=$(curl --fail --show-error --silent "${HOST_API}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}");
    if [[ ${#LISTMR} -lt 5 ]]; then
    	COUNTBRANCHES=0
		echo "COUNTBRANCHES $COUNTBRANCHES"
    else
    	COUNTBRANCHES="$(echo ${LISTMR} | grep -o "\"source_branch\":\"${FeatureBranchName}\"" | wc -l)";
		echo "COUNTBRANCHES $COUNTBRANCHES"
    fi


    # No MR found, let's create a new one
    if [[ "${COUNTBRANCHES}" -eq "0" ]];
    then
		echo "No open merge request found for source_branch=${FeatureBranchName}, creating Merge Request"
        result=$(curl -X POST "${HOST_API}${CI_PROJECT_ID}/merge_requests" \
            --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}");
        mrId="$(echo ${result} | jq -r .iid)"
        #echo ${result} | jq .
        if [[ -z "${mrId}" ]];
        then
            echo  "Could not start MR:\n${result}" >&2
            exit 1
        fi
        echo "Merge request ID: ${mrId}"
        echo ${mrId} > MergeRequestID
        echo "Opened a new merge request: WIP: ${FeatureBranchName} and assigned to you";
    fi

    echo "No new merge request opened";
}

createAndWaitMergeRequest() {
	echo "###########################"
	echo "# createAndWaitMergeRequest    #"
	echo "###########################"
	args=( "$@" )
	TARGET_BRANCH=$(_get_key_value args[@] "--config-env-branch")
	HOST=$(_get_key_value args[@] "--configrepo-url-https")
	#CI_PROJECT_ID=$(_get_key_value args[@] "--configrepo-project-id")
	CONFIGREPO_PROJECT_PATH=$(_get_key_value args[@] "--configrepo-project-path")
	PROJECT_PATH_URLENCODED=$(echo $CONFIGREPO_PROJECT_PATH | jq -Rr @uri)
	CI_COMMIT_REF_NAME=$FeatureBranchNameq
	GIT_USER_ID=$(_get_key_value args[@] "--git-user-id")
	PRIVATE_TOKEN=$(_get_key_value args[@] "--git-private-token")
	CI_PROJECT_ID=$(curl --fail --show-error --silent --header "PRIVATE-TOKEN:$PRIVATE_TOKEN" https://git.tech.rz.db.de/api/v4/projects/${PROJECT_PATH_URLENCODED} | jq .id )

 maxRetries=$(_get_key_value_optional args[@] "--max-retries") || 15
 sleepTime=$(_get_key_value_optional args[@] "--sleep-time") || 60
	#create Merge Request
	#TARGET_BRANCH=${CONFIG_ENVIRONMENT_BRANCH}  HOST=${CONFIG_REPO_URL} CI_PROJECT_ID=${CONFIG_REPO_PROJECT_ID} CI_COMMIT_REF_NAME=${FeatureBranchName} GIT_USER_ID=${GIT_USER_ID} PRIVATE_TOKEN=${git_personal_access_token} createMergeRequest
	createMergeRequest
	#read Merge request id created by createMergeRequest.sh
	MR_ID=$(cat ./MergeRequestID);echo MR_ID=$MR_ID
	#Wait for Merge Request
	#HOST=${CONFIG_REPO_URL} MR_ID=${MR_ID} CI_PROJECT_ID=${CONFIG_REPO_PROJECT_ID} PRIVATE_TOKEN=${git_personal_access_token} waitForMergeRequest
	if ! is_string_in_array args[@] "--no-wait"; then waitForMergeRequest; fi
}

testScript() {
	args=( "$@" )
	GIT_USERNAME=$(_get_key_value args[@] "--git-username")
	echo "$GIT_USERNAME"
	echo "------------YAYYY-----------------"
	param_value_must_exist args[@] "--git-username"
  echo "Success"
	exit 0
}

configureGit() {
		echo "#####################################
########  INSIDE configureGit ##############
###########################################"
		args=( "$@" )
		#IFS='' _get_key_value args[@] "--git-deploy-key"
		GIT_USERNAME=$(_get_key_value args[@] "--git-username")
		GIT_USER_EMAIL=$(_get_key_value args[@] "--git-user-email")
		POC_DEPLOY_KEY=$(_get_key_value args[@] "--git-deploy-key")
    #init git
    git config --global user.email "${GIT_USER_EMAIL}"
    git config --global user.name "${GIT_USERNAME}"
	git config --global color.ui always
    git config --list
    eval $(ssh-agent -s)
	#bash on commandline converts linebreaks into spaces without quotation marks
	ssh-add <(echo "$POC_DEPLOY_KEY" | base64 -d )
    ssh-add -L
    ssh -o "StrictHostKeyChecking=no" git@ssh.git.tech.rz.db.de
}

createFeatureBranch() {
	echo "#########################"
	echo "# create Feature branch #"
	echo "#########################"
	args=("$@")
	

	#ToDo create Branch with name
	#clone master
	cd $PROJECT_DIR
	git clone ${CONFIGREPO_URL} --branch ${CONFIG_ENVIRONMENT_BRANCH} ${PROJECT_DIR}/$HELMVALUES_CHECKOUT_DIR && cd ${PROJECT_DIR}/$HELMVALUES_CHECKOUT_DIR
	#create Branch
	#In case of gitlab retry: decide if the DR-branch already exists, then only git checkout existing Branch; else git checkout -b (create new branch)
	git checkout $FeatureBranchName && git pull origin $FeatureBranchName || git checkout -b $FeatureBranchName
	cd ${PROJECT_DIR}
}

adaptHelmValues() {
	echo "###########################"
	echo "# Replace Service version #"
	echo "###########################"
	args=("$@")
	PROJECT_DIR=$(_get_key_value args[@] "--project-dir")
	HELM_VALUE_SERVICE=$(_get_key_value args[@] "--helm-value-service")

	#modify values
	cd ${PROJECT_DIR}/$HELMVALUES_CHECKOUT_DIR
	sed -i "s/^\(\s*serviceVersion\s*:\s*\).*/\1${SW_VERSION}/" ${HELM_VALUE_SERVICE}
	echo "set serviceVersion=${SW_VERSION} in $FeatureBranchName in file ${HELM_VALUE_SERVICE}"
	COPY_DEPLOYMENT_HELM=$(_get_key_value args[@] '--daas-copy-deployment-helm')
	if [[ "${COPY_DEPLOYMENT_HELM}" == "true" ]];
		then
			#promote used chart and chart version
			cp -v  ${PROJECT_DIR}/deployment/helm/* ${PROJECT_DIR}/$HELMVALUES_CHECKOUT_DIR
			echo "copied from Service:/deployment/helm/*  to ${FeatureBranchName}"
        fi
	cd ${PROJECT_DIR}
}

commitChanges() {
	echo "###########################"
	echo "# commit changes #"
	echo "###########################"
	args=("$@")
	PROJECT_DIR=$(_get_key_value args[@] "--project-dir")
	CI_JOB_URL=$(_get_key_value args[@] "--ci-job-url")

	#commit Branch
	cd ${PROJECT_DIR}/$HELMVALUES_CHECKOUT_DIR
	#git diff 
	#in case of retry an empty commit must be possible => --allow-empty
	git commit -am "created $FeatureBranchName from $CI_JOB_URL" --allow-empty
	git diff $FeatureBranchName..${CONFIG_ENVIRONMENT_BRANCH} --relative
	git push origin $FeatureBranchName
	cd ${PROJECT_DIR}
}

main() {
	echo "usage"
	echo "############"
	echo "configure-git"
	echo "create-featurebranch"
	echo "adapt-helm-values"
	echo "commit-changes"
	echo "create-merge-request"
	echo "all"
    #SW_VERSION=$(cat ./sw_version);
    #FeatureBranchName=DR-$SW_VERSION
    #echo "SW_VERSION=$SW_VERSION"
    #configureGit
    #createFeatureBranch
    #adaptHelmValues
#		commitChanges
    #create Merge Request
 #   TARGET_BRANCH=${CONFIG_ENVIRONMENT_BRANCH}  HOST=${CONFIG_REPO_URL} CI_PROJECT_ID=${CONFIG_REPO_PROJECT_ID} CI_COMMIT_REF_NAME=${FeatureBranchName} GITLAB_USER_ID=${GITLAB_USER_ID} PRIVATE_TOKEN=${git_personal_access_token} createMergeRequest
    #read Merge request id created by createMergeRequest.sh
 #   MR_ID=$(cat ./MergeRequestID);echo MR_ID=$MR_ID
    #Wait for Merge Request
  #  HOST=${CONFIG_REPO_URL} MR_ID=${MR_ID} CI_PROJECT_ID=${CONFIG_REPO_PROJECT_ID} PRIVATE_TOKEN=${git_personal_access_token} waitForMergeRequest
}


############################################################################################
###############   MAIN      ################################################################
############################################################################################
set -eo pipefail; [[ "$TRACE" ]] && set -x

#_show_help_if_set "$@"


args=( "$@" )
declare cmd="$1"
#[[ ! -f ./sw_version ]] && error "the file ./sw_version must exist"

while true; do
	case "$cmd" in
		configure-git)
			param_value_must_exist args[@] "--git-username"
			param_value_must_exist args[@] "--git-user-email"
			param_value_must_exist args[@] "--git-deploy-key"
			configureGit $@
			break;;
		create-featurebranch)
			param_value_must_exist args[@] "--project-dir"
			param_value_must_exist args[@] "--configrepo-url"
			param_value_must_exist args[@] "--config-env-branch"
			createFeatureBranch $@
			break;;
		adapt-helm-values)
			param_value_must_exist args[@] "--project-dir"
			param_value_must_exist args[@] "--helm-value-service"
			adaptHelmValues $@
			break;;
		commit-changes)
			param_value_must_exist args[@] "--ci-job-url"
			param_value_must_exist args[@] "--project-dir"
			commitChanges $@
			break;;
		create-merge-request)
			param_value_must_exist args[@] "--config-env-branch"
			param_value_must_exist args[@] "--configrepo-url-https"
			#param_value_must_exist args[@] "--configrepo-project-id"
			param_value_must_exist args[@] "--configrepo-project-path"
			#param_value_must_exist args[@] "--commit-ref-name"
			param_value_must_exist args[@] "--git-user-id"
			param_value_must_exist args[@] "--git-private-token"
			createMergeRequest $@
			break;;
		all)
			param_value_must_exist args[@] "--git-username"
			echo " git-username  $(_get_key_value args[@] '--git-username')"
			param_value_must_exist args[@] "--git-user-email"
			echo " git-user-email  $(_get_key_value args[@] '--git-user-email')"
			param_value_must_exist args[@] "--git-deploy-key"
			echo " git-deploy-key  ****"
			param_value_must_exist args[@] "--project-dir"
			echo " project-dir  $(_get_key_value args[@] '--project-dir')"
			param_value_must_exist args[@] "--configrepo-url"
			echo " configrepo-url  $(_get_key_value args[@] '--configrepo-url')"
			param_value_must_exist args[@] "--config-env-branch"
			echo " config-env-branch  $(_get_key_value args[@] '--config-env-branch')"
			param_value_must_exist args[@] "--helm-value-service"
			echo " helm-value-service  $(_get_key_value args[@] '--helm-value-service')"
			param_value_must_exist args[@] "--ci-job-url"
			echo " ci-job-url  $(_get_key_value args[@] '--ci-job-url')"
			#param_value_must_exist args[@] "--target-branch"
			#ToDo remove is duplicate to --config-env-branch
			#echo " target-branch  $(_get_key_value args[@] '--target-branch')"
			param_value_must_exist args[@] "--configrepo-url-https"
			echo " configrepo-url-https  $(_get_key_value args[@] '--configrepo-url-https')"
			#param_value_must_exist args[@] "--configrepo-project-id"
			param_value_must_exist args[@] "--configrepo-project-path"
			#echo " configrepo-project-id  $(_get_key_value args[@] '--configrepo-project-id')"
			echo " configrepo-project-path  $(_get_key_value args[@] '--configrepo-project-path')"
			#ToDo remove this param, because it is the featurebranchname
			#param_value_must_exist args[@] "--commit-ref-name"
			#echo " commit-ref-name  $(_get_key_value args[@] '--commit-ref-name')"
			param_value_must_exist args[@] "--git-user-id"
			echo " git-user-id  $(_get_key_value args[@] '--git-user-id')"
			param_value_must_exist args[@] "--git-private-token"
			echo " git-private-token  ***"
			param_value_must_exist args[@] "--daas-copy-deployment-helm"
			echo " daas-copy-deployment-helm  $(_get_key_value args[@] '--daas-copy-deployment-helm')"
			param_value_must_exist args[@] "--daas-software-version"
			echo " daas-software-version  $(_get_key_value args[@] '--daas-software-version')"
			SW_VERSION=$(_get_key_value args[@] '--daas-software-version')
			FeatureBranchName=DR-$SW_VERSION
			echo "SW_VERSION=$SW_VERSION"
			HELMVALUES_CHECKOUT_DIR=helmvalues
			PROJECT_DIR=$(_get_key_value args[@] "--project-dir")
			CONFIGREPO_URL=$(_get_key_value args[@] "--configrepo-url")
			CONFIG_ENVIRONMENT_BRANCH=$(_get_key_value args[@] "--config-env-branch")
			configureGit $@
			createFeatureBranch $@
			adaptHelmValues $@
			commitChanges $@
			createAndWaitMergeRequest $@
			break;;
		test-script)
			param_value_must_exist args[@] "--git-username"
			testScript $@
			break;;
			*)
				main "$@";;
	esac
done
echo "Done with the case"

exit 0
