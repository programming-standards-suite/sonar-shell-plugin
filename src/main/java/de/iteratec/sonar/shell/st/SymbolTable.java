package de.iteratec.sonar.shell.st;

import com.sonar.sslr.api.AstNode;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SymbolTable {

    SymbolTable parentTable;

    private List<AstNode> childrenScopes;

    private int complexity;

    /**
     * maps variable name to a declaration, which details
     * usage of said reference
     */
    HashMap<String, Declaration> declarationTable = new HashMap<>();

    SymbolTable(SymbolTable parentTable, List<AstNode> childNodes) {
        this.parentTable = parentTable;
        this.childrenScopes = childNodes;
        complexity = 0;
    }

    SymbolTable(SymbolTable parentTable, List<AstNode> childNodes, int complexity) {
        this.parentTable = parentTable;
        this.childrenScopes = childNodes;
        this.complexity = complexity;
    }

    public boolean containsDeclaration(String variable) {
        return declarationTable.containsKey(variable);
    }

    void addDeclaration(AstNode declaringNode) {
        Declaration declaration = new Declaration(declaringNode);
        declarationTable.put(declaringNode.getTokenValue(), declaration);
    }

    void addVariableDeclaration(AstNode declaringNode) {
        Declaration declaration = new Declaration(declaringNode, DeclarationType.VARIABLE);
        declarationTable.put(declaringNode.getTokenValue(), declaration);
    }

    void addFunctionDeclaration(AstNode declaringNode) {
        Declaration declaration = new Declaration(declaringNode, DeclarationType.FUNCTION);
        declarationTable.put(declaringNode.getTokenValue(), declaration);
    }


    public Declaration getDeclaration(String variableName) {
        return declarationTable.get(variableName);
    }

    /**
     * for the passed variable reference, traverses
     * from current symbol table to root
     * symbol table to confirm its declaration.
     *
     * @param reference variable that is supposed
     *                  to be in a symbol table
     */
    void checkDeclarationOfReference(AstNode reference) {

        String variableReference = reference.getTokenValue();

        if (this.containsDeclaration(variableReference)) {

            declarationTable.get(variableReference).addReference(reference);
        } else {
            SymbolTable ancestorTable = this.getPreceedingTable();
            while (ancestorTable != null) {
                if (ancestorTable.containsDeclaration(variableReference)) {
                    ancestorTable.declarationTable.get(variableReference).addReference(reference);
                    return;
                } else {
                    ancestorTable = ancestorTable.getPreceedingTable();
                }
            }
        }
    }

    public List<Declaration> getUnreferencedDeclarations() {
        return declarationTable.values().stream()
                .filter(declaration -> !declaration.isReferenced())
                .collect(Collectors.toList());
    }

    public SymbolTable getPreceedingTable() {
        return this.parentTable;
    }

    public boolean allVariablesReferenced() {
        return declarationTable.values().stream().allMatch(
                //either declarations that is referenced or no declarations at all in scope
                declaration -> (declaration.isReferenced() || declarationTable.isEmpty())
        );
    }

    public int getComplexity() {
        return complexity;
    }

    public List<AstNode> getChildrenScopes() {
        return childrenScopes;
    }
}
