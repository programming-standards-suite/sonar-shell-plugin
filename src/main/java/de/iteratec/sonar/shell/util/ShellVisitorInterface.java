package de.iteratec.sonar.shell.util;

import com.sonar.sslr.api.AstNode;

import java.util.List;

/**
 * The interface for a
 * shell-ast-visitor. what other methods
 * would make sense? Do I need the ones
 * that are unused atm?
 */
public interface ShellVisitorInterface {

    /**
     * entry point for check that merely returns a boolean value. can be used for simple testing purposes
     *
     * @param node the to be analyzed tree
     * @return a list of issues
     */
    boolean visitNode(AstNode node);


    /**
     * entry point for check that returns the actual issues in the AST that are related to the check.
     *
     * @param node the to be analyzed tree
     * @return a list of issues
     */
    List<PreciseIssue> visitNodeAndCollectIssues(AstNode node);

}
