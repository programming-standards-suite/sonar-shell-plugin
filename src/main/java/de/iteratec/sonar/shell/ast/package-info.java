/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */
/**
 *
 * @author <a href="mailto:Gerd.Neugebauer@iteratec.de">Gerd Neugebauer</a>
 */
package de.iteratec.sonar.shell.ast;