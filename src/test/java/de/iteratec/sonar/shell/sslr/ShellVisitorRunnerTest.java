package de.iteratec.sonar.shell.sslr;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.check.AllDeclaredVariablesAreReferencedCheck;
import de.iteratec.sonar.shell.check.FunctionDeclarationCheck;
import de.iteratec.sonar.shell.check.NoUsageOfLocalCheck;
import de.iteratec.sonar.shell.check.UsageAfterDeclarationCheck;
import de.iteratec.sonar.shell.util.FileImporter;
import de.iteratec.sonar.shell.util.PreciseIssue;
import de.iteratec.sonar.shell.util.ShellVisitorRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShellVisitorRunnerTest {

    @BeforeEach
    public void beforeEach() {
        ShellParser.create();
        ShellVisitorRunner.resetIssues();
    }

    @Test
    public void t1() {
        AstNode ast = ShellParser.parse(
                "fooBarXo() {echo xoxoxo}\n function BarBar { echo hi }\n");
        assertFalse(ShellVisitorRunner.scanTree(ast,
                new FunctionDeclarationCheck()));
    }

    @Test
    public void t2() {
        AstNode ast = ShellParser.parse(
                "fooBarXo() {echo xoxoxo}; fooBarXoXo() { echo hi }");
        assertTrue(ShellVisitorRunner.scanTree(ast,
                new FunctionDeclarationCheck()));
    }

    @Test
    public void t3() {
        AstNode ast = ShellParser.parse(
                "function fooBarXo(){\nlocal why are you using local;\n}");
        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new NoUsageOfLocalCheck(),
                new FunctionDeclarationCheck()
        );
        assertEquals(2, issues.size());
    }

    @Test
    public void t4() {
        AstNode ast = ShellParser.parse(
                "fooBarXo() {\necho no local\n}\n fooBarXo() {\nlocal is here\n}\n");
        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new NoUsageOfLocalCheck()
        );
        assertEquals(1, issues.size());
    }

    @Test
    public void t5() {
        AstNode ast = ShellParser.parse(
                "function fooBarXo(){\necho no local\n}\n fooBarXo() {\nno local here either\n}\n");

        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new NoUsageOfLocalCheck(),
                new FunctionDeclarationCheck()
        );
        assertEquals(1, issues.size());
    }

    @Test
    public void t6() {
        AstNode ast = ShellParser.parse(
                "fooBarXo(){\nlocal loco\n}\n fooBarXo(){\nno local here\n}\n");
        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new NoUsageOfLocalCheck(),
                new FunctionDeclarationCheck()
        );
        assertEquals(1, issues.size());
    }

    @Test
    public void t7() {
        AstNode ast = ShellParser.parse("xo=1\n foo(){\n" + "echo xo\n" + "}\n");
        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new UsageAfterDeclarationCheck(),
                new AllDeclaredVariablesAreReferencedCheck()
        );
        assertEquals(1, issues.size());
    }

    @Test
    public void t8() {
        AstNode ast = ShellParser.parse("foo(){\n" + "echo ${xo}\n" + "}\n xo=1\n");
        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new UsageAfterDeclarationCheck(),
                new AllDeclaredVariablesAreReferencedCheck()
        );
        assertEquals(1, issues.size());

    }

    @DisplayName("Script that used to lead to infinite loop")
    @Test
    public void t9() {
        String code = FileImporter.getFile("file/015-newrm.sh").getContent();
        AstNode ast = ShellParser.parse(code);

        List<PreciseIssue> issues = ShellVisitorRunner.scanTreeAndRaiseIssues(
                ast,
                new UsageAfterDeclarationCheck()
        );


    }
}
