package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.AstNodeType;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

@Rule(key = "SH001",
        name = "No Camel-Case in function identifiers",
        description = "Function identifier actually uses camel case",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class CamelCaseIdentifierCheck extends AbstractCheckTester {

    List<PreciseIssue> issues = new ArrayList();

    String camelCaseMessage = "You declared a function in non-camel-case-style - how could you?";

    Pattern camelCasePattern = Pattern
            .compile("[a-z0-9]+([A-Z0-9][a-z0-9]*)*");

    public List<AstNodeType> getAstNodeTypesToVisit() {
        // TODO inspired from sonar-python, what
        // do I use this for?
        return Collections.singletonList(
                ShellGrammar.FUNCTION_DEFINITION);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.sonar.sslr.api.AstVisitor#
     * visitNode(com.sonar.sslr.api.AstNode)
     */
    @Override
    public boolean visitNode(AstNode ast) {
        List<AstNode> functions = AstUtils
                .getAllOccurencesOf("FUNCTION_DEFINITION",
                        ast);
        return
                checkIfNameIsCamelCase(functions);
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        List<AstNode> functions = AstUtils
                .getAllOccurencesOf("FUNCTION_DEFINITION",
                        node);
        checkIfNameIsCamelCase(functions);

        return issues;
    }

    private boolean checkIfNameIsCamelCase(
            List<AstNode> functions) {
        for (AstNode function : functions) {
            List<AstNode> functionNameList = AstUtils
                    .getAllOccurencesOf("FUNCTION_NAME",
                            function);
            if (!camelCasePattern.matcher(
                    functionNameList.get(0).getTokenValue())
                    .matches()) {
                PreciseIssue issue = new PreciseIssue(IssueLocation.tokenLocation(functionNameList.get(0), camelCaseMessage));
                issues.add(issue);
                return false;
            }
        }
        return true;
    }

}
