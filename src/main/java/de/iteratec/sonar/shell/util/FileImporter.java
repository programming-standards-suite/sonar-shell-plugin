/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.util;

import java.io.File;

/**
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public abstract class FileImporter {

    public static ShellScriptFile getFile(String path) {

        File file = new File(path);

        return new ShellScriptFile(file);

    }

}
