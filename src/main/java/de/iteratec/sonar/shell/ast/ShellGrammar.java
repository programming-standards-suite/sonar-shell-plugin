/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.ast;

import com.sonar.sslr.api.GenericTokenType;
import com.sonar.sslr.api.Grammar;
import de.iteratec.sonar.shell.ast.ShellLexer.Keywords;
import de.iteratec.sonar.shell.ast.ShellLexer.Literals;
import de.iteratec.sonar.shell.ast.ShellLexer.Punctuators;
import org.sonar.sslr.grammar.GrammarRuleKey;
import org.sonar.sslr.grammar.LexerfulGrammarBuilder;

import static de.iteratec.sonar.shell.ast.ShellLexer.Punctuators.*;

/**
 * Bases on the grammar <a href=
 * "http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_10_02">found
 * here</a>, slightly varied at points
 * to match possibilities that are
 * offered by sslr
 *
 * @author <a href=
 * "mailto:Navid.Noorshams@iteratec.de">Navid
 * Noorshams</a>
 */
public enum ShellGrammar implements GrammarRuleKey {

    ADDITIVE_EXPRESSION, //
    AND_OR, //
    ARITHMETIC_ASSIGNMENT, //
    ASSIGNMENT_OPERATOR, //
    ASSIGNMENT_WORD, //
    BACKTICK_SUBSTITUTION, //
    BITWISE_AND, //
    BITWISE_OR, //
    BITWISE_XOR, //
    BRACED_PARAMETER, //
    BRACE_GROUP, //
    CASE_CLAUSE, //
    CASE_ITEM, //
    CASE_ITEM_OPTIONAL_DSEMI, //
    CASE_LIST, //
    CHARACTER_CLASS, //
    CMD_NAME, //
    CMD_PREFIX, //
    CMD_SUFFIX, //
    CMD_WORD, //
    COMMAND, //
    SIMPLE_WORD, //
    COMPARATORS, //
    COMPARISON, //
    COMPLETE_COMMAND, //
    COMPLETE_COMMANDS, //
    COMPOUND_COMMAND, //
    COMPOUND_LIST, //
    DEFAULT_CASE, //
    DELIMITER_OR_WHITE_SPACE, //
    DISABLED_SPECIAL_CHARACTER_DQ, //
    DOLLAR_CONSTRUCT, //
    DOUBLE_BRACKET_AND_EXPRESSION, //
    DOUBLE_BRACKET_BINARY_OPERATION, //
    DOUBLE_BRACKET_BINARY_TEST_OPERATOR, //
    DOUBLE_BRACKET_NEGATED_EXPRESSION, //
    DOUBLE_BRACKET_OR_EXPRESSION, //
    DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION, //
    DOUBLE_BRACKET_TEST, //
    DOUBLE_QUOTED_WORD, //
    DO_GROUP, //
    ELSE_PART, //
    ESCAPED_SPECIAL_CHARACTER, //
    ESCAPED_SPECIAL_CHARACTER_DQ, //
    FILENAME, //
    FOR_CLAUSE, //
    FUNCTION_BODY, //
    FUNCTION_DEFINITION, //
    FUNCTION_NAME, //
    HERE_END, //
    IF_CLAUSE, //
    IN_EQUALITY, //
    IO_FILE, //
    IO_FILE_PUNCTUATOR, //
    IO_HERE, //
    IO_HERE_PUNCTUATOR, //
    IO_REDIRECT, //
    LINEBREAK, //
    LIST, //
    LOGICAL_AND, //
    LOGICAL_OR, //
    MULTIPLICATIVE_EXPRESSION, //
    NAME, //
    NEWLINE_LIST, //
    NUMBER, //
    OPERAND, //
    PARAMETER_EXPANSION_OPERATOR, //
    PARENTHESIZED_EXPRESSION, //
    PARENTHESIZED_PARAMETER, //
    PATTERN, //
    PIPELINE, //
    PIPE_SEQUENCE, //
    PROGRAM, //
    REDIRECT_LIST, //
    REPEAT_CLAUSE, //
    SELECT_CLAUSE, //
    SEPARATOR, //
    SEPARATOR_OP, //
    SEQUENTIAL_SEP, //
    SHIFT, //
    SIMPLE_COMMAND, //
    SINGLE_BRACKET_AND_EXPRESSION, //
    SINGLE_BRACKET_BINARY_OPERATION, //
    SINGLE_BRACKET_BINARY_TEST_OPERATOR, //
    SINGLE_BRACKET_NEGATED_EXPRESSION, //
    SINGLE_BRACKET_OR_EXPRESSION, //
    SINGLE_BRACKET_PARENTHESIZED_EXPRESSION, //
    SINGLE_BRACKET_TEST, //
    SINGLE_QUOTED_WORD, //
    SPECIAL_PARAMETER, //
    COMPOSITE_WORD, //
    COMPOSITE_WORD_DQ, //
    SUBSHELL, //
    SUBSTRING, //
    TERM, //
    TERNARY_EXPRESSION, //
    TEST_SEQUENCE, //
    TILDE_EXPANSION, //
    UNARY_ARITHMETIC_OPERATOR, //
    UNARY_TEST_OPERATION, //
    UNARY_TEST_OPERATOR, //
    UNTIL_CLAUSE, //
    WHILE_CLAUSE, //
    KEYWORD, //
    WORDIFIED_PUNCTUATOR, //
    PUNCTUATOR, //
    WORD_IN_DOLLAR_CONSTRUCT, //
    ARRAY, //
    ARRAY_OPERATION, //
    NUMERIC_ARRAY, //
    SPECIAL_ARRAY, //
    ASSOCIATIVE_ARRAY, //
    LOOP_CONDITION_C_STYLE, //
    COMMAND_TERMINATOR, //
    SIMPLE_PUNCTUATOR,
    HERE_DOCUMENT,
    HERE_DOCUMENT_LINE,
    CASE_ITEM_DELIMITER,
    STRING_MODIFICATION,
    CASE_VARIATION,
    GLOBAL_SUBSTITUTION,
    LOCAL_SUBSTITUTION,
    MOCK_DOUBLE_QUOTED_WORD,
    BRACE_EXPANSION,
    BRACE_EXPANSION_LIST,
    BRACE_EXPANSION_SEQUENCE,
    ALPHABETIC_CHARACTER,
    PATH,
    PATH_COMPONENT,
    ASSIGNMENT_LEFT_SIDE,
    ASSIGNMENT_RIGHT_SIDE,
    PROCESS_SUBSTITUTION,
    LOOP_COMPONENT,
    LOOP_COMPONENTS,
    SHEBANG;

    /**
     * contains rules for arithmetic
     * expressions used in
     * command-substitution.
     * <p>
     * Rules also structure the ast
     * according to order precedence.
     * Evaluating arithmetic precedence by
     * adhering to <a href=
     * "https://sites.ualberta.ca/dept/chemeng/AIX-43/share/man/info/C/
     * a_doc_lib/aixuser/usrosdev/arithmetic_eval_korn_shell.htm">
     * the following specification</a>.
     * <p>
     * Also following the bash
     * specification, which is nearly the
     * same.
     *
     * @param g the the grammar-builder
     *          that is being instantiated
     *          in ShellGrammar.create()
     */
    public static void arithmeticExpressionGrammar(
            LexerfulGrammarBuilder g) {

        g.rule(ARITHMETIC_ASSIGNMENT).is(g.firstOf(
                g.sequence(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        NAME,
                        g.firstOf(Punctuators.INCREMENT,
                                Punctuators.DECREMENT),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                g.sequence(g.zeroOrMore(NAME,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.firstOf(ASSIGNMENT_OPERATOR,
                                COMPARATORS),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                        g.firstOf(TERNARY_EXPRESSION, NAME),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE))))
                .skipIfOneChild();

        g.rule(ASSIGNMENT_OPERATOR)
                .is(g.firstOf(Punctuators.EQUALS,
                        Punctuators.MUL_EQUALS,
                        Punctuators.SLASH_EQUALS,
                        Punctuators.PERCENTAGE_EQUALS,
                        Punctuators.ADD_EQUALS,
                        Punctuators.SUB_EQUALS,
                        Punctuators.DOUBLE_LESS_EQUALS,
                        Punctuators.DOUBLE_GREATER_EQUALS,
                        Punctuators.AMPERSAND_EQUALS,
                        Punctuators.POWER_EQUALS,
                        Punctuators.PIPE_EQUALS))
                .skipIfOneChild();

        g.rule(TERNARY_EXPRESSION).is(g.sequence(LOGICAL_OR,
                g.zeroOrMore(Punctuators.QMARK, LOGICAL_OR,
                        Punctuators.COLON, LOGICAL_OR)))
                .skipIfOneChild();

        g.rule(LOGICAL_OR)
                .is(g.sequence(LOGICAL_AND,
                        g.zeroOrMore(Punctuators.OR,
                                LOGICAL_AND)))
                .skipIfOneChild();

        g.rule(LOGICAL_AND)
                .is(g.sequence(BITWISE_OR,
                        g.zeroOrMore(Punctuators.AND,
                                BITWISE_OR)))
                .skipIfOneChild();

        g.rule(BITWISE_OR)
                .is(g.sequence(BITWISE_XOR,
                        g.zeroOrMore(Punctuators.PIPE,
                                BITWISE_XOR)))
                .skipIfOneChild();

        g.rule(BITWISE_XOR)
                .is(g.sequence(BITWISE_AND,
                        g.zeroOrMore(Punctuators.POWER,
                                BITWISE_AND)))
                .skipIfOneChild();

        g.rule(BITWISE_AND)
                .is(g.sequence(IN_EQUALITY,
                        g.zeroOrMore(Punctuators.AMPERSAND,
                                IN_EQUALITY)))
                .skipIfOneChild();

        g.rule(IN_EQUALITY)
                .is(g.sequence(COMPARISON,
                        g.zeroOrMore(
                                g.firstOf(Punctuators.EQ,
                                        Punctuators.NE),
                                COMPARISON)))
                .skipIfOneChild();

        g.rule(COMPARISON)
                .is(g.sequence(SHIFT,
                        g.zeroOrMore(COMPARATORS, SHIFT)))
                .skipIfOneChild();

        g.rule(COMPARATORS)
                .is(g.firstOf(Punctuators.LE,
                        Punctuators.GE, Punctuators.LT,
                        Punctuators.GT));

        g.rule(SHIFT).is(g.sequence(ADDITIVE_EXPRESSION,
                g.zeroOrMore(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.firstOf(Punctuators.DOUBLE_GT,
                                Punctuators.DOUBLE_LT),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        ADDITIVE_EXPRESSION)))
                .skipIfOneChild();

        g.rule(ADDITIVE_EXPRESSION)
                .is(MULTIPLICATIVE_EXPRESSION, g.zeroOrMore(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.firstOf(Punctuators.ADD,
                                Punctuators.SUB),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        MULTIPLICATIVE_EXPRESSION))
                .skipIfOneChild();

        g.rule(MULTIPLICATIVE_EXPRESSION).is(g.sequence(
                PARENTHESIZED_EXPRESSION,
                g.zeroOrMore(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.firstOf(Punctuators.MUL,
                                Punctuators.SLASH,
                                Punctuators.PERCENTAGE),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        OPERAND)))
                .skipIfOneChild();

        g.rule(PARENTHESIZED_EXPRESSION).is(g.firstOf(
                g.sequence(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.ESCAPE,
                        Punctuators.PAREN_L,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        ARITHMETIC_ASSIGNMENT,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.ESCAPE,
                        Punctuators.PAREN_R,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                // TODO escaped parens only work with
                // expr - create own branches for expr
                g.sequence(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.ESCAPE,
                        Punctuators.PAREN_L,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        OPERAND,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.ESCAPE,
                        Punctuators.PAREN_R,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                g.sequence(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.PAREN_L,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        ARITHMETIC_ASSIGNMENT,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.PAREN_R,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                g.sequence(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.PAREN_L,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        OPERAND,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.PAREN_R,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                OPERAND)).skipIfOneChild();

        g.rule(OPERAND).is(//
                g.sequence( //
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.zeroOrMore(
                                UNARY_ARITHMETIC_OPERATOR), //
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        g.firstOf(//
                                Literals.OCTAL, //
                                Literals.HEXADECIMAL, //
                                NUMBER, //
                                DOLLAR_CONSTRUCT, NAME),
                        g.optional(DELIMITER_OR_WHITE_SPACE)//
                )//
        );

        g.rule(UNARY_ARITHMETIC_OPERATOR).is(//
                g.firstOf(//
                        Punctuators.SUB, //
                        Punctuators.ADD, //
                        Punctuators.TILDE, //
                        Punctuators.BANG //

                )//
        );

        g.rule(SPECIAL_PARAMETER).is(//
                g.firstOf(//
                        Punctuators.MUL, //
                        Punctuators.AT_SIGN, //
                        Punctuators.HASH, //
                        Punctuators.SUB, //
                        Punctuators.DOLLAR, //
                        Punctuators.BANG, //
                        Punctuators.QMARK
                        //
                )//
        );

    }

    /**
     * contains rules structuring each kind
     * of command. Differentiates between
     * compound_command, simple_command and
     * definition of functions. Also
     * <ul>
     * <li>For-Clauses
     * <li>Case-Clauses
     * <li>If-Clauses
     * <li>While-Clauses
     * <li>Until-Clauses
     * <li>Repeat-Clauses
     * <li>Select-Clauses
     * </ul>
     *
     * @param g the the grammar-builder
     *          that is being instantiated
     *          in ShellGrammar.create()
     */
    public static void commandsGrammar(
            LexerfulGrammarBuilder g) {

        g.rule(COMMAND).is(//
                g.optional(
                        g.zeroOrMore(GenericTokenType.EOL),
                        Literals.DELIMITER),
                g.firstOf( //
                        FUNCTION_DEFINITION, //
                        g.sequence(//
                                COMPOUND_COMMAND,
                                REDIRECT_LIST//
                        ), //
                        COMPOUND_COMMAND, //
                        SIMPLE_COMMAND//
                )//
        ).skipIfOneChild();

        g.rule(COMPOUND_COMMAND).is(//
                g.firstOf(//
                        BRACE_GROUP, //
                        SUBSHELL, //
                        FOR_CLAUSE, //
                        CASE_CLAUSE, //
                        IF_CLAUSE, //
                        WHILE_CLAUSE, //
                        UNTIL_CLAUSE, //
                        REPEAT_CLAUSE, //
                        SELECT_CLAUSE//
                )//
        ).skipIfOneChild();

        g.rule(SUBSHELL).is(//
                // g.firstOf(
                g.sequence(Punctuators.PAREN_L, g.optional(
                        g.optional(Literals.DELIMITER),
                        COMPOUND_LIST), Punctuators.PAREN_R,
                        g.optional(Literals.DELIMITER))// ,
                // g.sequence(Punctuators.PAREN_L,
                // Punctuators.PAREN_R))
        );
        /*
         * The optional has been added here (and
         * at other places) so that linebreaks
         * do not randomly appear.
         */
        g.rule(COMPOUND_LIST).is(//
                g.optional(LINEBREAK), TERM,
                g.optional(Literals.DELIMITER)//
        );

        g.rule(TERM).is(//
                g.sequence(//
                        AND_OR,
                        g.zeroOrMore(SEPARATOR, AND_OR),
                        g.optional(SEPARATOR)//
                )//
        ).skipIfOneChild();

        g.rule(SELECT_CLAUSE).is(//
                Keywords.SELECT, Literals.DELIMITER, NAME,
                g.optional(Literals.DELIMITER), Keywords.IN,
                Literals.DELIMITER,
                g.zeroOrMore(COMPOSITE_WORD,
                        g.optional(Literals.DELIMITER)),
                SEQUENTIAL_SEP, //
                DO_GROUP//
        );

        g.rule(FOR_CLAUSE).is(//
                g.firstOf(//
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                Keywords.IN,
                                g.optional(
                                        Literals.DELIMITER),
                                g.zeroOrMore(COMPOSITE_WORD,
                                        g.optional(
                                                Literals.DELIMITER)),
                                g.firstOf(g.sequence(
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                DO_GROUP//
                        ), // TODO add a newline, since you
                        // can also terminate in-clause
                        // with that instead of
                        // semicolon
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                LINEBREAK, Keywords.IN,
                                g.optional(
                                        Literals.DELIMITER),
                                g.firstOf(g.sequence(
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                g.optional(
                                        Literals.DELIMITER),
                                DO_GROUP//
                        ), //
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                g.firstOf(g.sequence(
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                g.optional(
                                        Literals.DELIMITER),
                                DO_GROUP//
                        ), //
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                g.firstOf(g.sequence(
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                        SEQUENTIAL_SEP,
                                        LINEBREAK),
                                g.optional(
                                        Literals.DELIMITER),
                                DO_GROUP//
                        ), //
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                LOOP_CONDITION_C_STYLE,
                                g.optional(
                                        Literals.DELIMITER),
                                g.firstOf(SEQUENTIAL_SEP, LINEBREAK),
                                g.optional(
                                        Literals.DELIMITER),
                                DO_GROUP//
                        ), //
                        g.sequence(//
                                Keywords.FOR,
                                g.optional(
                                        Literals.DELIMITER),
                                NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                DO_GROUP//
                        ) //
                )//
        );
        //(( loop_component ; loop_component ; loop_component ))
        g.rule(LOOP_CONDITION_C_STYLE).is(
                Punctuators.PAREN_L, Punctuators.PAREN_L,
                g.optional(Literals.DELIMITER),
                LOOP_COMPONENTS,
                g.optional(Literals.DELIMITER), SEMICOLON,
                LOOP_COMPONENTS,
                g.optional(Literals.DELIMITER), SEMICOLON,
                g.optional(LOOP_COMPONENTS),
                g.optional(Literals.DELIMITER),
                Punctuators.PAREN_R, Punctuators.PAREN_R
        );
        //j=1, x=3 ...
        g.rule(LOOP_COMPONENTS).is(
                LOOP_COMPONENT,
                g.zeroOrMore(
                        g.optional(Literals.DELIMITER),
                        COMMA,
                        g.optional(Literals.DELIMITER),
                        LOOP_COMPONENT
                )
        );

        g.rule(LOOP_COMPONENT).is(
                ARITHMETIC_ASSIGNMENT
        );

        g.rule(CASE_CLAUSE).is(//
                g.firstOf(//
                        g.sequence(// posix,bash,ksh,zsh
                                Keywords.CASE,
                                g.optional(Literals.DELIMITER),
                                COMPOSITE_WORD,
                                g.optional(Literals.DELIMITER),
                                Keywords.IN,
                                g.optional(Literals.DELIMITER), //
                                LINEBREAK, CASE_LIST,
                                Keywords.ESAC//
                        ), g.sequence(// tcsh
                                Keywords.SWITCH,
                                Literals.DELIMITER,
                                Punctuators.PAREN_L,
                                COMPOSITE_WORD,
                                Punctuators.PAREN_R,
                                LINEBREAK, CASE_LIST,
                                g.optional(DEFAULT_CASE),
                                Keywords.ENDSW//
                        )//
                )//
        );

        g.rule(CASE_LIST).is(//
                g.zeroOrMore(CASE_ITEM), //
                // last item could have no
                // double-semicolon
                g.optional(CASE_ITEM_OPTIONAL_DSEMI)//
        );

        g.rule(CASE_ITEM_DELIMITER)
                .is(g.firstOf(Punctuators.DOUBLE_SEMI_AND,
                        Punctuators.SEMICOLON_AND,
                        Punctuators.DOUBLE_SEMI)

                );

        g.rule(CASE_ITEM).is(//
                g.firstOf(//
                        g.sequence(// posix,bash,ksh,zsh
                                Punctuators.PAREN_L,
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST,
                                CASE_ITEM_DELIMITER,
                                LINEBREAK//
                        ), //
                        g.sequence(// posix,bash,ksh,zsh
                                Punctuators.PAREN_L,
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                LINEBREAK,
                                CASE_ITEM_DELIMITER,
                                LINEBREAK//
                        ), //
                        g.sequence(// posix,bash,ksh,zsh
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST,
                                CASE_ITEM_DELIMITER,
                                LINEBREAK//
                        ), //
                        g.sequence( // posix,bash,ksh,zsh
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                LINEBREAK,
                                CASE_ITEM_DELIMITER,
                                LINEBREAK//
                        ), g.sequence( // tcsh-compliant
                                Keywords.CASE,
                                Literals.DELIMITER,
                                GenericTokenType.IDENTIFIER,
                                Punctuators.COLON,
                                Literals.DELIMITER,
                                LINEBREAK, COMPOUND_LIST,
                                Keywords.BREAKSW, LINEBREAK//
                        )//
                )//

        );

        g.rule(DEFAULT_CASE).is(Keywords.DEFAULT,
                Punctuators.COLON, Literals.DELIMITER,
                LINEBREAK, COMPOUND_LIST, Keywords.BREAKSW,
                LINEBREAK);

        g.rule(CASE_ITEM_OPTIONAL_DSEMI).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.PAREN_L,
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                COMPOUND_LIST,
                                g.optional(
                                        CASE_ITEM_DELIMITER),
                                LINEBREAK//
                        ), //
                        g.sequence(//
                                Punctuators.PAREN_L,
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                LINEBREAK,
                                g.optional(
                                        CASE_ITEM_DELIMITER),
                                LINEBREAK//
                        ), //
                        g.sequence(//
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                COMPOUND_LIST,
                                g.optional(
                                        CASE_ITEM_DELIMITER),
                                LINEBREAK//
                        ), //
                        g.sequence(//
                                PATTERN,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_R,
                                LINEBREAK,
                                g.optional(
                                        CASE_ITEM_DELIMITER),
                                LINEBREAK//
                        )//
                )//
        );

        g.rule(PATTERN).is(//
                g.optional(Punctuators.POWER),
                g.oneOrMore(g.firstOf(CHARACTER_CLASS,
                        COMPOSITE_WORD)),
                g.zeroOrMore(g.optional(Literals.DELIMITER),
                        Punctuators.PIPE,
                        g.optional(Literals.DELIMITER),
                        g.oneOrMore(
                                g.firstOf(CHARACTER_CLASS,
                                        COMPOSITE_WORD)),
                        g.optional(Literals.DELIMITER)), //
                g.optional(Punctuators.DOLLAR));

        g.rule(IF_CLAUSE).is(//
                g.firstOf( //
                        g.sequence(//
                                Keywords.IF, COMPOUND_LIST,
                                Keywords.THEN,
                                COMPOUND_LIST,
                                g.optional(ELSE_PART),
                                Keywords.FI, g.optional(
                                        Literals.DELIMITER)//
                        ), // tcsh-compliant
                        g.sequence(//
                                Keywords.IF,
                                Literals.DELIMITER,
                                Punctuators.PAREN_L,
                                COMPOUND_LIST,
                                Punctuators.PAREN_R,
                                Literals.DELIMITER,
                                Keywords.THEN,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST,
                                g.zeroOrMore(Keywords.ELSE,
                                        Literals.DELIMITER,
                                        Keywords.IF,
                                        Literals.DELIMITER,
                                        Punctuators.PAREN_L,
                                        COMPOUND_LIST,
                                        Punctuators.PAREN_R,
                                        g.optional(
                                                Literals.DELIMITER),
                                        Keywords.THEN,
                                        COMPOUND_LIST),
                                g.optional(Keywords.ELSE,
                                        COMPOUND_LIST), //
                                Keywords.ENDIF//
                        )//
                ) //
        );

        g.rule(ELSE_PART).is(//
                g.firstOf(//
                        g.sequence(//
                                Keywords.ELIF,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST,
                                Keywords.THEN,
                                COMPOUND_LIST, ELSE_PART//
                        ), //
                        g.sequence(//
                                Keywords.ELIF,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST,
                                Keywords.THEN, COMPOUND_LIST//
                        ), //
                        g.sequence(//
                                Keywords.ELSE,
                                g.optional(
                                        Literals.DELIMITER),
                                COMPOUND_LIST//
                        )//
                )//
        );

        g.rule(WHILE_CLAUSE).is(//
                g.firstOf(
                        // POSIX, BASH, KSH, ZSH-compliant
                        g.sequence(//
                                Keywords.WHILE,
                                COMPOUND_LIST, DO_GROUP//
                        ),
                        // TCSH-compliant
                        g.sequence(//
                                Keywords.WHILE,
                                Literals.DELIMITER,
                                Punctuators.PAREN_L,
                                COMPOUND_LIST,
                                Punctuators.PAREN_R,
                                GenericTokenType.EOL,
                                COMPOUND_LIST, Keywords.END//
                        )//
                )//
        );

        g.rule(REPEAT_CLAUSE).is(//
                g.sequence(//
                        Keywords.REPEAT, Literals.DELIMITER,
                        NUMBER, Literals.DELIMITER,
                        g.firstOf(//
                                // ZSH
                                DO_GROUP, //
                                // TCSH
                                COMPOUND_LIST//
                        )//
                )//
        );

        g.rule(UNTIL_CLAUSE).is(//
                Keywords.UNTIL, COMPOUND_LIST, DO_GROUP//
        );

        g.rule(FUNCTION_DEFINITION).is(//
                g.firstOf(//
                        g.sequence(Keywords.FUNCTION,
                                Literals.DELIMITER,
                                FUNCTION_NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_L, //
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                FUNCTION_BODY),
                        g.sequence(Keywords.FUNCTION,
                                Literals.DELIMITER,
                                FUNCTION_NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                FUNCTION_BODY),
                        g.sequence(
                                g.optional(
                                        Keywords.FUNCTION,
                                        Literals.DELIMITER),
                                FUNCTION_NAME,
                                g.optional(
                                        Literals.DELIMITER),
                                Punctuators.PAREN_L, //
                                Punctuators.PAREN_R,
                                g.optional(
                                        Literals.DELIMITER),
                                FUNCTION_BODY)//
                )//
        );

        g.rule(FUNCTION_NAME).is(g.firstOf(KEYWORD,
                GenericTokenType.IDENTIFIER));

        g.rule(FUNCTION_BODY).is(//
                g.optional(GenericTokenType.EOL), g.firstOf(//
                        g.sequence(//
                                COMPOUND_COMMAND,
                                REDIRECT_LIST//
                        ), //
                        COMPOUND_COMMAND//
                )//
        );

        g.rule(BRACE_GROUP).is(//
                g.sequence(//
                        BRACE_L,
                        g.optional(Literals.DELIMITER),
                        COMPOUND_LIST,
                        g.optional(Literals.DELIMITER),
                        Punctuators.BRACE_R//
                )//
        );

        g.rule(DO_GROUP).is(//
                g.sequence(//
                        Keywords.DO,
                        g.optional(Literals.DELIMITER),
                        COMPOUND_LIST, Keywords.DONE));

        g.rule(ASSIGNMENT_WORD).is(//
                ASSIGNMENT_LEFT_SIDE, Punctuators.EQUALS,
                ASSIGNMENT_RIGHT_SIDE);

        g.rule(ASSIGNMENT_LEFT_SIDE)
                .is(g.firstOf(DOLLAR_CONSTRUCT,
                        NUMERIC_ARRAY, SIMPLE_WORD));

        g.rule(ASSIGNMENT_RIGHT_SIDE).is(g.oneOrMore(
                g.firstOf(SUBSHELL, COMPOSITE_WORD)));

        g.rule(SIMPLE_COMMAND).is(//
                g.firstOf(//
                        g.sequence(//
                                g.firstOf(g.sequence(
                                        Punctuators.BANG,
                                        Literals.DELIMITER),
                                        g.oneOrMore(//
                                                CMD_PREFIX,
                                                g.optional(
                                                        Literals.DELIMITER)//
                                        )), CMD_WORD,
                                g.optional(
                                        Literals.DELIMITER),
                                g.zeroOrMore(CMD_SUFFIX,
                                        g.optional(
                                                Literals.DELIMITER)) //
                        ), //
                        g.oneOrMore(CMD_PREFIX, g.optional(
                                Literals.DELIMITER)), //

                        g.sequence(TEST_SEQUENCE,
                                g.optional(
                                        Literals.DELIMITER)), //

                        g.sequence("expr",
                                Literals.DELIMITER,
                                ARITHMETIC_ASSIGNMENT),
                        g.sequence(//
                                CMD_NAME, //
                                g.zeroOrMore(//
                                        g.optional(
                                                DELIMITER_OR_WHITE_SPACE),
                                        CMD_SUFFIX//
                                ), //
                                g.optional(
                                        DELIMITER_OR_WHITE_SPACE))));

        g.rule(CMD_WORD).is(COMPOSITE_WORD);

        g.rule(CMD_NAME)
                .is(g.optional(GenericTokenType.EOL),
                        g.firstOf(
                                // TODO you can virtually
                                // put anything here, maybe
                                // merge with composite
                                // word?
                                NAME, DOLLAR_CONSTRUCT,
                                DOUBLE_QUOTED_WORD,
                                SINGLE_QUOTED_WORD,
                                g.oneOrMore(SIMPLE_WORD)))
                .skipIfOneChild();

        g.rule(NAME).is(GenericTokenType.IDENTIFIER)
                .skipIfOneChild();

        g.rule(CMD_PREFIX).is(//
                g.firstOf(//
                        IO_REDIRECT, ASSIGNMENT_WORD));

        g.rule(CMD_SUFFIX).is(//
                g.firstOf(//
                        PATH,
                        IO_REDIRECT, //
                        ASSIGNMENT_WORD,
                        SINGLE_QUOTED_WORD,
                        PROCESS_SUBSTITUTION,
                        BRACE_EXPANSION,
                        g.sequence(COMPOSITE_WORD,
                                g.optional(
                                        Literals.DELIMITER),
                                g.optional(
                                        Punctuators.BRACE_R))));

        g.rule(PROCESS_SUBSTITUTION).is(
                Punctuators.LT, Punctuators.PAREN_L, COMPLETE_COMMAND, Punctuators.PAREN_R
        );

        g.rule(BRACE_EXPANSION).is(Punctuators.BRACE_L,
                g.firstOf(BRACE_EXPANSION_LIST,
                        BRACE_EXPANSION_SEQUENCE),
                Punctuators.BRACE_R

        );

        g.rule(BRACE_EXPANSION_LIST).is(COMPOSITE_WORD,
                g.zeroOrMore(Punctuators.COMMA,
                        COMPOSITE_WORD));

        g.rule(BRACE_EXPANSION_SEQUENCE)
                .is(g.firstOf(
                        g.sequence(NUMBER, "..", NUMBER),
                        g.sequence(ALPHABETIC_CHARACTER,
                                "..",
                                ALPHABETIC_CHARACTER)));

        g.rule(ALPHABETIC_CHARACTER)
                .is(g.firstOf("a", "b", "c", "d", "e", "f",
                        "g", "h", "i", "j", "k", "l", "m",
                        "n", "o", "p", "q", "r", "s", "t",
                        "u", "v", "w", "x", "y", "z"));

        g.rule(REDIRECT_LIST).is(//
                g.oneOrMore(IO_REDIRECT)

        );

        g.rule(IO_REDIRECT).is(//
                g.optional(DELIMITER_OR_WHITE_SPACE), //
                g.firstOf(//
                        g.sequence(//
                                g.optional(NUMBER), IO_FILE //
                        ), //
                        g.sequence(//
                                g.optional(NUMBER), IO_HERE//
                        )));

        g.rule(NUMBER).is(//
                g.firstOf(//
                        Literals.DIGIT, //
                        Literals.INTEGER))
                .skipIfOneChild();

        g.rule(IO_FILE).is(//
                g.sequence(IO_FILE_PUNCTUATOR,
                        g.optional(Literals.DELIMITER),
                        FILENAME));

        g.rule(IO_FILE_PUNCTUATOR).is(//
                g.optional(Literals.DELIMITER), g.firstOf(//
                        Punctuators.LT, //
                        Punctuators.LESS_AND, //
                        Punctuators.GT, //
                        Punctuators.GREATER_AND, //
                        Punctuators.DOUBLE_GT, //
                        Punctuators.LESS_GREATER, //
                        Punctuators.CLOBBER, //
                        // tcsh
                        Punctuators.PIPE_AND), //
                g.optional(Literals.DELIMITER));

        g.rule(FILENAME).is(COMPOSITE_WORD);

        g.rule(IO_HERE).is(//
                g.firstOf(g.sequence(IO_HERE_PUNCTUATOR,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        HERE_DOCUMENT),
                        // BASH, KSH, ZSH
                        g.sequence(TRIPLE_LT, g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                                g.firstOf(
                                        DOUBLE_QUOTED_WORD,
                                        MOCK_DOUBLE_QUOTED_WORD))));

        /*
         * used when a double quoted word inside
         * of a double quoted word is required,
         * see t67 in BashShellParserTest.java
         * as example. represents a shallow form
         * of a double-quoted-word
         */
        g.rule(MOCK_DOUBLE_QUOTED_WORD).is(DOUBLE_QUOTE,
                g.firstOf(g.oneOrMore(
                        g.firstOf(KEYWORD, SIMPLE_WORD),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE)),
                        DOLLAR_CONSTRUCT),
                DOUBLE_QUOTE);

        g.rule(HERE_DOCUMENT).is(g.firstOf(
                g.sequence(Punctuators.DOUBLE_QUOTE,
                        Literals.HERE_DOCUMENT_TOKEN,
                        Punctuators.DOUBLE_QUOTE),
                Literals.HERE_DOCUMENT_TOKEN),
                g.optional(g.optional(IO_FILE),
                        GenericTokenType.EOL,
                        g.zeroOrMore(
                                g.oneOrMore(
                                        HERE_DOCUMENT_LINE),
                                HERE_END)));

        g.rule(HERE_DOCUMENT_LINE).is(
                g.zeroOrMore(g.firstOf(Literals.DELIMITER,
                        COMPOSITE_WORD,
                        WORDIFIED_PUNCTUATOR, KEYWORD)),
                GenericTokenType.EOL);

        g.rule(HERE_END).is(Literals.HERE_DOCUMENT_TOKEN,
                LINEBREAK);

        g.rule(IO_HERE_PUNCTUATOR).is(//
                g.firstOf(//
                        Punctuators.DOUBLE_LESS_DASH, //
                        Punctuators.DOUBLE_LT));

        g.rule(DELIMITER_OR_WHITE_SPACE)
                .is(g.firstOf(Literals.DELIMITER,
                        Literals.WHITE_SPACE))
                .skip();
    }

    /**
     * Contains rules that represent the
     * word expansion that is undertaken for
     * every word. The specific word
     * expansions are as follows:
     *
     * <ul>
     * <li>Parameter expansion denoted by
     * the $-character
     * <li>Command-Substitutions ($() and
     * Backtick-based)
     * <li>Arithmetic Expansion ($(()))
     * <li>Tilde-Expansion (~)
     * </ul>
     * <p>
     * Also, contains rules for
     * double-quotes and single-quotes.
     *
     * @param g the the grammar-builder
     *          that is being instantiated
     *          in ShellGrammar.create()
     */
    public static void wordExpansionGrammar(
            LexerfulGrammarBuilder g) {

        /**
         * a node that merges the values of many
         * subnodes. usually employed as an
         * argument in the shell-script,
         * delimited with with white-spaces,
         * e.g. echo ${foo}bar (where echo is
         * the command name).
         */
        g.rule(COMPOSITE_WORD).is(//
                g.oneOrMore(//
                        g.firstOf(//
                                SINGLE_QUOTED_WORD, //
                                DOLLAR_CONSTRUCT, //
                                DOUBLE_QUOTED_WORD, //
                                TILDE_EXPANSION, //
                                BACKTICK_SUBSTITUTION, //
                                ARRAY, PUNCTUATOR, KEYWORD, //
                                SIMPLE_WORD//
                        )//
                )//
        ).skip();

        g.rule(ARRAY).is(g.firstOf(ASSOCIATIVE_ARRAY,
                NUMERIC_ARRAY, SPECIAL_ARRAY));

        g.rule(NUMERIC_ARRAY).is(NAME,
                Punctuators.BRACKET_L,
                g.firstOf(NUMBER, DOLLAR_CONSTRUCT,
                        ADDITIVE_EXPRESSION,
                        g.sequence(Punctuators.SUB, "1")),
                Punctuators.BRACKET_R);

        g.rule(SPECIAL_ARRAY).is(NAME,
                Punctuators.BRACKET_L,
                g.firstOf(Punctuators.AT_SIGN,
                        Punctuators.MUL),
                Punctuators.BRACKET_R);

        g.rule(ASSOCIATIVE_ARRAY).is(NAME,
                Punctuators.BRACKET_L, NAME,
                Punctuators.BRACKET_R);

        /**
         * ${foo:-bar}, where bar is the word in
         * dollar construct
         */
        g.rule(WORD_IN_DOLLAR_CONSTRUCT).is(g.firstOf(//
                SINGLE_QUOTED_WORD, //
                DOLLAR_CONSTRUCT, //
                DOUBLE_QUOTED_WORD, //
                TILDE_EXPANSION, //
                BACKTICK_SUBSTITUTION, //
                KEYWORD, SIMPLE_WORD)//
        ).skipIfOneChild();

        /*
         * word that employs characters with no
         * particular meanings or or special
         * character that have been disabled
         * with an escape
         *
         */
        g.rule(SIMPLE_WORD).is(//
                g.firstOf(ESCAPED_SPECIAL_CHARACTER,
                        g.sequence(
                                g.optional(
                                        Punctuators.ESCAPE),
                                g.firstOf(//
                                        NUMBER, //
                                        NAME,
                                        Literals.OCTAL,
                                        Literals.HEXADECIMAL,
                                        g.oneOrMore(
                                                PUNCTUATOR))//
                        ))//
        ).skipIfOneChild();

        /*
         * single quoted words usually are being
         * fully generated on lexer level as
         * IDENTIFIER through
         * SingleQuoteIdentifierChannel.java But
         * sometimes, the generation of a single
         * quoted word in double quoted context
         * (e.g. "$(hello 'world')") needs to be
         * done on grammar level. Thus, aside of
         * merely using IDENTIFIER, the other
         * tokens are required in this branch.
         */
        g.rule(SINGLE_QUOTED_WORD).is(//
                Punctuators.SINGLE_QUOTE, //
                g.zeroOrMore(g.firstOf(
                        GenericTokenType.IDENTIFIER,
                        WORDIFIED_PUNCTUATOR, SIMPLE_WORD,
                        Literals.WHITE_SPACE,
                        Punctuators.DOUBLE_QUOTE,
                        Literals.DELIMITER)),
                Punctuators.SINGLE_QUOTE//
        );

        /*
         * a node that merges the values of many
         * subnodes in a double-quoted-context.
         * In comparison to COMPOSITE_WORD, the
         * meaning of specific characters (e.g.
         * single quotes) gets disabled.
         */
        g.rule(COMPOSITE_WORD_DQ).is(//
                g.oneOrMore( //
                        g.firstOf(//
                                DOLLAR_CONSTRUCT,
                                DISABLED_SPECIAL_CHARACTER_DQ, //
                                ESCAPED_SPECIAL_CHARACTER_DQ, //
                                // SINGLE_QUOTED_WORD,
                                COMPOSITE_WORD, //
                                WORDIFIED_PUNCTUATOR, // basically
                                // all
                                // punctuators
                                // can
                                // be
                                // used
                                // here
                                Literals.WHITE_SPACE, //
                                Literals.DELIMITER)//
                )//
        ).skip();

        g.rule(DISABLED_SPECIAL_CHARACTER_DQ)
                .is(g.firstOf(Punctuators.TILDE,
                        Punctuators.SINGLE_QUOTE));

        g.rule(STRING_MODIFICATION)
                .is(g.firstOf(NUMBER, NAME, KEYWORD,
                        ARRAY_OPERATION),
                        g.firstOf(SUBSTRING, CASE_VARIATION,
                                GLOBAL_SUBSTITUTION,
                                LOCAL_SUBSTITUTION)

                );

        g.rule(LOCAL_SUBSTITUTION).is(Punctuators.SLASH,
                WORDIFIED_PUNCTUATOR, Punctuators.SLASH,
                WORDIFIED_PUNCTUATOR);
        g.rule(GLOBAL_SUBSTITUTION).is(Punctuators.SLASH,
                Punctuators.SLASH,
                g.firstOf(NAME, NUMBER,
                        WORDIFIED_PUNCTUATOR),
                Punctuators.SLASH, g.firstOf(NAME, NUMBER,
                        WORDIFIED_PUNCTUATOR));

        g.rule(CASE_VARIATION)
                .is(g.firstOf(Punctuators.DOUBLE_COMMA,
                        Punctuators.COMMA,
                        Punctuators.DOUBLE_POWER,
                        Punctuators.POWER));

        g.rule(BRACED_PARAMETER).is( //
                Punctuators.BRACE_L,
                g.optional(Punctuators.BANG), g.firstOf(//
                        g.sequence(//
                                g.firstOf(NAME, KEYWORD,
                                        Literals.DIGIT),
                                PARAMETER_EXPANSION_OPERATOR,
                                g.oneOrMore(
                                        WORD_IN_DOLLAR_CONSTRUCT)//
                        ), //
                        g.sequence(// required for
                                // ${blankWordAfterOperator:-}
                                g.firstOf(NAME, KEYWORD,
                                        Literals.DIGIT),
                                PARAMETER_EXPANSION_OPERATOR),
                        STRING_MODIFICATION,
                        ARRAY_OPERATION, g.sequence(//
                                g.optional(
                                        Punctuators.HASH),
                                g.firstOf(NAME, KEYWORD) //
                        ), //
                        // TODO find more exceptional
                        // cases like BANG and group
                        // them properly
                        Punctuators.HASH,
                        Punctuators.AT_SIGN,
                        Punctuators.QMARK, Literals.DIGIT, //
                        Literals.OCTAL, //
                        Literals.HEXADECIMAL, //
                        Literals.INTEGER//
                ), //
                Punctuators.BRACE_R);

        g.rule(ARRAY_OPERATION)
                .is(g.firstOf(
                        g.sequence(ARRAY, Punctuators.COLON,
                                NUMBER, Punctuators.COLON,
                                g.firstOf(DOLLAR_CONSTRUCT,
                                        NUMBER)),
                        g.sequence(Punctuators.HASH, ARRAY),
                        g.sequence(Punctuators.BANG, ARRAY),
                        ARRAY));

        g.rule(SUBSTRING).is(Punctuators.COLON,
                g.firstOf(DOLLAR_CONSTRUCT, NUMBER),
                g.optional(Punctuators.COLON, g.firstOf(
                        DOLLAR_CONSTRUCT, NUMBER)));

        g.rule(PARAMETER_EXPANSION_OPERATOR).is(//
                g.firstOf(//
                        Punctuators.DOUBLE_PERCENTAGE, //
                        Punctuators.PERCENTAGE, //
                        Punctuators.DOUBLE_HASH, //
                        Punctuators.HASH, //
                        Punctuators.COLON_SUB, //
                        Punctuators.SUB, //
                        Punctuators.COLON_ADD, //
                        Punctuators.ADD, //
                        Punctuators.COLON_QMARK, //
                        Punctuators.QMARK, //
                        Punctuators.COLON_EQUALS, //
                        Punctuators.EQUALS, //
                        SUBSTRING)//
        );
        g.rule(DOUBLE_QUOTED_WORD).is(//
                Punctuators.DOUBLE_QUOTE,
                g.optional(COMPOSITE_WORD_DQ),
                Punctuators.DOUBLE_QUOTE//
        );

        g.rule(KEYWORD).is(//
                g.firstOf(//
                        Keywords.CASE, //
                        Keywords.COPROC, //
                        Keywords.DO, //
                        Keywords.DONE, //
                        Keywords.ELIF, //
                        Keywords.ELSE, //
                        Keywords.ESAC, //
                        Keywords.FI, //
                        Keywords.FOR, //
                        Keywords.FUNCTION, //
                        Keywords.IF, //
                        Keywords.IN, //
                        Keywords.SELECT, //
                        Keywords.THEN, //
                        Keywords.TIME, //
                        Keywords.UNTIL, // //
                        Keywords.WHILE, //
                        Keywords.REPEAT, //
                        Keywords.END, //
                        Keywords.ENDIF, //
                        Keywords.SWITCH, //
                        Keywords.BREAKSW, //
                        Keywords.ENDSW, //
                        Keywords.DEFAULT)//
        ).skipIfOneChild();

        g.rule(CHARACTER_CLASS).is(Punctuators.BRACKET_L,
                g.oneOrMore(SIMPLE_WORD),
                Punctuators.BRACKET_R);

        g.rule(PATH).is(
                g.optional(Punctuators.SLASH),
                g.firstOf(
                        TILDE_EXPANSION,
                        g.sequence(
                                PATH_COMPONENT,
                                Punctuators.SLASH,
                                PATH_COMPONENT
                        )
                ),
                g.zeroOrMore(
                        Punctuators.SLASH,
                        PATH_COMPONENT
                ),
                g.optional(
                        GenericTokenType.IDENTIFIER,
                        Punctuators.MUL
                )
        );

        g.rule(PATH_COMPONENT).is(
                g.firstOf(
                        "..", ".",
                        GenericTokenType.IDENTIFIER
                )
        ).skip();

        /*
         * Entirety of all punctuators that can
         * be used inside of a double-quote or
         * single-quote-context. This therefore
         * excludes Punctuators.SINGLE_QUOTE and
         * Punctuators.DOUBLE_QUOTE. Else, the
         * closing double-quote will be turned
         * into a word and the quoted composite
         * word will lack a closing quote, thus
         * not building the quoted composite
         * word. The escaped forms of the quotes
         * are properly reflected in the branch
         * denoted by
         * ESCAPED_SPECIAL_CHARACTER_DQ.
         */
        g.rule(WORDIFIED_PUNCTUATOR).is(//
                g.firstOf(//
                        SIMPLE_PUNCTUATOR,
                        COMMAND_TERMINATOR)//
        );

        /*
         * Entirety of all punctuators that
         * abruptly terminate or connect simple
         * commands with one another. These
         * specific tokens cannot be used as an
         * argument to a command, unless they're
         * escaped
         */
        g.rule(COMMAND_TERMINATOR).is(g.firstOf(
                Punctuators.AND, Punctuators.AMPERSAND,
                Punctuators.BRACE_R,
                Punctuators.DOUBLE_SEMI, Punctuators.DOLLAR,
                Punctuators.GT, Punctuators.LT,
                Punctuators.SEMICOLON, Punctuators.PIPE,
                Punctuators.PAREN_L, Punctuators.PAREN_R,
                Punctuators.OR)).skipIfOneChild();

        /*
         * Denotes all punctuators that can be
         * used as arguments in commands without
         * any specific precautions, e.g.
         * escaping
         */
        g.rule(SIMPLE_PUNCTUATOR).is(g.firstOf(
                Punctuators.ADD, Punctuators.ADD_EQUALS,
                Punctuators.AMPERSAND_EQUALS,
                Punctuators.AT_SIGN, Punctuators.BANG,
                Punctuators.BRACE_L, Punctuators.BRACKET_L,
                Punctuators.BRACKET_R, Punctuators.CLOBBER,
                Punctuators.COLON, Punctuators.COLON_ADD,
                Punctuators.COLON_EQUALS,
                Punctuators.COLON_QMARK,
                Punctuators.COLON_SUB, Punctuators.SLASH,
                Punctuators.DOUBLE_COMMA, Punctuators.COMMA,
                Punctuators.DECREMENT,
                Punctuators.DOUBLE_GREATER_DASH,
                Punctuators.DOUBLE_BRACKET_L,
                Punctuators.DOUBLE_BRACKET_R,
                Punctuators.DOUBLE_GREATER_EQUALS,
                Punctuators.DOUBLE_GT,
                Punctuators.DOUBLE_HASH,
                Punctuators.DOUBLE_LESS_DASH,
                Punctuators.DOUBLE_LESS_EQUALS,
                Punctuators.DOUBLE_LT,
                Punctuators.TRIPLE_LT,
                Punctuators.DOUBLE_PERCENTAGE,
                Punctuators.EQ, Punctuators.EQUALS,
                Punctuators.GE, Punctuators.HASH,
                Punctuators.INCREMENT, Punctuators.LE,
                Punctuators.LESS_AND,
                Punctuators.LESS_GREATER, Punctuators.MUL,
                Punctuators.MUL_EQUALS, Punctuators.NE,
                Punctuators.PERCENTAGE,
                Punctuators.PERCENTAGE_EQUALS,
                Punctuators.PIPE_AND,
                Punctuators.PIPE_EQUALS, Punctuators.POWER,
                Punctuators.DOUBLE_POWER,
                Punctuators.POWER_EQUALS, Punctuators.QMARK,
                Punctuators.SLASH_EQUALS, Punctuators.SUB,
                Punctuators.SUB_EQUALS, Punctuators.TILDE,
                Punctuators.DOUBLE_SEMI_AND,
                Punctuators.SEMICOLON_AND))
                .skipIfOneChild();

        /*
         * denotes all punctuators that can be
         * used in arguments in commands
         * including those that have been
         * escaped
         *
         */
        g.rule(PUNCTUATOR).is(//
                g.firstOf(//
                        ESCAPED_SPECIAL_CHARACTER,
                        SIMPLE_PUNCTUATOR
                        //
                )//
        ).skipIfOneChild();

        g.rule(ESCAPED_SPECIAL_CHARACTER_DQ).is(//
                g.firstOf(Literals.ESCAPED_WHITE_SPACE,
                        Punctuators.ESCAPED_ESCAPE,
                        Punctuators.ESCAPED_DOLLAR,
                        g.sequence(//
                                Punctuators.ESCAPE, //
                                g.firstOf(//
                                        Punctuators.DOUBLE_QUOTE, //
                                        Punctuators.BACKTICK//
                                )//
                        )//
                ));

        g.rule(BACKTICK_SUBSTITUTION).is(//
                g.zeroOrMore(Punctuators.ESCAPE),
                Punctuators.BACKTICK, //
                COMPLETE_COMMANDS, //
                g.zeroOrMore(Punctuators.ESCAPE),
                Punctuators.BACKTICK//
        );

        // Either characters that interrupt an
        // input or that loses its
        // meaning when escaped
        g.rule(ESCAPED_SPECIAL_CHARACTER).is(g.firstOf(
                Literals.ESCAPED_WHITE_SPACE,
                Punctuators.ESCAPED_ESCAPE,
                Punctuators.ESCAPED_DOLLAR,
                g.sequence(Punctuators.ESCAPE, g.firstOf(
                        Punctuators.PIPE, Punctuators.GT,
                        Punctuators.AMPERSAND,
                        Punctuators.AND,
                        Punctuators.SEMICOLON,
                        Punctuators.DOUBLE_SEMI,
                        Punctuators.PAREN_L,
                        Punctuators.PAREN_R,
                        Punctuators.SINGLE_QUOTE,
                        Punctuators.DOUBLE_QUOTE,
                        Punctuators.BACKTICK,
                        GenericTokenType.EOL,
                        Punctuators.LT,
                        Punctuators.DOUBLE_QUOTE,
                        Punctuators.TILDE, Punctuators.HASH,
                        Punctuators.BRACE_R))));

        g.rule(TILDE_EXPANSION).is(//
                g.sequence(Punctuators.TILDE,
                        g.sequence(NAME, g.zeroOrMore(
                                Punctuators.SLASH, NAME)))//
        );

        g.rule(DOLLAR_CONSTRUCT).is( //
                g.sequence( //
                        Punctuators.DOLLAR, //
                        g.firstOf( //
                                BRACED_PARAMETER, //
                                PARENTHESIZED_PARAMETER, //
                                SPECIAL_PARAMETER, //
                                Literals.DIGIT, //
                                NAME, //
                                //see ANSI-C-Quoting
                                SINGLE_QUOTED_WORD

                        )//
                )//
        );

        g.rule(PARENTHESIZED_PARAMETER).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.PAREN_L,
                                Punctuators.PAREN_L,
                                ARITHMETIC_ASSIGNMENT,
                                Punctuators.PAREN_R,
                                Punctuators.PAREN_R//
                        ), //
                        g.sequence(//
                                Punctuators.PAREN_L,
                                COMPLETE_COMMANDS,
                                Punctuators.PAREN_R //
                        ), //

                        g.sequence(Punctuators.PAREN_L,
                                DOLLAR_CONSTRUCT,
                                Punctuators.PAREN_R)//
                )//
        );

    }

    /**
     * contains the rules for evaluation of
     * test-sequences, using double- and
     * single brackets
     *
     * @param g grammar
     */
    public static void testSequenceGrammar(
            LexerfulGrammarBuilder g) {

        g.rule(TEST_SEQUENCE).is(//
                g.firstOf(//
                        DOUBLE_BRACKET_TEST, //
                        g.sequence(//
                                SINGLE_BRACKET_TEST, //
                                g.zeroOrMore(//
                                        g.optional(
                                                Literals.DELIMITER),
                                        g.firstOf(
                                                Punctuators.AND,
                                                Punctuators.OR),
                                        g.optional(
                                                Literals.DELIMITER),
                                        SINGLE_BRACKET_TEST
                                )
                        )
                )
        ).skipIfOneChild();

        g.rule(DOUBLE_BRACKET_TEST).is(//
                Punctuators.DOUBLE_BRACKET_L,
                Literals.DELIMITER,
                DOUBLE_BRACKET_OR_EXPRESSION,
                Literals.DELIMITER, //
                Punctuators.DOUBLE_BRACKET_R);

        g.rule(SINGLE_BRACKET_TEST).is(//
                Punctuators.BRACKET_L, Literals.DELIMITER,
                SINGLE_BRACKET_OR_EXPRESSION,
                Literals.DELIMITER, Punctuators.BRACKET_R)
                .skipIfOneChild();

        g.rule(DOUBLE_BRACKET_OR_EXPRESSION).is(
                DOUBLE_BRACKET_AND_EXPRESSION,
                g.zeroOrMore(Literals.DELIMITER,
                        Punctuators.OR, Literals.DELIMITER,
                        DOUBLE_BRACKET_AND_EXPRESSION))
                .skipIfOneChild();

        g.rule(SINGLE_BRACKET_OR_EXPRESSION).is(
                SINGLE_BRACKET_AND_EXPRESSION,
                g.zeroOrMore(Literals.DELIMITER,
                        g.sequence(Punctuators.SUB, "o"),
                        Literals.DELIMITER,
                        SINGLE_BRACKET_AND_EXPRESSION))
                .skipIfOneChild();

        g.rule(DOUBLE_BRACKET_AND_EXPRESSION).is(
                DOUBLE_BRACKET_NEGATED_EXPRESSION,
                g.zeroOrMore(Literals.DELIMITER,
                        Punctuators.AND, Literals.DELIMITER,
                        DOUBLE_BRACKET_NEGATED_EXPRESSION))
                .skipIfOneChild();

        g.rule(SINGLE_BRACKET_AND_EXPRESSION).is(
                SINGLE_BRACKET_NEGATED_EXPRESSION,
                g.zeroOrMore(Literals.DELIMITER,
                        g.sequence(Punctuators.SUB, "a"),
                        Literals.DELIMITER,
                        SINGLE_BRACKET_NEGATED_EXPRESSION))
                .skipIfOneChild();

        g.rule(DOUBLE_BRACKET_NEGATED_EXPRESSION).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.BANG, //
                                Literals.DELIMITER,
                                DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION//
                        ), //
                        DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION //
                )//
        ).skipIfOneChild();

        g.rule(SINGLE_BRACKET_NEGATED_EXPRESSION).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.BANG, //
                                Literals.DELIMITER,
                                SINGLE_BRACKET_PARENTHESIZED_EXPRESSION//
                        ), //
                        SINGLE_BRACKET_PARENTHESIZED_EXPRESSION //
                )//
        ).skipIfOneChild();

        g.rule(DOUBLE_BRACKET_PARENTHESIZED_EXPRESSION)
                .is(g.firstOf(//
                        g.sequence(//
                                Punctuators.PAREN_L, //
                                Literals.DELIMITER,
                                DOUBLE_BRACKET_OR_EXPRESSION,
                                Literals.DELIMITER, //
                                Punctuators.PAREN_R//
                        ), //
                        g.sequence(//
                                Punctuators.PAREN_L, //
                                Literals.DELIMITER,
                                g.firstOf(
                                        DOUBLE_BRACKET_BINARY_OPERATION,
                                        UNARY_TEST_OPERATION),
                                Literals.DELIMITER, //
                                Punctuators.PAREN_R//
                        ), //
                        DOUBLE_BRACKET_BINARY_OPERATION,
                        UNARY_TEST_OPERATION //
                        )//
                ).skipIfOneChild();

        g.rule(SINGLE_BRACKET_PARENTHESIZED_EXPRESSION).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.PAREN_L, //
                                Literals.DELIMITER,
                                SINGLE_BRACKET_OR_EXPRESSION,
                                Literals.DELIMITER, //
                                Punctuators.PAREN_R//
                        ), //
                        g.sequence(//
                                Punctuators.PAREN_L, //
                                Literals.DELIMITER,
                                g.firstOf(
                                        SINGLE_BRACKET_BINARY_OPERATION,
                                        UNARY_TEST_OPERATION),
                                Literals.DELIMITER, //
                                Punctuators.PAREN_R//
                        ), //
                        SINGLE_BRACKET_BINARY_OPERATION,
                        UNARY_TEST_OPERATION //
                )//
        ).skipIfOneChild();

        g.rule(DOUBLE_BRACKET_BINARY_OPERATION).is(//
                g.firstOf(g.sequence(COMPOSITE_WORD,
                        Literals.DELIMITER, //
                        Punctuators.EQUALS_TILDE, //
                        Literals.DELIMITER, PATTERN),
                        g.sequence(COMPOSITE_WORD,
                                Literals.DELIMITER, //
                                DOUBLE_BRACKET_BINARY_TEST_OPERATOR, //
                                Literals.DELIMITER,
                                COMPOSITE_WORD)));

        g.rule(SINGLE_BRACKET_BINARY_OPERATION).is(//
                COMPOSITE_WORD, Literals.DELIMITER, //
                SINGLE_BRACKET_BINARY_TEST_OPERATOR, //
                Literals.DELIMITER, COMPOSITE_WORD);

        g.rule(UNARY_TEST_OPERATION)
                .is(g.optional(UNARY_TEST_OPERATOR,
                        Literals.DELIMITER),
                        COMPOSITE_WORD);

        g.rule(DOUBLE_BRACKET_BINARY_TEST_OPERATOR).is(//
                g.firstOf(//
                        Punctuators.GT, Punctuators.LT,
                        Punctuators.GE, //
                        Punctuators.LE, //
                        SINGLE_BRACKET_BINARY_TEST_OPERATOR));

        g.rule(SINGLE_BRACKET_BINARY_TEST_OPERATOR).is(//
                g.firstOf(//
                        Punctuators.EQ, Punctuators.EQUALS,
                        Punctuators.NE,
                        g.sequence(Punctuators.SUB, "ne"),
                        g.sequence(Punctuators.SUB, "eq"),
                        g.sequence(Punctuators.SUB, "gt"),
                        g.sequence(Punctuators.SUB, "ge"),
                        g.sequence(Punctuators.SUB, "lt"),
                        g.sequence(Punctuators.SUB, "le")));

        g.rule(UNARY_TEST_OPERATOR).is(// also applies for
                // double_brackets
                g.firstOf(//
                        g.sequence(Punctuators.SUB, "a"),
                        g.sequence(Punctuators.SUB, "b"),
                        g.sequence(Punctuators.SUB, "c"),
                        g.sequence(Punctuators.SUB, "d"),
                        g.sequence(Punctuators.SUB, "e"),
                        g.sequence(Punctuators.SUB, "f"),
                        g.sequence(Punctuators.SUB, "g"),
                        g.sequence(Punctuators.SUB, "h"),
                        g.sequence(Punctuators.SUB, "L"),
                        g.sequence(Punctuators.SUB, "n"),
                        g.sequence(Punctuators.SUB, "p"),
                        g.sequence(Punctuators.SUB, "r"),
                        g.sequence(Punctuators.SUB, "S"),
                        g.sequence(Punctuators.SUB, "s"),
                        g.sequence(Punctuators.SUB, "t"),
                        g.sequence(Punctuators.SUB, "u"),
                        g.sequence(Punctuators.SUB, "w"),
                        g.sequence(Punctuators.SUB, "x"),
                        g.sequence(Punctuators.SUB, "z")));

    }

    /**
     * Contains the main structure of the
     * to-be-generated ast. Also represents
     * the entry point for the whole
     * to-be-parsed program.
     *
     * @param g the the grammar-builder
     *          that is being instantiated
     *          in ShellGrammar.create()
     */
    public static void basicStructureGrammar(
            LexerfulGrammarBuilder g) {

        g.rule(PROGRAM).is(//
                g.optional(LINEBREAK), //
                g.optional(SHEBANG, NEWLINE_LIST),
                g.firstOf(//
                        COMPLETE_COMMANDS, //
                        LINEBREAK//
                ), //
                g.optional(LINEBREAK) //
        );

        g.rule(SHEBANG).is(
                HASH, BANG, PATH, g.optional(Literals.DELIMITER, NAME)
        );

        g.rule(COMPLETE_COMMANDS).is(//
                COMPLETE_COMMAND, //
                g.zeroOrMore(NEWLINE_LIST, COMPLETE_COMMAND)//
        );

        g.rule(COMPLETE_COMMAND).is(//
                g.sequence(LIST, g.optional(SEPARATOR_OP)) //
        ).skipIfOneChild();

        g.rule(LIST).is(//
                AND_OR, //
                g.zeroOrMore(SEPARATOR_OP, AND_OR)//
        ).skipIfOneChild();

        g.rule(AND_OR).is(//
                PIPELINE, //
                g.zeroOrMore(//
                        g.firstOf(Punctuators.OR,
                                Punctuators.AND),
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE), //
                        PIPELINE//
                )//
        ).skipIfOneChild();

        g.rule(PIPELINE).is(//
                g.optional(Punctuators.BANG,
                        DELIMITER_OR_WHITE_SPACE),
                PIPE_SEQUENCE//
        ).skipIfOneChild();

        g.rule(PIPE_SEQUENCE).is(//
                COMMAND, //
                g.zeroOrMore(
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        Punctuators.PIPE,
                        g.optional(
                                DELIMITER_OR_WHITE_SPACE),
                        COMMAND)//
        ).skipIfOneChild();

        g.rule(LINEBREAK)
                .is(g.optional(Literals.DELIMITER),
                        g.optional(NEWLINE_LIST))
                .skipIfOneChild();

        g.rule(NEWLINE_LIST)
                .is(g.oneOrMore(GenericTokenType.EOL,
                        g.optional(Literals.DELIMITER)))
                .skipIfOneChild();

        g.rule(SEPARATOR_OP).is(//
                g.firstOf(//
                        Punctuators.SEMICOLON, //
                        Punctuators.AMPERSAND//
                ), g.optional(Literals.DELIMITER)//
        ).skipIfOneChild();

        g.rule(SEPARATOR).is(//
                g.firstOf(//
                        g.sequence(//
                                SEPARATOR_OP,
                                g.optional(
                                        Literals.DELIMITER),
                                g.optional(
                                        GenericTokenType.EOL)//
                        ), //
                        NEWLINE_LIST //
                )//
        ).skipIfOneChild();

        g.rule(SEQUENTIAL_SEP).is(//
                g.firstOf(//
                        g.sequence(//
                                Punctuators.SEMICOLON,
                                LINEBREAK//
                        ), //
                        NEWLINE_LIST//
                ), g.optional(Literals.DELIMITER)//
        );

    }

    /**
     * creates the grammar that is to be
     * used by the parser.
     *
     * @return Shell-compliant grammar.
     */
    public static Grammar create() {
        LexerfulGrammarBuilder g = LexerfulGrammarBuilder
                .create();

        ShellGrammar.arithmeticExpressionGrammar(g);

        ShellGrammar.basicStructureGrammar(g);

        ShellGrammar.commandsGrammar(g);

        ShellGrammar.wordExpansionGrammar(g);

        ShellGrammar.testSequenceGrammar(g);

        g.setRootRule(ShellGrammar.PROGRAM);

        return g.build();
    }

}
