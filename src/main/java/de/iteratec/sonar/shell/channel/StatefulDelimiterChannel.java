package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.impl.Lexer;
import com.sonar.sslr.impl.LexerException;

import de.iteratec.sonar.shell.ast.LexerState;
import de.iteratec.sonar.shell.ast.ShellLexer;

import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * concerns itself with the gaps between characters. if we are in a double-quote-context, they
 * get turned into a Literals.WHITE_SPACE. Else, Literals.DELIMITER. This is important, because
 * the WHITE_SPACES need to be retained in the double quoted string, while DELIMITERs do not get included
 * into the final AST and are simply required to separate complete tokens from one another.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class StatefulDelimiterChannel extends Channel<Lexer> {

	private final StringBuilder tmpBuilder = new StringBuilder();
	private final Matcher matcher;
	private final String regexp;
	private final Token.Builder tokenBuilder = Token.builder();
	private final LexerState ls;

	public StatefulDelimiterChannel(String regexp, LexerState ls) {

		matcher = Pattern.compile(regexp).matcher("");
		this.regexp = regexp;
		this.ls = ls;

	}

	@Override
	public boolean consume(CodeReader code, Lexer lexer) {
		try {
			if (code.popTo(matcher, tmpBuilder) > 0) {
				if (ls.isInDoubleQuotes()) {
					String value = tmpBuilder.toString();

					Token token = tokenBuilder.setType(ShellLexer.Literals.WHITE_SPACE)
							.setValueAndOriginalValue(value).setURI(lexer.getURI())
							.setLine(code.getPreviousCursor().getLine())
							.setColumn(code.getPreviousCursor().getColumn())
							.build();

					lexer.addToken(token);
					tmpBuilder.delete(0, tmpBuilder.length());
					return true;

				} else {
					String value = tmpBuilder.toString();

					Token token = tokenBuilder.setType(ShellLexer.Literals.DELIMITER)
							.setValueAndOriginalValue(value).setURI(lexer.getURI())
							.setLine(code.getPreviousCursor().getLine())
							.setColumn(code.getPreviousCursor().getColumn())
							.build();

					lexer.addToken(token);
					tmpBuilder.delete(0, tmpBuilder.length());
					return true;
				}
			} else {
				return false;
			}
		} catch (StackOverflowError e) {
			throw new LexerException("The regular expression " + regexp
					+ " has led to a stack overflow error. "
					+ "This error is certainly due to an inefficient use of alternations. See http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=5050507",
					e);
		}
	}
}

