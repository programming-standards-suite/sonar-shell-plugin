#!/bin/bash

echo "Validating if mandatory shell script environment variables are set..."

if [ -z "$vendoDashUrl" -o "$vendoDashUrl" = "" -o "$vendoDashUrl" = "null" ];
then
    echo "===   Error: Variable vendoDashUrl is empty or null   ===";
    exit 1;
else
    echo "vendoDashUrl is "$vendoDashUrl
fi

if [ -z "$pipeline" -o "$pipeline" = "" -o "$pipeline" = "null" ];
then
    echo "===   Error: Variable pipeline is empty or null   ===";
    exit 1;
else
    echo "pipeline is "$pipeline
fi

if [ -z "$team" -o "$team" = "" -o "$team" = "null" ]
then
    echo "===   Error: Variable team is empty or null   ===";
    exit 1;
else
    echo "team is "$team;
fi

if [ -z "$buildId" -o "$buildId" = "" -o "$buildId" = "null" ]
then
    echo "===   Error: Variable buildId is empty or null   ===";
    exit 1;
else
    echo "buildId is "$buildId;
fi

if [ -z "$result" -o "$result" = "" -o "$result" = "null" ]
then
    echo "===   Error: Variable result is empty or null   ===";
    exit 1;
else
    echo "result is "$result;
fi

if [ -z "$stage" -o "$stage" = "" -o "$stage" = "null" ]
then
    echo "===   Error: Variable stage is empty or null   ===";
    exit 1;
else
    echo "stage is "$stage;
fi

if [ -z "$testType" -o "$testType" = "" -o "$testType" = "null" ]
then
    echo "===   Error: Variable testType is empty or null   ===";
    exit 1;
else
    echo "testType is "$testType;
fi

if [ -z "$cucumberJsonFile" -o "$cucumberJsonFile" = "" -o "$cucumberJsonFile" = "null" ]
then
    echo "===   Error: Variable cucumberJsonFile is empty or null   ===";
    exit 1;
else
    echo "cucumberJsonFile is "$cucumberJsonFile;
fi

echo "Printing optional shell script environment variables..."

echo "beforeInfo is "$beforeInfo
echo "afterInfo is "$afterInfo
echo "gitCommit is "$gitCommit
echo "gitCommitDate is "$gitCommitDate
echo "gitCommitter is "$gitCommitter


echo "If exists deleting myjson.json..."
[ -e myjson.json ] && rm myjson.json

echo "Printing metadata to myjson.json..."
echo -n "{ \"afterInfo\": \"$afterInfo\", \"beforeInfo\": \"$beforeInfo\", \"buildId\": \"$buildId\", \"gitCommit\": \"$gitCommit\", \"gitCommitDate\": \"$gitCommitDate\", \"gitCommitter\": \"$gitCommitter\", \"pipeline\": \"$pipeline\", \"result\": \"$result\", \"stage\": \"$stage\", \"team\": \"$team\", \"testType\": \"$testType\", \"cucumberJsonString\":" >> myjson.json

echo "Print report-json to myjson.json..."
cat "$cucumberJsonFile" >> myjson.json

echo "Printing finishing brace to myjson.json..."
echo -n "}" >> myjson.json

echo "Constructing full url to VendoDash reports endpoint..."
if [[ $vendoDashUrl =~ [/\\]$ ]] # Checks if vendoDashUrl ends with /
then
    fullUrl="$vendoDashUrl"reports/json
else
    fullUrl="$vendoDashUrl"/reports/json
fi
echo "Full url to VendoDash reports endpoint is $fullUrl"

echo "Sending report to VendoDash..."
curl -X POST "$fullUrl" -H "accept: */*" -H "Content-Type: application/json" -d @myjson.json