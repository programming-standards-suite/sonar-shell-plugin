package de.iteratec.sonar.shell.util;

import com.sonar.sslr.api.AstNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * runs all visitor-classes through a
 * specific piece of code or tree for
 * now, only utilized in
 * ShellVisitorRunnerTest.
 */
public class ShellVisitorRunner {

    private ShellVisitorRunner() {
    }

    static List<PreciseIssue> issues = new ArrayList<>();

    /**
     * runs all passed checks and generates issues
     *
     * @param node     the node from which the checks shall start
     * @param visitors the passed checks that shall be used
     * @return true if no visitor finds anything
     */
    public static boolean scanTree(AstNode node,
                                   ShellVisitorInterface... visitors) {

        return Arrays.stream(visitors).allMatch(visitor ->
                visitor.visitNode(node)
        );

    }

    /**
     * runs all passed checks and generates issues
     *
     * @param node     the node from which the checks shall start
     * @param visitors the passed checks that shall be used
     * @return a list of issues
     */
    public static List<PreciseIssue> scanTreeAndRaiseIssues(AstNode node, ShellVisitorInterface... visitors) {
        for (ShellVisitorInterface visitor : visitors) {
            issues.addAll(visitor.visitNodeAndCollectIssues(node));
        }
        return issues;
    }


    public static void resetIssues() {
        issues = new ArrayList<>();
    }
}
