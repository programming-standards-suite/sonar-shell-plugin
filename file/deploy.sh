#!/bin/bash
#it is necessarry to have the following environment variables:  OC_TOKEN and OC_URL and git_personal_access_token
# uninstall command only works if all OC objects are tagged with app=domain-service-sitzplatz (that is stored in $service)
# variables used from service.properties: PROJECT_SHORTCUT, LABEL_APP
#assure that the service variable is maintained correctly
set -e -x
echo "deploy.sh: $(pwd)"

##########################################################
# Internal Variables
##########################################################

#a list of temporary files that should be deleted after successful deployment; seperated by a space
cleanup_list=""


##########################################################
# Helper functions
##########################################################
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
	 c=${string:$pos:1}
	 case "$c" in
		[-_.~a-zA-Z0-9] ) o="${c}" ;;
		* )               printf -v o '%%%02x' "'$c"
	 esac
	 encoded+="${o}"
  done
  echo "${encoded}"
}


oc_login(){
    allValid=1
    # Retrieve OC_URL from temp params within the  - if it was not set within the bash script environment
    if [ "${OC_URL_TEMP_PARAM:-}" ]; then OC_URL=$OC_URL_TEMP_PARAM;fi
    if [ "${OC_PROJECT_TEMP_PARAM:-}" ]; then OC_PROJECT=$OC_PROJECT_TEMP_PARAM;fi
    if [ "${OC_URL:-}" ]; then echo "OC_URL exists"; else echo "OC_URL does not exist";allValid=0;fi
    if [ "${OC_TOKEN:-}" ]; then echo "OC_TOKEN exists"; else echo "OC_TOKEN does not exist";allValid=0;fi

    if [ "$allValid" -eq 0 ]
    then
        exit 1
    fi

    oc login --token=$OC_TOKEN --insecure-skip-tls-verify=true $OC_URL && oc project $OC_PROJECT

}

# Method for verifying the success of the deployment. All deploymentconfigs within the template are checked. If one of them fails, the method returns error (1)
oc_verify_deployment(){
    local DEPLOYMENT=$1
    local NUMBER_OF_REPLICAS=$(($(oc get $DEPLOYMENT -o jsonpath={.spec.replicas}) + 3))
    local LOOP=0
    # Number of tries is depending on the number of replicas -> scaling
    while [ $LOOP -lt $NUMBER_OF_REPLICAS ]
    do
        # each status query last about 1 minute. After inutes not ready status -> break deployment.
        oc rollout status -w $DEPLOYMENT && echo "successfull verification of $DEPLOYMENT rollout status" && return 0
        echo "$LOOP try verifying rollout of $DEPLOYMENT ...."
        LOOP=$((LOOP+1))
    done
    #### Uncomment
    # echo "Verification failed for $DEPLOYMENT ! Undo rollout is starting... "
    # oc rollout undo $DEPLOYMENT
    # echo "Undo Rollout $DEPLOYMENT"

    return 1
}

# Method for rolling back the deployment process (all deployment of all deployment configs in the template are rolled back)
oc_rollback_deployment(){
    for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep "deploymentconfig");
    do
      echo "Rolling back $curr_deployment!"
      oc rollout undo $curr_deployment
    done;
    echo "Rolling back finished!"
    return 1
}

#method to create a secret for openshift that allows the pull of a docker from a repository
oc_secret(){
     allValid=1
    #REPO_USER and REPO_PASSWORD has to be passed via environment parameter
    if [ "${REPO_USER:-}" ]; then echo "REPO_USER exists"; else echo "REPO_USER does not exist";allValid=0;fi
    if [ "${REPO_PASSWORD:-}" ]; then echo "REPO_PASSWORD exists"; else echo "REPO_PASSWORD does not exist";allValid=0;fi
   # Retrieve Parameters from temp params within the environment properties - if it was not set within the bash script environment
    if [ "${SOURCE_DOCKER_REPO_TEMP_PARAM:-}" ]; then SOURCE_DOCKER_REPO=$SOURCE_DOCKER_REPO_TEMP_PARAM;else echo "SOURCE_DOCKER_REPO_TEMP_PARAM does not exist";allValid=0;fi
    if [ "${SOURCE_ARTIFACTORY_REPO_TEMP_PARAM:-}" ]; then SOURCE_ARTIFACTORY_REPO=$SOURCE_ARTIFACTORY_REPO_TEMP_PARAM;else echo "SOURCE_ARTIFACTORY_REPO_TEMP_PARAM does not exist";allValid=0;fi
    if [ "${DOCKER_EMAIL_TEMP_PARAM:-}" ]; then DOCKER_EMAIL=$DOCKER_EMAIL_TEMP_PARAM;else echo "DOCKER_EMAIL_TEMP_PARAM does not exist";allValid=0;fi

    if [ "$allValid" -eq 0 ]
    then
        exit 1
    fi

    #create the secret
    oc secrets new-dockercfg ${PROJECT_SHORTCUT}-docker-secret --docker-server=${SOURCE_DOCKER_REPO}.${SOURCE_ARTIFACTORY_REPO} \
    --docker-username=${REPO_USER} --docker-password=${REPO_PASSWORD} \
    --docker-email=${DOCKER_EMAIL}

    #assign the dockercfg to the variable
    export DOCKERCFG="$(oc get secret ${PROJECT_SHORTCUT}-docker-secret -o json | grep \".dockercfg\" | awk '{print $2}')"

    #delete secret
    oc delete secret ${PROJECT_SHORTCUT}-docker-secret





}



##########################################################
# Processing functions
##########################################################
setup_script_environment(){
    #check if necessarry environment variables are set and not empty
    allValid=1
    if [ "${git_personal_access_token:-}" ]
    then
        echo "git_personal_access_token exists"
    else
        echo "git_personal_access_token does not exist"
        allValid=0
    fi

    if [ "$allValid" -eq 0 ]
    then
        exit 1
    fi

    SCRIPT_DIR=$(cd $(dirname "$0") && pwd)
    pushd ${SCRIPT_DIR}
}

apply_deployment_properties_service(){
    #load service.properties
    service_file=service.properties
    . $SCRIPT_DIR/$service_file
}

load_environment_specific_deployment_parameters(){
    # get environment properties from git (download raw link via gitlab api):
    environment_file=env_${environment}_${service}.properties
    env_filepath="$environment_file"

    #url encode filepath
        env_filepath_urlencode=$(rawurlencode $env_filepath)
        #due to a bug in gitlab api
        env_filepath_fixed=${env_filepath_urlencode//./%2E}
    #GitLab API
    #GET /projects/:id/repository/files/:file_path/raw?ref=master
    #:file_path has to be url encoded
    env_url="$git_api_url/projects/$git_project_id/repository/files/$env_filepath_fixed/raw?ref=$git_version_tag"
    echo "using $env_filepath in version $git_version_tag, curl for getting URL encoded path for environment properties: curl -H '"'PRIVATE-TOKEN xxx'"' $env_url"
    echo "$env_url"
    curl -o  $environment_file --request GET --header "PRIVATE-TOKEN: $git_personal_access_token" "$env_url"

    #add to list that gets deleted when deployment is successful
    cleanup_list+=" $environment_file"


    #replace variables inside $environment_file in sub bash
   ( set -a; source $environment_file;   set +a;   cat $environment_file | while read line; do echo $(eval echo `echo $line`) ; done) > ${environment_file}.tmp
    cp ${environment_file}.tmp ${environment_file}

    #add to list that gets deleted when deployment is successful
    cleanup_list+=" ${environment_file}.tmp"



    # Execute environment property assignment once in order to retrieve "_TEMP_PARAM" assignments which provide
    # a possibility to inject environment specific information like the OC_URL into the processing logic of this script
    # Note:
    # This does not mean necessarily that this information has to be part of the environment parameter file,
    # it can optionally be set as environment variable before
    . $environment_file
}

#
# TODO TCK Diese Methode wird ben�tigt um auf DBB zuzugreifen, was allerdings 
#          im aktuellen Deployment-Skript nur hartcodiert f�r die Umgebung TEST vorliegt. 
#          Daher auskommentiert.
# 
# load_public_deployment_parameters(){
#    #get DBB public properties from git (download raw link via gitlab api):
#    dbb_public=env_Test_DEV_public.properties
#    dbb_filepath="$dbb_public"
#    #url encode filepath
#
#        dbb_filepath_urlencode=$(rawurlencode $dbb_filepath)
#        #due to a bug in gitlab api
#        dbb_filepath_fixed=${dbb_filepath_urlencode//./%2E}
#    #curl -o $dbb_public https://git.tech.rz.db.de/vendo/blueprint/config_repo/vendo_config/raw/master/dbb/$dbb_public
#    #GET /projects/:id/repository/files/:file_path/raw?ref=master
#    #:file_path has to be url encoded
#    public_url="$git_api_url/projects/$git_project_id_public/repository/files/$dbb_filepath_fixed/raw?ref=master"
#    echo "curl with URL encoded path for public properties: curl -H '"'PRIVATE-TOKEN xxx'"' $public_url"
#    echo "$public_url"
#    curl -o  $dbb_public --request GET --header "PRIVATE-TOKEN: $git_personal_access_token" "$public_url"
#
#    #add to list that gets deleted when deployment is successful
#    cleanup_list+=" $dbb_public"
#}


merge_effective_properties(){
    # create effective properties - remove any "/.*_TEMP_PARAM=.*/" parameters before, because variables which
    # have the purpose to be executed within the script only must not appear in
    # the effective properties (the oc process command will otherwise throw an error because it treats them
    # as "unknown parameters" in Openshift version 3.5. On top those might contain sensitive login information
    # and should not be visible within the target environment)
    awk -F= '/.*_TEMP_PARAM=.*/ {next} !a[$1]++' $environment_file $service_file > $effective_file

    #append public dbb properties
    echo "#public properties" >> $effective_file
    
	# TODO TCK Auskommentiert weil (siehe load_public_deployment_parameters)
	# echo "#$dbb_public" >> $effective_file
    # cat $dbb_public >> $effective_file

    #append configuration version
    echo "#configuration version" >> $effective_file
    echo "CONFIG_VERSION=$git_version_tag" >> $effective_file

    #replace version for service in effective properties
    sed -i "/SERVICE_VERSION=/ s/=.*/=$SERVICE_VERSION/" $effective_file

    #replace label app for uninstall to add project shortcut
    sed -i "/LABEL_APP=/ s/=.*/=$LABEL_APP/" $effective_file

    #replace PROJECT_SHORTCUT with updated PROJECT_SHORTCUT
    sed -i "/PROJECT_SHORTCUT=/ s/=.*/=$PROJECT_SHORTCUT/" $effective_file

    #create DOCKERCFG with updated DOCKERCFG
    echo "#Artifactory Secret for Docker pull" >> $effective_file
    echo "DOCKERCFG=$DOCKERCFG" >> $effective_file



    #add to list that gets deleted when deployment is successful
    cleanup_list+=" $effective_file"
}

setup_static_parameters(){
    #service
    service=${PROJECT_SHORTCUT}-${PROJECT_SERVICENAME}

    #Konfiguration Repository parameters
    #gitlab project id can be found in gitlab on project settings
    git_project_id=1550
	
	# Konfiguration Repository von dbb (f�r Data-Lake-Zugriff ben�tigt)
	git_project_id_public=1106
    #url of the gitlab api
    git_api_url=https://git.tech.rz.db.de/api/v4

    #if Branchname is not set it is assumed that master is used
    if "${BRANCH_NAME:-}"; then echo "Branch Name is $BRANCH_NAME"; else BRANCH_NAME=master;fi
    #KURZ_BRANCH is used to allow the deployment of multiple Branches into one single OC Namespace
    if $BRANCH_NAME == "master" || $BRANCH_NAME == "develop"
        then
            KURZ_BRANCH=""
        else
            #length 25
            KURZ_BRANCH=$(echo ${BRANCH_NAME:-02})
            #convert all to lowercase
            KURZ_BRANCH=$(echo "${KURZ_BRANCH:-02}")
            #extend PROJECT_SHORTCUT with KURZ_BRANCH
            PROJECT_SHORTCUT=$PROJECT_SHORTCUT-$KURZ_BRANCH
    fi
    echo "KURZ_BRANCH=$KURZ_BRANCH"
    echo "PROJECT_SHORTCUT=$PROJECT_SHORTCUT"

    #change label for uninstall
    LABEL_APP=$PROJECT_SHORTCUT-$LABEL_APP
    echo "LABEL_APP=$LABEL_APP"

    #effective properties filename
    timestamp=$(date +"%Y.%m.%d-%H.%M.%S")
    effective_name=_effective.properties
    effective_file=$timestamp$effective_name

    echo $timestamp$effective_name
    echo "working dir"
    echo `pwd`
}

##########################################################
# General setup
##########################################################
setup_script_environment
apply_deployment_properties_service
setup_static_parameters

##########################################################
# Process commands
##########################################################
if "$#" -eq 4 && $1 == "install"
then
	environment=$2
	#tag of the version
	git_version_tag=$3
	#version of dockerimage of the service
	SERVICE_VERSION=$4
	if $1 == "install"
	then
		deployment_template_yaml=deployment-template.yaml
		deployconfig_effective=deployment-config.yaml



        load_environment_specific_deployment_parameters
		# TODO TCK: Auskommentiert (siehe Kommentar der Funktion)
        # load_public_deployment_parameters
        oc_login
        oc_secret

        merge_effective_properties

        echo "install on $environment in $OC_URL in project $OC_PROJECT "
        echo "input: $environment_file, $service_file"

		#only generate deployment config
		oc process -f $deployment_template_yaml --param-file=$effective_file > $deployconfig_effective
		#oc process -f $deployment_template_yaml --param-file=$effective_file --ignore-unknown-parameters> $deployconfig_effective

		#add to list that gets deleted when deployment is successful
        cleanup_list+=" $deployconfig_effective"

		#execute template and create new would be:
		#oc process -f $deployment_template_yaml --param-file=$effective_file | oc create -f -

		#Create or reconfigure deployment
		oc apply -f $deployconfig_effective 
	
		# cancel rollout before redeployment - this is a best effort operation and might not take effect immediately 
		(oc rollout cancel $(oc get -f deployment-config.yaml -o name | grep deploymentconfig) && sleep 20) || true 
	
		#rollout all existing deployment configs
		for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep "deploymentconfig"); 
		do
		  echo "Rollout of $curr_deployment" 
		  oc rollout latest $curr_deployment;
		done;
		echo "Initial Rollout Done!"

		#check success of rollout
		for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep "deploymentconfig");  
		do
		  echo "Verify Rollout $curr_deployment!"
		  oc_verify_deployment $curr_deployment || oc_rollback_deployment
		done;  

        #cleanup after successful deployment
        rm ${cleanup_list}

		#only execute template, for debug only
		#oc process -f $deployment_template_yaml --param-file=$effective_file
	
	fi
elif "$#" -eq 3  &&   $1 == "uninstall" 
	then
		environment=$2
		git_version_tag=$3
        load_environment_specific_deployment_parameters
		echo "uninstall app=$LABEL_APP"
		oc_login
		oc delete all,pvc,secret,sa -l app=$LABEL_APP
else
	echo "Usage deploy.sh install <environment> <Configuration version> <Service version>"
	echo "sample test: ./deploy.sh install Test 0.3.20170919_114831_060a500_20170919_153221 0.3.20170919_114831_060a500_20170919_153221"
	echo "sample int: ./deploy.sh install Integration v1.2 0.3.20170913_162512_39315e8_20170914_093353"
	echo "Usage deploy.sh uninstall <environment> <Configuration version>"
	echo "sample uninstall: ./deploy.sh uninstall Integration 0.3.20170919_114831_060a500_20170919_153221"
	echo "commands [install,uninstall]"
	echo "environments [Test,Integration,Presentation]"
	echo "project : An existing oc project"
	exit 1
fi  


exit 0