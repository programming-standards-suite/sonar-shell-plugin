/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.AstNodeType;
import de.iteratec.sonar.shell.ast.ShellGrammar;
import de.iteratec.sonar.shell.ast.ShellLexer;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Verifies that function declaration
 * does not use a FUNCTION-keyword and
 * that function nanme is camel case.
 *
 * @author <a href=
 * "mailto:Navid.Noorshams@iteratec.de">Navid
 * Noorshams</a>
 */
@Rule(key = "SH003",
        name = "Function keyword used",
        description = "Function keyword has been employed",
        priority = Priority.CRITICAL,
        tags = {"stupid"})
public class FunctionDeclarationCheck extends AbstractCheckTester {

    List<PreciseIssue> issues = new ArrayList();

    String functionKeywordMessage = "You declared a function using the function keyword - how could you?";

    public List<AstNodeType> getAstNodeTypesToVisit() {
        // TODO inspired from sonar-python, what
        // do I use this for?
        return Collections.singletonList(
                ShellGrammar.FUNCTION_DEFINITION);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.sonar.sslr.api.AstVisitor#
     * visitNode(com.sonar.sslr.api.AstNode)
     */
    @Override
    public boolean visitNode(AstNode ast) {
        List<AstNode> functions = AstUtils.getAllOccurencesOf("FUNCTION_DEFINITION", ast);
        return checkNoFunctionKeyword(functions);
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        List<AstNode> functions = AstUtils.getAllOccurencesOf("FUNCTION_DEFINITION", node);
        checkNoFunctionKeyword(functions);
        return issues;
    }

    private boolean checkNoFunctionKeyword(List<AstNode> functions) {
        for (AstNode function : functions) {
            List<AstNode> functionComponents = function.getChildren();

            if (functionComponents.stream().anyMatch(
                    child -> child.getToken().getType().equals(ShellLexer.Keywords.FUNCTION
                    ))) {
                PreciseIssue issue = new PreciseIssue(
                        IssueLocation.tokenLocation(function.getFirstChild(ShellLexer.Keywords.FUNCTION), functionKeywordMessage
                        )
                );
                issues.add(issue);
                return false;
            }
        }
        return true;
    }

    @DisplayName("error marking generated at the right position")
    @Test
    public void t1() {
        String code = "function scramble_word(){\n" +
                "  # pick a word randomly from the wordlib, and scramble it\n" +
                "  # Original word is 'match' and scrambled word is 'scrambled'\n" +
                "\n" +
                "  match=\"$($randomquote $wordlib)\"\n" +
                "\n" +
                "  echo \"Picked out a word!\"\n" +
                "\n" +
                "  len=$(echo $match | wc -c | sed 's/[^[:digit:]]//g')\n" +
                "  scrambled=\"\"; lastval=1\n" +
                "}\n";
        assertThatCheckFails(code);
        assertThatErrorHighlightingIsAt(code, 1, 1);
        assertThatErrorHighlightingIsAt(code, 1, 1, 0, 8);
    }


}
