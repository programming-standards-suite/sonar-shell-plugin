/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the 3-clause BSD license.
 * See file LICENSE for details.
 */

package de.iteratec.sonar.shell.sslr;

import com.google.gson.JsonSyntaxException;
import com.sonar.sslr.api.AstNode;
import com.sonar.sslr.api.Token;
import com.sonar.sslr.impl.Lexer;

import de.iteratec.sonar.shell.ast.ShellLexer;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.ShellDialect;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Abstract base class for test suites on Parsers.
 *
 * @author <a href="mailto:Gerd.Neugebauer@iteratec.de">Gerd Neugebauer</a>
 */
public abstract class AbstractParserTester {

	/**
	 * The field <code>parser</code> contains the object under test. It is
	 * recreated for each test case.
	 */
	protected static ShellParser ShellParser;

	/**
	 * The method <code>assertOk</code> parses the parameter string and asserts
	 * that this parsing succeeds.
	 *
	 * @param s the shell input
	 */
	protected AstNode assertOk(String s) {
		AstNode tree = ShellParser.parse(s);
		System.out.println(AstUtils.stringifyTree(tree));
		System.out.println("____________________________________");
		assertNotNull(tree);
		return tree;

	}

	/**
	 * checks the generated ast for the existence of a specific value. Use this method sparingly, since it is
	 * prone to verifying too much.
	 *
	 * @param expectedValueInTree value to be searched for in the tree
	 * @param code                the code used to generate the tree
	 */
	protected void assertExistenceOfValue(String expectedValueInTree,
										  String code) {

		AstNode tree = ShellParser.parse(code);

		boolean success = AstUtils.containsValue(expectedValueInTree, tree);

		if (!success) {
			AstUtils.printTree(tree);
			System.out.println("____________________________________");

		}

		Assertions.assertTrue(success);

	}

	/**
	 * Verifies if code matches the passed dialects. returns true if and only if
	 * the expected dialects are exactly the same as the detected dialects
	 *
	 * @param code             the shell-script that is to be analyzed
	 * @param expectedDialects the dialects that this code is to match
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	protected void assertCodeIsOfDialect(String code,
										 ShellDialect... expectedDialects)
			throws JsonSyntaxException, IOException {


		Set<ShellDialect> detectedDialects = ShellParser.detectDialect(code);

		Set<ShellDialect> expectedDialectsSet = new HashSet<>(
				Arrays.asList(expectedDialects));
		assertEquals(expectedDialectsSet, detectedDialects);

	}

	/**
	 * verifies that code is not of a specific dialect. returns true if and only
	 * if the intersection between unwanted dialects and detected dialects is
	 * empty
	 *
	 * @param code             the shell-script that is to be analyzed
	 * @param unwantedDialects the dialects that this code should not contain
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	protected void assertCodeIsNotOfDialect(String code,
											ShellDialect... unwantedDialects)
			throws JsonSyntaxException, IOException {

		Set<ShellDialect> detectedDialects = ShellParser.detectDialect(code);

		Set<ShellDialect> unwantedDialectsSet = new HashSet<>(
				Arrays.asList(unwantedDialects));
		detectedDialects.retainAll(unwantedDialectsSet);
		assertTrue(detectedDialects.isEmpty());
	}

	/**
	 * checks the generated ast for the existence of a specific value
	 *
	 * @param expectedTokenInTree value to be searched for in the tree
	 * @param code                the code used to generate the tree
	 */
	protected void assertExistenceOfToken(String expectedTokenInTree,
										  String code) {

		AstNode tree = ShellParser.parse(code);

		boolean success = AstUtils.containsToken(expectedTokenInTree, tree);

		if (!success) {
			System.out.println(AstUtils.stringifyTree(tree));
		}

		Assertions.assertTrue(success);

	}

	/**
	 * checks the generated ast for the existence of a specific value
	 *
	 * @param unexpectedTokenInTree value to be searched for in the tree
	 * @param code                  the code used to generate the tree
	 */
	protected void assertAbsenceOfToken(String unexpectedTokenInTree,
										String code) {

		AstNode tree = ShellParser.parse(code);

		boolean found = AstUtils.containsToken(unexpectedTokenInTree, tree);

		if (found) {
			System.out.println(AstUtils.stringifyTree(tree));
		}

		Assertions.assertFalse(found);

	}

	/**
	 * checks the generated ast for the existence of a specific token
	 *
	 * @param expectedTokenInTree token to be searched for in the tree
	 * @param code                the code used to generate the tree
	 * @param toPrint             if true, to be constructed tree also gets printed in
	 *                            console
	 */
	protected void assertExistenceOfToken(String expectedTokenInTree,
										  String code, boolean toPrint) {

		AstNode tree = ShellParser.parse(code);

		boolean success = AstUtils.containsToken(expectedTokenInTree, tree);

		if (toPrint || !success) {
			System.out.println(AstUtils.stringifyTree(tree));
		}

		Assertions.assertTrue(success);

	}

	/**
	 * checks the generated ast for the existence of a specific value
	 *
	 * @param expectedTokenInTree value to be searched for in the tree
	 * @param code                the code used to generate the tree
	 * @param toPrint             if true, to be constructed tree also gets printed
	 */
	protected void assertAbsenceOfToken(String expectedTokenInTree, String code,
										boolean toPrint) {

		AstNode tree = ShellParser.parse(code);

		boolean found = AstUtils.containsToken(expectedTokenInTree, tree);

		if (toPrint || found) {
			System.out.println(AstUtils.stringifyTree(tree));

		}

		Assertions.assertFalse(found);

	}


	/**
	 * checks if generated ast contains the passed token as many times as
	 * expected
	 *
	 * @param expectedCount expected count of token
	 * @param token         the token that is to be searched for
	 * @param code          the code used to generate the tree
	 */
	protected void assertCorrectCountOfToken(int expectedCount, String token,
											 String code) {
		AstNode tree = ShellParser.parse(code);


		int actualCount = AstUtils.countOccurencesOfToken(token, tree);
		Assertions.assertEquals(expectedCount, actualCount);
	}

	/**
	 * checks if generated ast contains the passed token as many times as
	 * expected
	 *
	 * @param expectedCount expected count of token
	 * @param token         the token that is to be searched for
	 * @param code          the code used to generate the tree
	 */
	protected void assertCorrectCountOfToken(int expectedCount, String token,
											 String code, boolean toPrint) {
		AstNode tree = ShellParser.parse(code);


		int actualCount = AstUtils.countOccurencesOfToken(token, tree);

		if (toPrint) {
			System.out.println(AstUtils.stringifyTree(tree));
		}

		assertEquals(expectedCount, actualCount);
	}

	/**
	 * verifies that the successor-token is contained inside of the sub-tree
	 * that are found under precedessor-token. in arithmetic expressions or test
	 * expressions, if a token appears on a subsequent level, this implies a
	 * higher precedence, allowing us to verify if the precedence is built
	 * properly. returns true if the successor-token is found in any occurence
	 * of the precedessor-token in the tree created from the passed code.
	 *
	 * @param precedessorToken token that is supposed to follow after the
	 *                         successor
	 * @param successorToken   token that represents the sub-tree
	 * @param code             the code used to generate the AST
	 */
	protected void assertCorrectOrderOfTokens(String precedessorToken,
											  String successorToken, String code) {

		boolean correctOrder = false;

		AstNode tree = ShellParser.parse(code);

		List<AstNode> occurencesOfPrecedessor = AstUtils
				.getAllOccurencesOf(precedessorToken, tree);

		for (AstNode precedessor : occurencesOfPrecedessor) {
			List<AstNode> occurencesOfSuccessor = AstUtils
					.getAllOccurencesOf(successorToken, precedessor);
			if (occurencesOfSuccessor.size() > 0) {
				correctOrder = true;
			}
		}
		assertTrue(correctOrder);

	}

	/**
	 * takes a sequence of comma separated shell grammar tokens and a shell
	 * script. converts the shell-script into an AST and compares the children
	 * of the complete commands token (called "top-level-token" here) to the
	 * passed tokens.
	 *
	 * @param tokens expected tokens in the top level tokens of code
	 * @param code   the to be parsed code
	 */
	protected void assertCorrectOrderOfTopLevelToken(String tokens,
													 String code) {

		AstNode tree = ShellParser.parse(code);
		AstNode completeCommands = AstUtils.getCompleteCommands(tree);
		List<String> stringifiedExpectedTokens = Arrays
				.asList(tokens.split(","));
		List<AstNode> topLevelTokens = completeCommands.getChildren();

		List<String> stringifiedTopLevelTokens = topLevelTokens.stream()
				.map(token -> token.getName()).collect(Collectors.toList());

		assertEquals(stringifiedExpectedTokens, stringifiedTopLevelTokens);
	}

	/**
	 * takes a script and attempts to generate a tree out of it. only true if all tokens get incorporated into
	 * the to be generated tree.
	 *
	 * @param code input that is to be turned into a tree
	 */
	protected void assertScriptHasBeenFullyParsed(String code) {

		assertTrue(ShellParser.hasParsedAllTokens(code));

	}

	/**
	 * represents negated version of {@link #assertScriptHasBeenFullyParsed(String)}.
	 *
	 * @param code input that is to be turned into a tree
	 */
	protected void assertScriptHasNotBeenFullyParsed(String code) {

		assertFalse(ShellParser.hasParsedAllTokens(code));

	}


	/**
	 * The method <code>assertOk</code> parses the parameter string and prints the
	 * prints the generated tree in the console. This method should never be used,
	 * aside quick structure verification before using the other methods depicted here
	 *
	 * @param s    the shell input
	 * @param tree the expected tree
	 */
	protected AstNode assertOk(String s, String tree) {

		AstNode node = ShellParser.parse(s);
		assertNotNull(node);
		return node;
	}

	/**
	 * The method <code>before</code> sets up the test instance for each test
	 * case.
	 */
	@BeforeEach
	public void before() {

		ShellParser.create();

	}

	/**
	 * lexes code and prints the result in console
	 *
	 * @param code the shell code to be lexed
	 */
	protected List<Token> lexCode(String code) {
		Lexer sl = ShellLexer.create();
		return sl.lex(code);
	}

}
