package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.util.AstUtils;
import de.iteratec.sonar.shell.util.PreciseIssue;
import de.iteratec.sonar.shell.util.ShellVisitorInterface;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public abstract class AbstractCheckTester implements ShellVisitorInterface {


    /**
     * a succeeding check means that no transgression of the rule has been detected.
     *
     * @param code the code that shall be tested
     */
    protected void assertThatCheckSucceeds(String code) {
        AstNode ast = ShellParser.create().parse(code);
        assertTrue(this.visitNode(ast));
    }

    /**
     * a failing check means that a transgression of the rule has been detected.
     *
     * @param code the code that shall be tested
     */
    protected void assertThatCheckFails(String code) {
        AstNode ast = ShellParser.create().parse(code);
        assertFalse(this.visitNode(ast));
    }

    protected void assertThatIssuesAreGenerated(String code) {
        AstNode ast = ShellParser.create().parse(code);
        assertTrue(this.visitNodeAndCollectIssues(ast).size() > 0);
    }

    protected void assertThatThisManyIssuesAreGenerated(int expectedCount, String code) {
        AstNode ast = ShellParser.create().parse(code);
        assertTrue(this.visitNodeAndCollectIssues(ast).size() == expectedCount);
    }

    protected void assertThatNoIssuesAreGenerated(String code) {
        AstNode ast = ShellParser.create().parse(code);
        assertTrue(this.visitNodeAndCollectIssues(ast).size() == 0);
    }


    /**
     * checks if the line-range of the first issue matches the expected issue location.
     *
     * @param code      the code that shall have an issue generated on it
     * @param startLine expected starting line of issue
     * @param endLine   expected ending line of issue
     */
    protected void assertThatErrorHighlightingIsAt(String code, int startLine, int endLine) {
        AstNode ast = ShellParser.create().parse(code);
        List<PreciseIssue> issues = this.visitNodeAndCollectIssues(ast);
        PreciseIssue issue = issues.get(0);
        assertEquals(startLine, issue.primaryLocation().startLine());
        assertEquals(endLine, issue.primaryLocation().endLine());
    }

    /**
     * checks if the line-range and the column-positions of issue are as expected
     *
     * @param code      the code that shall have an issue generated on it
     * @param startLine expected starting line of issue
     * @param endLine   expected ending line of issue
     * @param startCol  starting column of issue
     * @param endCol    ending column of issue
     */
    protected void assertThatErrorHighlightingIsAt(String code, int startLine, int endLine, int startCol, int endCol) {
        AstNode ast = ShellParser.create().parse(code);
        List<PreciseIssue> issues = this.visitNodeAndCollectIssues(ast);
        PreciseIssue issue = issues.get(0);
        assertEquals(startLine, issue.primaryLocation().startLine());
        assertEquals(endLine, issue.primaryLocation().endLine());
        assertEquals(startCol, issue.primaryLocation().startLineOffset());
        assertEquals(endCol, issue.primaryLocation().endLineOffset());
    }


    protected void printTree(String code) {
        System.out.println(AstUtils.stringifyTree(code));
    }

    public static <T> Set<T> immutableSet(T... el) {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(el)));
    }


}
