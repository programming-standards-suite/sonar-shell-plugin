/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.util;

/**
 * Basic functionalities a Shell-script-file should contain.
 * 
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 *
 */
public interface ShellScriptFileInterface {

    /**
     * 
     * @return dialect of file.
     */
    ShellDialect getDialect();

    /**
     * 
     * @return name of file.
     */
    String getName();

    /**
     * 
     * @return content of file.
     */
    String getContent();

}
