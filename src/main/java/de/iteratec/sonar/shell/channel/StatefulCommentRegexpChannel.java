package de.iteratec.sonar.shell.channel;

import com.sonar.sslr.api.Token;
import com.sonar.sslr.api.Trivia;
import com.sonar.sslr.impl.Lexer;
import com.sonar.sslr.impl.LexerException;
import de.iteratec.sonar.shell.ast.LexerState;
import org.sonar.sslr.channel.Channel;
import org.sonar.sslr.channel.CodeReader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sonar.sslr.api.GenericTokenType.COMMENT;

public class StatefulCommentRegexpChannel extends Channel<Lexer> {

    private final StringBuilder tmpBuilder = new StringBuilder();
    private final Matcher matcher;
    private final String regexp;
    private final Token.Builder tokenBuilder = Token.builder();
    private final LexerState lexerState;

    public StatefulCommentRegexpChannel(String regexp, LexerState ls) {
        matcher = Pattern.compile(regexp).matcher("");
        this.regexp = regexp;
        lexerState = ls;
    }

    @Override
    public boolean consume(CodeReader code, Lexer lexer) {
        if (!lexerState.isInDoubleQuotes() && !lexerState.isInSingleQuotes()) {
            try {
                if (code.popTo(matcher, tmpBuilder) > 0) {
                    String value = tmpBuilder.toString();

                    Token token = tokenBuilder
                            .setType(COMMENT)
                            .setValueAndOriginalValue(value)
                            .setURI(lexer.getURI())
                            .setLine(code.getPreviousCursor().getLine())
                            .setColumn(code.getPreviousCursor().getColumn())
                            .build();

                    lexer.addTrivia(Trivia.createComment(token));

                    tmpBuilder.delete(0, tmpBuilder.length());
                    return true;
                }
                return false;
            } catch (StackOverflowError e) {
                throw new LexerException(
                        "The regular expression "
                                + regexp
                                + " has led to a stack overflow error. "
                                + "This error is certainly due to an inefficient use of alternations. " +
                                "See http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=5050507",
                        e);
            }
        } else {
            return false;
        }
    }
}
