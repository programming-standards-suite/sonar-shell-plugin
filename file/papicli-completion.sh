#!/bin/bash

_papicli() {
    local storagedir="/tmp"
    local storagefileglob="*-papicli"
    local switches=(--debug --help --version)
    local commands=(booking absence work-package package user status login logout plain doc)
    local operations=(list add change remove)
    local verbs=(GET POST DELETE)
    local dates=(today yesterday -2days -3days -4days last-week last-month)
    local times=(-)
    local params=()
    local words cword cur prev opts cmd op cparam

    for h in $(seq --equal-width 08 19)
    do
        times=(${times[@]} $(seq --equal-width ${h}.00 0.15 ${h}.45 | sed 's/\./:/g'))
    done

    COMPREPLY=()
    words=("${COMP_WORDS[@]}")
    cword="${COMP_CWORD}"
    cur="${words[cword]}"
    prev="${words[cword-1]}"
    _get_comp_words_by_ref -n : cur prev words cword &> /dev/null
    for word in "${words[@]:1:$((cword-1))}"
    do
        case "${word}" in
            booking | b | absence | ab | work-package | w | package | p)
                cmd="${word}"
                ;;
            list | ls | add | a | change | c | remove | rm)
                op="${word}"
                ;;
            a | c)
                opts="${operations}"
                ;;
            --debug | -d | --help | -h | --version | -V | '')
                ;;
            *)
                params=("${params[@]}" "${word}")
                ;;
        esac
    done

    if [[ -z "${cmd}" || -z "${op}" ]]
    then
        case "${prev}" in
            absence | ab)
                opts="list"
                ;;
            booking | b)
                opts="list add change remove"
                ;;
            work-package | w | package | p)
                opts="list"
                ;;
            plain)
                opts="${verbs[@]}"
                ;;
            *)
                opts="${commands[@]} ${switches[@]}"
                ;;
        esac
    elif [[ -n "${cmd}" && -n "${op}" ]]
    then
        local IFS=$'\n'
        local storagefile="$(find "${storagedir}/" -name "${storagefileglob}" -printf '%T@\t%p\n' 2> /dev/null | sort --numeric-sort | head -1 | cut -f2)"
        if [[ -f "${storagefile}" ]]
        then
            local storage="$(cat "${storagefile}" 2> /dev/null)"
            local bookingids=($(jq --raw-output '.bookings[]?.id?' <<< "${storage}" | sort --numeric))
            local bookingnotes=($(jq '.bookings[]?.note?' <<< "${storage}" | sort --unique))
            local packagenames=($(jq --raw-output '.workPackages[]? | "\(.id?) # \(.label?)"' <<< "${storage}" | sort --unique))
            local packagetasks=($(jq '.workPackages[]? | .task // empty' <<< "${storage}" | sort --unique))
        fi
        local resourceop1=('${dates[@]}' '${dates[@]}')
        local resourceop2=('${dates[@]}' '${times[@]}' '${times[@]}' '${packagenames[@]}' '${packagetasks[@]}' '${bookingnotes[@]}')
        local resourceop3=('${bookingids[@]}' '${dates[@]}' '${times[@]}' '${times[@]}' '${packagenames[@]}' '${packagetasks[@]}' '${bookingnotes[@]}')
        local resourceop4=('withLegacy')
        case "${cmd}" in
            absence | ab | booking | b )
                cparam="$((cword-3))"
                case "${op}" in
                    list | ls)
                        opts="${resourceop1[cparam]}"
                        ;;
                    add | a)
                        opts="${resourceop2[cparam]}"
                        ;;
                    change | c | remove | rm)
                        opts="${resourceop3[cparam]}"
                        ;;
                    '')
                        opts="${operations[@]}"
                        ;;
                esac
                ;;
            work-package | w | package | p)
                case "${op}" in
                    list | ls)
                        opts="${resourceop4[cparam]}"
                        ;;
                esac
        esac
    else
        opts="${switches[@]}"
    fi

    cur="${cur//\"/\\\"}"
    COMPREPLY=( $(compgen -W "${opts//\"/\\\"}" -- "${cur}") )
    test "${#COMPREPLY[@]}" -eq 1 && COMPREPLY=( "${COMPREPLY[0]%% # *}" )
    __ltrim_colon_completions "${cur}" &> /dev/null
    return 0
}

complete -o nosort -F _papicli papicli

