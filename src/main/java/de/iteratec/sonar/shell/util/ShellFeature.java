/**
 * Copyright (c) 2018 iteratec GmbH
 */
package de.iteratec.sonar.shell.util;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * Describes a specific shell feature, which is supported through the
 * ShellGrammar. a list of feature is specified in features.json. The features
 * represent a key element in detecting the dialect of the to be checked shell
 * script.
 *
 * @author <a href="mailto:Navid.Noorshams@iteratec.de">Navid Noorshams</a>
 */
public class ShellFeature {

    // name of the feature
    String name;

    // dialects that actually permit said feature to be used
    List<ShellDialect> supportedDialects;

    /* indicator in generated shell ast that displays the usage of the feature, e.g. REPEAT_CLAUSE
     * usually a stringified ShellGrammar enum.
     */
    String affectedElement;

    /*
     * subelements that make up the structure of the feature.
     * this can vary depending on the used shell-dialect, e.g. REPEAT 10
     *  DO cmd DONE (bash) vs. REPEAT 10 CMD DONE (tcsh).
     */
    List<String> componentsOfAffectedElements;

    /* keyword in affected element that indicates the usage of the feature, e.g. ENDIF
     * usually a ShellLexer.Keywords element
     */
    String indicatingKeyword;

    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("[");

        List<Field> fields = Arrays.asList(this.getClass().getDeclaredFields());

        for (Field field : fields) {
            try {
                builder.append(field.get(this));
                if (fields.indexOf(field) != fields.size() - 1) {
                    builder.append(", ");
                }

            } catch (IllegalAccessException x) {
                //This field is empty, because of reasons
            }
        }

        builder.append("]");
        return builder.toString();

    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     */
    public List<ShellDialect> getSupportedDialects() {
        return supportedDialects;
    }

    /**
     * @return
     */
    public List<String> getComponentsOfAffectedElements() {
        return componentsOfAffectedElements;
    }

    /**
     * @return the affectedElement
     */
    public String getAffectedElement() {
        return affectedElement;
    }

    /**
     * @return the indicatingKeyword
     */
    public String getIndicatingKeyword() {
        return indicatingKeyword;
    }

}
