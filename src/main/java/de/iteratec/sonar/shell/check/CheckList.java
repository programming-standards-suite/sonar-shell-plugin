package de.iteratec.sonar.shell.check;

public class CheckList {

    public static final String REPOSITORY_KEY = "shell";

    private CheckList() {
    }

    public static Iterable<Class> getChecks() {
        return AbstractCheckTester.immutableSet(
                AllDeclaredVariablesAreReferencedCheck.class,
                CamelCaseIdentifierCheck.class,
                CyclomaticComplexityCheck.class,
                FunctionDeclarationCheck.class,
                NoUsageOfLocalCheck.class,
                UsageAfterDeclarationCheck.class,
                ScriptIsOfDialectCheck.class,
                ScopeComplexityCheck.class,
                AllTokensUsedCheck.class
        );
    }

}
