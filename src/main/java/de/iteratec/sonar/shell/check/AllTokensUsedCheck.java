package de.iteratec.sonar.shell.check;

import com.sonar.sslr.api.AstNode;
import de.iteratec.sonar.shell.ast.ShellParser;
import de.iteratec.sonar.shell.util.IssueLocation;
import de.iteratec.sonar.shell.util.PreciseIssue;
import org.junit.jupiter.api.Test;
import org.sonar.check.Priority;
import org.sonar.check.Rule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Rule(key = "SH008",
        name = "No token is left unused when parsing the script",
        description = "Verifies that all tokens have been used when constructing the AST",
        priority = Priority.CRITICAL,
        tags = {"pre-analysis"})
public class AllTokensUsedCheck extends AbstractCheckTester {

    List<PreciseIssue> issues = new ArrayList();

    String failedParsingMessage = "" +
            "There was a problem parsing this script. " +
            "Contact the maintainers of this plugin with the affected script.";

    @Override
    public boolean visitNode(AstNode ast) {
        return true;
    }

    @Override
    public List<PreciseIssue> visitNodeAndCollectIssues(AstNode node) {
        issues.add(new PreciseIssue(IssueLocation.atFileLevel(failedParsingMessage)));
        return issues;
    }


    @Test
    public void t1() {
        String code = "my_function() { \\}";
        AstNode ast = ShellParser.parse(code);
        ast.getType();
        assertTrue(true);
    }

}
