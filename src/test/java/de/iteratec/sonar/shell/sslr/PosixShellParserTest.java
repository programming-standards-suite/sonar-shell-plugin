/*
 * Copyright (c) 2018 iteratec GmbH
 *
 * This file is distributed under the
 * 3-clause BSD license. See file
 * LICENSE for details.
 */

package de.iteratec.sonar.shell.sslr;

import de.iteratec.sonar.shell.util.FileImporter;
import de.iteratec.sonar.shell.util.ShellScriptFile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.sonar.api.internal.apachecommons.lang.StringUtils;

/**
 * This is a test suite for a POSIX
 * Shell Parser. The test cases are
 * based on the POSIX standard IEEE Std
 * 1003.1-2017 as documented in <a href=
 * "http://pubs.opengroup.org/onlinepubs/9699919799/">Section
 * 2: Shell Command Language</a>.
 *
 * @author <a href=
 * "mailto:Gerd.Neugebauer@iteratec.de">Gerd
 * Neugebauer</a>
 */
public class PosixShellParserTest
        extends AbstractParserTester {

    /**
     * Test cases for section 2.2.1.
     *
     * @param c the normal character which
     *          needs escaping
     */
    @ParameterizedTest(
            name = "escaped \"{0}\" is passed through")
    @DisplayName("escaping normal characters")
    @ValueSource(
            strings = {"a", "b", "f", "n", "r", "t", "1",
                    ",", "_", "^", "ä", "ß"})
    public void section221a(String c) {
        assertExistenceOfValue(c, "echo \\" + c);
    }

    @ParameterizedTest(name = "\"{0}\" is passed through")
    @DisplayName("strings containing escaped characters")
    @CsvSource({"1234\\a", "bfa\\h", "43\\13", "bgb\\gbgn",
            "ööö\\ööö"})
    public void section221MultipleEscapes(String c) {
        assertCorrectCountOfToken(2, "ESCAPE",
                "echo \\" + c);

    }

    @ParameterizedTest(name = "\"{0}\" is passed through")
    @DisplayName("strings containing escaped characters and and command escaped")
    @CsvSource({"1234\\a", "bfa\\h", "43\\13", "bgb\\gbgn",
            "r", "ööö\\ööö", "1", ",", "_", "^", "ä", "ß"})
    public void section221Extended(String c) {
        String code = "\\e\\c\\ho \\" + c;
        assertCorrectCountOfToken(1, "CMD_NAME", code);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);
    }

    /**
     * Test cases for section 2.2.1.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "escaped \"{0}\" is passed through")
    @DisplayName("escaping special characters")
    @ValueSource(
            strings = {"|", "&", ";", "<", ">", "(", ")",
                    "$", "`", "\\", "\"", "'", //
                    "#", "~"})
    public void section221b(String c) {
        String code = "echo \\" + c;
        assertExistenceOfToken("ESCAPED_SPECIAL_CHARACTER",
                code);
    }

    /**
     * Test cases for section 2.2.2.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "quoted \"{0}\" is passed through")
    @DisplayName("in double quotes \\ is escape character")
    @ValueSource(strings = {"$", "`", "\""})
    public void section222a(String c) {
        String code = "echo \"\\" + c + "\"";
        assertExistenceOfToken(
                "ESCAPED_SPECIAL_CHARACTER_DQ", code);

    }

    /**
     * Test cases for section 2.2.2.
     *
     * @param c the normal character which
     *          needs escaping
     */
    @ParameterizedTest(
            name = "quoted \"{0}\" is passed through")
    @DisplayName("double quoting normal characters")
    @ValueSource(
            strings = {"a", "b", "n", "r", "t", "1", ",",
                    "_", "^", "ä", "ß",})
    public void section222b(String c) {
        String code = "echo \"\\" + c + "\"";

        assertExistenceOfValue(c, code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
    }

    /**
     * Test cases for section 2.2.2. The
     * testcase with the whitespace won't
     * work properly.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "quoted \"{0}\" is passed through")
    @DisplayName("double quoting special characters")
    @ValueSource(
            strings = {"|", "&", ";", "<", ">", "(", ")",
                    "\"", "'", " ", "\t", //
                    "*", "?", "[", "#", "~", "=", "%"})
    public void section222c(String c) {

        if (c.equals(" ") || c.equals("\t")) {
            assertExistenceOfToken("DOUBLE_QUOTED_WORD",
                    "echo \"\\" + c + "\"");
        } else {
            assertExistenceOfValue(c,
                    "echo \"\\" + c + "\"");
        }
    }

    /**
     * Test cases for section 2.2.2.
     *
     * @param c the normal character which
     *          needs escaping
     */
    @ParameterizedTest(
            name = "quoted \"{0}\" is passed through")
    @DisplayName("quoting normal characters")
    @ValueSource(
            strings = {"a", "b", "n", "r", "t", "1", ",",
                    "_", "^", "ä", "ß",})
    public void section222d(String c) {
        String code = "echo '\\" + c + "'";
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
    }

    /**
     * Test cases for section 2.2.2.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "quoted \"{0}\" is passed through")
    @DisplayName("quoting special characters")
    @ValueSource(
            strings = {"|", "&", ";", "<", ">", "(", ")",
                    "$", "`", "\\", "\"", "'", " ", "\t", //
                    "*", "?", "[", "#", "~", "=", "%"})
    public void section222e(String c) {
        String code = "echo '\\" + c + "'";
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);

    }

    /**
     * Test cases for section 2.3.
     *
     * @param s the comment
     */
    @ParameterizedTest(
            name = "the comment ${0} is absorbed")
    @DisplayName("comments")
    @CsvSource({"a b c.", "$1 xxx", "{{", " !", " >>abc"})
    public void section23Comment(String s) {
        String code = "echo #" + s + "\r\n";
        assertAbsenceOfToken("CMD_SUFFIX", code);

    }

    /**
     * Test cases for section 2.5.1.
     *
     * @param c the parameter number
     */
    @ParameterizedTest(
            name = "positional parameter ${0} is accepted")
    @DisplayName("positional parameters")
    @CsvSource({"1, 1", "2, 2", "8, 8", "9, 9", "{10}, 10",
            "{32}, 32", "{02}, 02", "{0111}, 0111"})
    public void section251(String c, String d) {
        String code = "echo $" + c;
        assertExistenceOfValue(d, code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);

    }

    /**
     * Test cases for section 2.5.2.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "special parameter ${0} is accepted")
    @DisplayName("special parameters")
    @CsvSource({"@", "*", "'#'", "?", "-", "$", "!", "0"})
    public void section252(String c) {
        String code = "echo $" + c;
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code); // echo
        assertExistenceOfValue(c, code);
    }

    /**
     * Test cases for section 2.6.1.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "tilde expansion of ~{0} is accepted")
    @DisplayName("tilde expansion")
    @CsvSource({"abc, abc", "abc/def, def", "a23, a23",
            "a13/xyz/123, 123"})
    public void section261(String c, String d) {
        String code = "echo ~" + c;
        assertExistenceOfToken("TILDE_EXPANSION", code); // echo
        assertExistenceOfValue(d, code);
    }

    /**
     * Test cases for section 2.6.2.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "parameter expansion of ${0} is accepted")
    @DisplayName("parameter expansion")
    @CsvSource({"PS, null", "abc, null", //
            "a:-123,:-", "a:-def,:-", //
            "a:=123,:=", "a:=def,:=", //
            "a:?,:?", "a:?123,:?", "a:?def,:?", //
            "a:+123,:+", "a:+def,:+", //
            "'#abc', null", //
            "a%def,%", "a%.c,%", //
            "a%%def,%%", //
            "a#def,#", "a#$HOME, #", "a#\"*\",#", //
            "a##def, ##", //
            "a:-$(ls), :-"})
    public void section262(String c, String operator) {
        String toParse = "echo ${" + c + "} ";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", toParse); // echo
        if (!operator.equals("null")) {
            assertExistenceOfValue(operator, toParse);
            assertExistenceOfToken(
                    "PARAMETER_EXPANSION_OPERATOR",
                    toParse);
        }
    }

    /**
     * Test cases for section 2.6.2.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "parameter expansion of ${0} is accepted")
    @DisplayName("difficult parameter expansion")
    @CsvSource({ //
            "a%%/*,%%", //
            "a##*/, ##", //
    })
    public void section262StopFailing(String c,
                                      String operator) {
        String toParse = "echo ${" + c + "} ";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", toParse); // echo
        assertExistenceOfValue(operator, toParse);
        assertExistenceOfToken(
                "PARAMETER_EXPANSION_OPERATOR", toParse);
    }

    /**
     * Test cases for section 2.6.3.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "command substitution {0} is accepted")
    @DisplayName("command substitution")
    @CsvSource({"$(ls)", "`ls`"})
    public void section263(String c) {
        String code = "echo " + c;
        assertExistenceOfValue("ls", code);
    }

    /**
     * Test cases for section 2.6.3. This
     * time, commands are chained.
     *
     * @param c the parameter character
     */
    @ParameterizedTest(
            name = "command substitution in command substitution")
    @DisplayName("command substitution")
    @CsvSource({"$(echo $(ls))"})
    public void section263Chained(String c) {

        String code = "echo " + c;
        assertExistenceOfValue("ls", code);
        assertCorrectCountOfToken(2,
                "PARENTHESIZED_PARAMETER", code);
    }

    /**
     * Test cases for section 2.6.4.
     *
     * @param expr the expression
     */
    @ParameterizedTest(
            name = "arithmetic expansion of $(({0})) is accepted")
    @DisplayName("arithmetic expansion")
    @CsvSource({ //
            "255<<4", //
            "3>=4 ", //
            "x=23", //
            "3 <= 4", //
            "1>2", //
            "5==6", //
            "5!=6", //
            "1&3", //
            "1<2", //
            "1^2", //
            "1&2", //
            "1|2", //
            "3&&4", //
            "3||4", //
            "1>2?3:4", //
            "x*=23", //
            "x+=23", //
            "x/=23", //
            "x-=23", //
            "x%=23", //
            "1+24*4+-1", //
            " (1+3) * 6", //
            "1+3*6", //
            "x<<=23", //
            "x>>=23", //
            "x&=23", //
            "x|=23", //
            "x^=23", //
            "3>>1<<2", //
            "3>>1!=2<<4", //
            "1>2?3:4>>1!=2<<4", //
            "(1+3)*6", //
            "3==4==4", //
            "'1'", //
            "0x20", //
            "040", //
            "$a", //
            "1+2", //
            "2-3", //
            "4*5", //
            "6/7", //
            "~1234", //
            "-1234", //
            "!1234", //
            "+1234", //
            "4%3", //
            "1>>2" //
    })
    public void section264(String expr) {
        String code = "echo $((" + expr + "))";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
    }

    /**
     * Test cases for section 2.6.4.
     *
     * @param expr the expression
     */
    @ParameterizedTest(
            name = "arithmetic expansion of $(({0})) is accepted")
    @DisplayName("arithmetic expansion")
    @CsvSource({ //
            "x+=23", //
            "x/=23", //
            "x-=23", //
            "x%=23", //
    })
    public void xoxo(String expr) {
        String code = "echo $((" + expr + "))";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code, true);
    }


    @ParameterizedTest(
            name = "using the expression command")
    @DisplayName("Arithmetic expansion using expression")
    @CsvSource({"1 + 3"})
    public void additiveExpansionUsingExpr(String expr) {
        String code = "$(expr " + expr + " )";
        assertExistenceOfToken("ADDITIVE_EXPRESSION", code);
    }

    @ParameterizedTest(
            name = "using the expression command")
    @DisplayName("Arithmetic expansion using expression")
    @CsvSource({"\\( 1 + 3 \\) * 4"})
    public void parenthesizedExpansionUsingExpr(
            String expr) {
        String code = "$(expr " + expr + " )";
        assertExistenceOfToken("PARENTHESIZED_EXPRESSION",
                code);
        assertExistenceOfToken("ADDITIVE_EXPRESSION", code);
        assertExistenceOfToken("MULTIPLICATIVE_EXPRESSION",
                code);
    }

    @ParameterizedTest(
            name = "using the expression command")
    @DisplayName("Arithmetic expansion using expression")
    @CsvSource({"3 | 4"})
    public void bitwiseExpansionUsingExpr(String expr) {
        String code = "$(expr " + expr + " )";
        assertExistenceOfToken("BITWISE_OR", code);
    }

    @ParameterizedTest(
            name = "using the expression command")
    @DisplayName("Arithmetic expansion using expression")
    @CsvSource({"6 <= 5"})
    public void arithmeticExpansionUsingExpr(String expr) {
        String code = "$(expr " + expr + " )";
        assertExistenceOfToken("COMPARISON", code);
    }

    /**
     * Test cases for section 2.7.1.
     *
     * @param c the redirection spec
     */
    @ParameterizedTest(
            name = "redirecting input with {0} is accepted")
    @DisplayName("redirecting input")
    @CsvSource({"<abc", "<a.b.c", "5<abc", "6<a.b.c"})
    public void section271(String c) {

        String code = "echo " + c;
        assertExistenceOfToken("IO_FILE", code);
    }

    /**
     * Test cases for section 2.7.2.
     *
     * @param c the redirection spec
     */
    @ParameterizedTest(
            name = "redirecting output with {0} is accepted")
    @DisplayName("redirecting output")
    @CsvSource({">abc", ">a.b.c", "5>abc", "6>a.b.c", //
            ">|abc", ">|a.b.c", "5>|abc", "6>|a.b.c"})
    public void section272(String c) {
        String code = "echo " + c;
        assertExistenceOfToken("IO_FILE", code);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);
    }

    /**
     * Test cases for section 2.7.3.
     *
     * @param c the redirection spec
     */
    @ParameterizedTest(
            name = "appending redirected output with {0} is accepted")
    @DisplayName("appending redirected output")
    @CsvSource({">>abc", ">>a.b.c", "5>>abc",
            "6>>${a.b.c}"})
    public void section273(String c) {
        String code = "echo " + c;
        assertExistenceOfToken("IO_FILE", code);
    }

    /**
     * Test cases for section 2.7.4.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "here documents to the delimiter {0} are accepted")
    @DisplayName("single here documents")
    @CsvSource({"EOF", "eof"})
    public void section274a(String c) {
        String code = "echo <<" + c + "\n123\n45\n6\n" + c
                + "\n";
        assertExistenceOfToken("HERE_END", code);
    }

    @Test
    @DisplayName("Multiple redirects employing dollar operator")
    public void multipleRedirects() {
        String code = "echo xo > $xoxo > xoxoxo";
        assertCorrectCountOfToken(2, "IO_FILE_PUNCTUATOR",
                code);
        assertCorrectCountOfToken(3, "CMD_SUFFIX", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);

    }

    /**
     * Test cases for section 2.7.4.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "here documents to the delimiters {0} and {1} are accepted")
    @DisplayName("double here documents")
    @CsvSource({"EOF,EOF", "eof1,eof2"})
    public void section274b(String c, String d) {
        String code = "echo <<" + c + ";echo <<" + d //
                + "\n123\n45\n6\n" + c //
                + "\nabc\ndef\ng\n" + d + "\n";
        assertCorrectCountOfToken(2, "HERE_END", code);

    }

    /**
     * Test cases for section 2.7.4.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "here documents with stream to the delimiter {0} are accepted")
    @DisplayName("single here documents with stream")
    @CsvSource({"EOF", "eof"})
    public void section274c(String c) {
        assertCorrectCountOfToken(1, "HERE_END", "echo 3<<"
                + c + "\n123\n45\n6\n" + c + "\n");
    }

    /**
     * Test cases for section 2.7.4.
     *
     * @param c the special character
     *          which is escaped
     */
    @ParameterizedTest(
            name = "here documents with stream to the delimiters {0} and {1} are accepted")
    @DisplayName("double here documents with stream")
    @CsvSource({"EOF,EOF", "eof1,eof2"})
    public void section274d(String c, String d) {
        String code = "echo 4<<" + c + ";echo 5<<" + d //
                + "\n123\n45\n6\n" + c //
                + "\nabc\ndef\ng\n" + d + "\n";
        assertCorrectCountOfToken(2, "HERE_END", code);
    }

    /**
     * Test cases for section 2.7.5.
     *
     * @param pre  the target stream
     * @param post the source stream
     */
    @ParameterizedTest(
            name = "duplicating with delimiters {0} and {1} are accepted")
    @DisplayName("duplicating input file descriptor")
    @CsvSource({"'',1", "'',-", "1,2", "2,-"})
    public void section275(String pre, String post) {
        String input = "echo " + pre + "<&" + post;
        assertExistenceOfToken("IO_FILE_PUNCTUATOR", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);
        assertExistenceOfValue("<&", input);
    }

    /**
     * Test cases for section 2.7.6.
     *
     * @param pre  the target stream
     * @param post the source stream
     */
    @ParameterizedTest(
            name = "here documents with stream to the delimiters {0} and {1} are accepted")
    @DisplayName("duplicating input and output file descriptor")
    @CsvSource({"'',abc", "'',a-b-c.def", "3,abc",
            "3,a-b-c.def"})
    public void section276(String pre, String post) {
        String input = "echo " + pre + "<>" + post;
        assertExistenceOfToken("IO_FILE_PUNCTUATOR", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);

    }

    /**
     * Test cases for section 2.7.6.
     *
     * @param pre  the target stream
     * @param post the source stream
     */
    @ParameterizedTest(
            name = "here documents with stream to the delimiters {0} and {1} are accepted")
    @DisplayName("duplicating input file descriptor")
    @CsvSource({"'',1", "'',-", "1,2", "2,-"})
    public void section276b(String pre, String post) {
        String input = "echo " + pre + ">&" + post;
        assertExistenceOfToken("IO_FILE_PUNCTUATOR", input);
        assertExistenceOfValue(">&", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);

    }

    /**
     * Test cases for section 2.9.2.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("pipelines")
    @CsvSource({"cat a | wc -l, 1", //
            "cat a | grep xxx | wc, 2"})
    public void section292(String command, int count) {
        assertCorrectCountOfToken(count + 1,
                "SIMPLE_COMMAND", command);
        assertCorrectCountOfToken(count, "PIPE", command);
    }

    /**
     * Test cases for section 2.9.2.
     * containing bangs
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("pipelines containing bangs")
    @CsvSource({"!cat a | wc -l, 1", //
            "!cat a | grep xxx | wc, 2"})
    public void section292ContainingBang(String command,
                                         int count) {
        assertCorrectCountOfToken(count, "PIPE", command);
        assertExistenceOfToken("BANG", command);
    }

    /**
     * Test cases for section 2.9.3.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("and-or lists")
    @CsvSource({"false && true", //
            "true && true", //
            "echo a && echo b && echo c", //
            "false || true", //
            "true || true", //
            "echo a || echo b || echo c", //
            "false && echo foo || echo bar", //
            "true || echo foo && echo bar"})
    public void section293AndOr(String command) {
        if (command.contains("&")) {
            int count = StringUtils.countMatches(command,
                    "&&");
            assertCorrectCountOfToken(count, "AND",
                    command);

        }
        if (command.contains("|")) {
            int count = StringUtils.countMatches(command,
                    "||");
            assertCorrectCountOfToken(count, "OR", command);
        }
    }

    /**
     * Test cases for section 2.9.3.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("asynchronous lists")
    @CsvSource({"false & true", //
            "true & true", //
            "echo a & echo b & echo c"})
    public void section293AsynchronousLists(
            String command) {

        int count = StringUtils.countMatches(command, "&");
        assertCorrectCountOfToken(count, "AMPERSAND",
                command);
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("compound lists")
    @CsvSource({"(true ; false), 2", //
            "(echo a ; echo b ; echo c;), 3"}) //
    public void section294CompoundListsNoNewlines(
            String command, int commandCount) {
        int count = StringUtils.countMatches(command, ";");
        assertExistenceOfToken("COMPOUND_LIST", command);
        assertCorrectCountOfToken(count, "SEMICOLON",
                command);
        assertCorrectCountOfToken(commandCount,
                "SIMPLE_COMMAND", command);

    }

    /**
     * Additional test cases for section
     * 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "{0} \\n} is accepted")
    @DisplayName("compound lists")
    @CsvSource({"{ true ; false, 2", //
            "{ echo a ; echo b ; echo c, 3"}) // , //
    public void section294CompoundListsNewlineAtTheEnd(
            String command, int commandCount) {
        String code = command + ";  }";
        int count = StringUtils.countMatches(code, ";");
        assertExistenceOfToken("COMPOUND_LIST", code);
        assertCorrectCountOfToken(count, "SEMICOLON", code);
        assertCorrectCountOfToken(commandCount,
                "SIMPLE_COMMAND", code);
    }

    /**
     * Test cases for section 2.9.4.
     * unnecessarily parameterized.
     *
     * @param command1 first command
     * @param command2 second command
     */
    @ParameterizedTest(name = "{0} is accepted")
    @DisplayName("compound lists")
    @CsvSource({"true, false"})
    public void section294CompoundListsTwoNewlines(
            String command1, String command2) {
        String code = "{" + command1 + " \n " + command2
                + "\n}";
        assertCorrectCountOfToken(2, "EOL", code);
        assertExistenceOfToken("COMPOUND_LIST", code);
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                code);

    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param pre  the initial parenthesis
     * @param post the final ;;
     */
    @ParameterizedTest(name = "case ... is accepted")
    @DisplayName("case")
    @CsvSource({"'',;;", //
            "'',''", //
            "(,;;", //
            "(,''"})
    public void section294Case(String pre, String post) {
        String code = "case c in \n" + pre + "a) echo a;;\n"
                + pre + "" + "b) echo b" + post + "\n esac";
        assertExistenceOfToken("CASE_CLAUSE", code);
        if (post.length() != 0) {
            assertCorrectCountOfToken(2, "CASE_ITEM", code);
        } else {
            assertCorrectCountOfToken(1, "CASE_ITEM", code);
            assertCorrectCountOfToken(1,
                    "CASE_ITEM_OPTIONAL_DSEMI", code);
        }
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
    }

    /**
     * Test case for section 2.9.4, this
     * time attempting the usage of a piped
     * pattern in the case statement, e.g.
     * Xx|Yy
     */
    @Test
    public void section294CaseWithVariants() {
        String code = "case x in \n "
                + "Yy|Xx) echo x ;; \n b) echo y;;\n "
                + "esac";
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertCorrectCountOfToken(2, "PATTERN", code);
        assertCorrectCountOfToken(2, "CASE_ITEM", code);
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);

    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "for x {0} ... is accepted")
    @DisplayName("for loop with newlines")
    @CsvSource({"''", //
            "in;", //
            "in a;", //
            "in a b;", //
            "in a b ;"})
    public void section294ForLoop(String command) {
        String code = "for x " + command
                + " do\necho $x\ndone";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("DO", code);
        assertCorrectCountOfToken(2, "EOL", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);
        assertExistenceOfToken("DONE", code);

    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(name = "for x {0} ... is accepted")
    @DisplayName("for loop with semicolon")
    @CsvSource({"'', 1", //
            "in;, 2", //
            "in a;, 2", //
            "in a b;, 2"})
    public void section294ForLoopSemicolon(String command,
                                           int count) {

        String code = "for x " + command
                + " \n do echo $x; done";

        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("DO", code);
        assertCorrectCountOfToken(count, "SEMICOLON", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("DONE", code);

    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "if {0} then ... fi is accepted")
    @DisplayName("if")
    @CsvSource({"true;", //
            "false;", //
            "(a;b)"})
    public void section294If(String command) {
        String code = "if " + command
                + " then echo '.' \nfi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("FI", code);
        if (command.equals("(a;b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "if {0} then ... elif ... fi is accepted")
    @DisplayName("if elif")
    @CsvSource({"true;", //
            "false;", //
            "(a;b)"})
    public void section294IfElIf(String command) {
        String code = "if " + command
                + " then echo x\n elif " + command
                + " then echo '.' \nfi";
        assertExistenceOfToken("IF", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("ELIF", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("FI", code);
        if (command.equals("(a;b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "if {0} then ... elif ... else ... fi is accepted")
    @DisplayName("if elif else")
    @CsvSource({"true;", //
            "false;", //
            "(a;b;)"})
    public void section294IfElIfElse(String command) {
        String code = "if " + command
                + " then echo x\nelif " + command
                + " then echo '.' \n else echo ':'\nfi";
        assertExistenceOfToken("IF", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("ELIF", code);
        assertExistenceOfToken("ELSE", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("FI", code);
        if (command.equals("(a;b;)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "if {0} then ... else ... fi is accepted")
    @DisplayName("if else")
    @CsvSource({"true;", //
            "false;", //
            "(a;b)"})
    public void section294IfElse(String command) {
        String code = "if " + command
                + " then echo '.' \n else echo ':'\nfi";
        assertExistenceOfToken("IF", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("ELSE", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("FI", code);
        if (command.equals("(a;b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "until {0} do ... done is accepted")
    @DisplayName("until")
    @CsvSource({"true;", //
            "false;", //
            "(a;b)"})
    public void section294Until(String command) {
        String code = "until " + command
                + " do\n echo '.' \ndone";
        assertExistenceOfToken("UNTIL_CLAUSE", code);
        assertExistenceOfToken("DO", code);
        assertExistenceOfToken("DONE", code);
        if (command.equals("(a;b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }
    }

    /**
     * Test cases for section 2.9.4.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = "while {0} do ... done is accepted")
    @DisplayName("while")
    @CsvSource({"(echo a;echo b)", "true;", //
            "false;"})
    public void section294While(String command) {
        String code = "while " + command
                + " do\n echo '.' \ndone";
        assertExistenceOfToken("WHILE_CLAUSE", code);
        assertExistenceOfToken("DO", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("DONE", code);
        if (command.equals("(echo a;echo b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
            assertCorrectCountOfToken(3, "CMD_SUFFIX",
                    code);
            assertCorrectCountOfToken(3, "SIMPLE_COMMAND",
                    code);
        } else {
            assertCorrectCountOfToken(1, "CMD_SUFFIX",
                    code);
            assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                    code);

        }

    }

    /**
     * Test cases for section 2.9.5.
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = " my_fct() {0} ... is accepted")
    @DisplayName("function definition")
    @CsvSource({ // "{}", //
            "{echo a > x.y.z;}", //
            "{cat < a.b.c; echo x;}"}) //
    public void section295NoNewlines(String command) {
        String code = "my_fct()" + command + "\n";
        assertExistenceOfToken("FUNCTION_BODY", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfToken("SEMICOLON", code);
        assertExistenceOfToken("SIMPLE_COMMAND", code);

    }

    /**
     * Test cases for section 2.9.5.,
     * employing newlines
     *
     * @param command the command
     */
    @ParameterizedTest(
            name = " my_fct() {0} ... is accepted")
    @DisplayName("function definition")
    @CsvSource({"{ " + "cat < a.b.c;" + " echo x;" + "}"})
    public void section2953NewlineAtTheEnd(String command) {
        String code = "my_fct()" + command + "\n";

        assertExistenceOfToken("FUNCTION_BODY", code);
    }

    /**
     * Test cases for section 2.9.5., this
     * time adding the newline in the
     * java-code, so that the function gets
     * properly parsed.
     *
     * @param pre  first command in the
     *             function body
     * @param post second command in the
     *             function body
     */
    @ParameterizedTest(
            name = " my_fct() {0} ... is accepted")
    @DisplayName("function definition")
    @CsvSource({"cat < a.b.c;, echo x;"})
    public void section2952NewlinesEverywhere(String pre,
                                              String post) {
        assertExistenceOfToken("FUNCTION_BODY",
                "my_fct(){\n" + pre + "\n" + post
                        + "\n}\n");
    }

    /**
     * returns command and single word as
     * input. Ignores the dollar sign.
     */
    @Test
    @DisplayName("Variables do not get expanded in single quotes")
    public void noVariableExpansionInSingleQuote() {
        String code = "echo \'do not expand the variable $name\'";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", code);
    }

    /**
     * does not prematurely break the
     * single-quoted word by using the
     * escaped string as delimiter.
     */
    @Test
    @DisplayName("Variables do not get expanded in single quotes, escaped single quote gets disabled")
    public void noVariableExpansionInSingleQuoteWithEscapedSingleQuote() {
        String code = "echo \'do not        expand \\'the variable $name'";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", code,
                true);
        assertCorrectCountOfToken(1, "SINGLE_QUOTED_WORD",
                code);
    }

    /**
     * returns command and single word as
     * input. considers the variable inside
     * of it.
     */
    @Test
    @DisplayName("Variables get expanded in double quotes")
    public void variableExpansionInDoubleQuote() {
        String code = "echo \"please do for following variable: $name then do the following\"";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCorrectCountOfToken(9, "WHITE_SPACE", code);
    }

    /**
     * returns command and a parameter
     * expansion in the respective double
     * quotes.
     */
    @Test
    @DisplayName("Variables get expanded when double-quotes are employed")
    public void variableExpansionInEcho() {
        assertExistenceOfToken("DOLLAR_CONSTRUCT",
                "echo \"$name\"; echo \"$name\";");
    }

    @Test
    @DisplayName("First variable does not get expanded when dollarsign gets escaped in double quote")
    public void noVariableExpansionWhenEscapedDollar() {
        String code = "echo \"\\$dontexpandthisname\"; echo \"$expandThisName\";";
        assertExistenceOfToken(
                "ESCAPED_SPECIAL_CHARACTER_DQ", code);
    }

    @ParameterizedTest
    @DisplayName("Variables do not get expanded when dollarsign gets escaped in double quote")
    @CsvSource({"\"names \" are for \" losers \", 2, 4", //
            "\"names \" are \" losers \", 2, 3", //
            "\"names \" \" losers \", 2, 2",
            " \"names are for losers \", 1, 1"})
    public void testingDoublequoteContext(String input,
                                          int countOfDoubleQuotedWords,
                                          int countOfSuffixes) {
        String toTest = "echo " + input + ";";
        assertCorrectCountOfToken(
                (countOfDoubleQuotedWords),
                "DOUBLE_QUOTED_WORD", toTest);
        assertCorrectCountOfToken((countOfSuffixes),
                "CMD_SUFFIX", toTest);

    }

    /**
     * Creates a double quoted word which
     * also contains a command expansion
     */
    @Test
    @DisplayName("Command substitution gets expanded when in double quotes")
    public void commandSubstitutionExpansionInDoubleQuotes() {
        String code = "echo \"abc`ls`xyz\"";
        assertExistenceOfToken("BACKTICK_SUBSTITUTION",
                code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
    }

    /**
     * Creates a double quoted word which
     * also contains a command expansion
     */
    @Test
    @DisplayName("Command substitution gets expanded when in double quotes")
    public void commandSubstitutionChainedWith() {
        String code = "echo \"do for each 'idiot' in this world ${crazy}\" > some.txt";
        assertExistenceOfToken("SIMPLE_COMMAND", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfToken("IO_FILE_PUNCTUATOR", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("FILENAME", code);
        assertCorrectCountOfToken(2,
                "DISABLED_SPECIAL_CHARACTER_DQ", code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);

    }

    @Test
    @DisplayName("simple command with multiple suffixes gets built properly")
    public void echoWithManySuffixes() {
        String code = "echo hello ${why} are you not using 'double-quotes'";
        assertExistenceOfToken("SIMPLE_COMMAND", code);
        assertCorrectCountOfToken(7, "CMD_SUFFIX", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);

    }

    @Test
    @DisplayName("simple command with multiple suffixes and a punctuator gets built properly")
    public void echoWithManySuffixesIncludingAPunctuator() {
        String code = "echo hello ${why} are yo:u not using 'double-quotes'";
        assertExistenceOfToken("SIMPLE_COMMAND", code);
        assertOk(code);
        assertCorrectCountOfToken(7, "CMD_SUFFIX", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
    }

    @Test
    @DisplayName("simple command with multiple suffixes and a keyword gets built properly")
    public void echoWithManySuffixesIncludingAKeyword() {
        String code = "echo hello ${why} do you not use 'double-quotes'";
        assertExistenceOfToken("SIMPLE_COMMAND", code);
        assertCorrectCountOfToken(7, "CMD_SUFFIX", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);

    }

    @Test
    @DisplayName("simple command with multiple suffixes and a keyword and a punctuator gets built properly")
    public void echoWithManySuffixesIncludingAKeywordAndAPunctuator() {
        String code = "echo hello ${why} do yo:u ! not use 'double-quotes'";
        assertCorrectCountOfToken(8, "CMD_SUFFIX", code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);

    }

    @Test
    @DisplayName("simple command with multiple suffixes including an option gets built properly")
    public void echoWithArgument() {
        String code = "echo -n \"Hello          \\Wo\\rld\"";
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(1, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(2, "ESCAPE", code);
        assertCorrectCountOfToken(1, "WHITE_SPACE", code);

    }

    @ParameterizedTest
    @DisplayName("simple command employing keywords gets built into ast")
    @CsvSource({ //
            "echo \"do this\" ", //
            "echo \"do `ls` forever\"", //
            "echo \"do {this} forever\"", //
            "echo \"do this+*\"", //
            "echo \"will !    do \\${th+is} forever?\"" //
    })
    public void echoWithKeywordsAsArguments(String input) {
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", input);
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);

    }

    @ParameterizedTest
    @DisplayName("escaped double-quoted-word does not get built")
    @CsvSource({ //
            "echo \\\"blabla this\\\" ", //
            "echo \\\"bla `ls` forever\\\"", //
            "echo \\\\\\\"blabla this\\\\\\\" "//
    })
    public void commandWithEscapedDoubleQuotedWord(
            String input) {

        assertAbsenceOfToken("DOUBLE_QUOTED_WORD", input);
    }

    @ParameterizedTest
    @DisplayName("double-quoted-word gets built despite escape-attempt")
    @CsvSource({ //
            "echo \\\\\"blabla this\\\\\" ", //
            "echo \\\\\"bla `ls` forever\\\\\"", //
            "echo \\\\\\\\\"blabla this\\\\\\\\\" "//
    })
    public void commandWithInsufficientlyEscapedDoubleQuotedWord(
            String input) {
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);
    }

    @ParameterizedTest
    @DisplayName("escaped single-quoted-word does not get built")
    @CsvSource({ //
            "echo \\'blabla this\\' ", //
            "echo \\'bla `ls` forever\\'", //
            "echo \\\\\\'blabla this\\\\\\' "//
    })
    public void commandWithEscapedSingleQuotedWord(
            String input) {
        assertAbsenceOfToken("SINGLE_QUOTED_WORD", input);
    }

    @ParameterizedTest
    @DisplayName("single-quoted-word gets built despite escape-attempt")
    @CsvSource({ //
            "echo \\\\'blabla this\\\\' ", //
            "echo \\\\'bla `ls` forever\\\\'", //
            "echo \\\\\\\\'blabla this\\\\\\\\' "//
    })
    public void commandWithInsufficientlyEscapedSingleQuotedWord(
            String input) {
        assertExistenceOfToken("SINGLE_QUOTED_WORD", input);
    }

    @ParameterizedTest
    @DisplayName("simple command employing keywords"
            + " and valid dollar operator gets built into ast")
    @CsvSource({ //
            "echo \"do \\\" ${this} \\\" \\\" forever\", 5", //
            "echo \"will do ${this} forever\", 3", //
            "echo \"will do ${th#is} forever\", 3", //
            "echo \"will ! do ${th##is} forever?\", 4", //
            "echo \"${th##$omg}\", 0", //
            "echo \"${th##${omg:-lol}}\", 0",
            "echo \"will '! do ${th:-is} for' ever?\", 5", //
            "echo \"will ! do ${th+is} forever?\", 4", //
            "echo \"do+* $(${lol})\", 1", //
    })
    public void echoWithKeywordsAsArgumentsAndADollarOperator(
            String input, int countOfWhiteSpace) {
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", input);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", input);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", input);
        assertCorrectCountOfToken(countOfWhiteSpace,
                "WHITE_SPACE", input);

    }

    @Test
    @DisplayName("no tilde expansion in double quotes")
    public void noTildeExpansionInDQ() {
        String code = "echo \"~nnoorshams\" bla";
        assertAbsenceOfToken("TILDE_EXPANSION", code);
        assertCorrectCountOfToken(1, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
    }

    @Test
    @DisplayName("parameter substitution in command substitution is possible")
    public void paramSubInCommandSub() {
        String toParse = "echo $(${lol})";
        assertExistenceOfValue("lol", toParse);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                toParse);
        assertExistenceOfToken("BRACED_PARAMETER", toParse);

    }

    @Test
    @DisplayName("double-quoted string retains its whitespace")
    public void retainWhiteSpaceInDQ() {
        String toParse = "echo \"catch    me if1+ you can\"";
        assertCorrectCountOfToken(1, "CMD_SUFFIX", toParse);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD",
                toParse);
        assertCorrectCountOfToken(4, "WHITE_SPACE",
                toParse);
    }

    @Test
    @DisplayName("multiple suffixes get distinguished properly")
    public void parseMultipleSuffixes() {
        String toParse = "echo catch me if1+you can";
        assertExistenceOfToken("SIMPLE_COMMAND", toParse);
        assertCorrectCountOfToken(4, "CMD_SUFFIX", toParse);
    }

    @Test
    @DisplayName("subtraction operator gets included into tree")
    public void parseTheseCommands() {
        String toParse = "echo -bla -blub; echo -navid xd";
        assertCorrectCountOfToken(3, "SUB", toParse);
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                toParse);
        assertCorrectCountOfToken(4, "CMD_SUFFIX", toParse);

    }

    @Test
    @DisplayName("subtraction operator gets included into tree")
    public void parseThis() {
        String toParse = "echo -s double-quote";
        assertCorrectCountOfToken(2, "SUB", toParse);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", toParse);
    }

    @Test
    @DisplayName("BACKTICK_SUBSTITUTION does not get built when in single quotes")
    public void backtickGetsIgnored() {
        String toParse = "echo '`ls a`'";
        assertAbsenceOfToken("BACKTICK_SUBSTITUTION",
                toParse);
        assertExistenceOfToken("SINGLE_QUOTED_WORD",
                toParse);
        assertExistenceOfToken("CMD_SUFFIX", toParse);

    }

    @Test
    @DisplayName("command substitution does not get parsed when escaped")
    public void dollarGetsIgnored() {
        String toParse = "echo \\${x}";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", toParse);
        assertExistenceOfToken("CMD_SUFFIX", toParse);
        assertExistenceOfToken("ESCAPED_DOLLAR", toParse);
    }

    @Test
    @DisplayName("command substitution get parsed, because escapes cancel each other")
    public void dollarDoesNotGetIgnored() {
        String toParse = "echo \\\\${x}";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", toParse);
    }

    @Test
    @DisplayName("command substitution does not get parsed when escaped")
    public void dollarGetsIgnoredAgain() {
        String toParse = "echo \\\\\\${x}";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", toParse);
        assertExistenceOfToken("CMD_SUFFIX", toParse);
        assertExistenceOfToken("ESCAPED_ESCAPE", toParse);
        assertExistenceOfToken("ESCAPED_DOLLAR", toParse);
    }

    @Test
    @DisplayName("the space between opening brace leads to erroneous build.")
    public void badSubstitution() {
        String code = "echo \\${ x}";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
        // assertExistenceOfToken("CMD_SUFFIX",
        // code);
    }

    @Test
    @DisplayName("why did I include this test case?")
    public void testingEndMatcherBlaBlaBla() {
        String toParse = "echo \"YOYOAYOYO LET! ${x}\"";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", toParse);
        assertExistenceOfToken("CMD_SUFFIX", toParse);
    }

    @Test
    @DisplayName("parsing a bunch of keywords as suffixes")
    public void testingPostParsing() {
        String toParse = "echo do do do do";
        assertCorrectCountOfToken(4, "CMD_SUFFIX", toParse);
    }

    @ParameterizedTest(
            name = "while {0} do ... done is accepted")
    @DisplayName("while")
    @CsvSource({"(echo a;echo b)", "true;", //
            "false;"})
    public void section294WhileDo(String command) {
        String code = "while " + command
                + " do\n echo '.' \ndone";
        assertExistenceOfToken("WHILE_CLAUSE", code);
        assertExistenceOfToken("DO", code);
        assertExistenceOfToken("SINGLE_QUOTED_WORD", code);
        assertExistenceOfToken("DONE", code);
        if (command.equals("(echo a;echo b)")) {
            assertExistenceOfToken("COMPOUND_LIST", code);
        }

    }

    @Test
    @DisplayName("if-then-clause employing a complex boolean-clause gets parsed")
    public void complexIfThenElse() {
        String code = "if test -n \"$opt_u\" && test \"$opt_u\" || $uid; then\r\n"
                + "continue\r\n" + "fi";
        assertExistenceOfToken("IF", code);
        assertCorrectCountOfToken(1, "AND", code);
        assertExistenceOfToken("OR", code);
        assertExistenceOfToken("SEMICOLON", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("FI", code);
        assertCorrectCountOfToken(3, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(3, "CMD_SUFFIX", code);

    }

    @Test
    @DisplayName("if-then-clause employing an assignment-word gets parsed")
    public void ifThenElseWithAssignmentWord() {
        String code = "if $opt_a; then\r\n found=true \n fi";
        assertExistenceOfToken("IF", code);
        assertExistenceOfToken("SEMICOLON", code);
        assertExistenceOfToken("THEN", code);
        assertExistenceOfToken("FI", code);
        assertCorrectCountOfToken(1, "DOLLAR_CONSTRUCT",
                code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
    }

    /*
     * for dir in $PATH; do if test -x
     * "$dir"/ranlib; then
     * ranlib="$dir"/ranlib break fi done
     */
    @Test
    @DisplayName("a more complex seeming for-loop "
            + "containing an if then gets built properly")
    public void t1() {
        String code = "for dir in $PATH; do\r\n"
                + "if test -x \"$dir\"/ranlib; then\r\n"
                + "ranlib=\"$dir\"/ranlib\r\n" + "break\r\n"
                + "fi\r\n" + "done";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertCorrectCountOfToken(1, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(3, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);

    }

    @Test
    @DisplayName("the simple command of t1 separated and"
            + " checked if adequate number of suffixes get created")
    public void t2() {
        String code = "test -x \"$dir\"/ranlib";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
    }

    /*
     * func_select_var=$1 shift case $1 in
     * in) shift;; *) echo >&2
     * "func_select: usage: func_select var in ..."
     * ; return 1;; esac
     *
     */
    @Test
    @DisplayName("complex case and a couple of assignments")
    public void t3() {
        String code = "func_select_var=$1\r\n" + "shift\r\n"
                + "case $1 in\r\n" + "in) shift;;\r\n"
                + "*) echo >&2 \"func_select: usage: func_select var in ...\"; return 1;;\r\n"
                + "esac";
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        assertExistenceOfValue(">&", code);
        // the double-quoted-string and the
        // digit 1
        assertCorrectCountOfToken(3, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(5, "WHITE_SPACE", code);
        assertCorrectCountOfToken(2, "CASE_ITEM", code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
    }

    /*
     * func_select_args=0 case $1 in
     * [!_a-zA-Z]* | *[!_a-zA-Z0-9]* ) echo
     * >&2
     * "func_select: '$1' is not a valid variable name."
     * return 1 ;; esac func_select_var=$1
     * shift
     *
     */
    @Test
    @DisplayName("case employing regex patterns, ")
    public void t4() {
        String code = "func_select_args=0\r\n"
                + "case $1 in\r\n"
                + "[!_a-zA-Z]|[!_a-zA-Z])\r\n"
                + "echo >&2 \"func_select: '$1' is not a valid variable name.\"\r\n"
                + "return 1\r\n" + ";;\r\n" + "esac\r\n"
                + "func_select_var=$1\r\n" + "shift";
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        assertCorrectCountOfToken(3, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(1, "PATTERN", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("GREATER_AND", code);
        assertCorrectCountOfToken(2, "ASSIGNMENT_WORD",
                code);
        assertAbsenceOfToken("SINGLE_QUOTED_WORD", code);
        assertCorrectCountOfToken(2,
                "DISABLED_SPECIAL_CHARACTER_DQ", code);
        // '$1' is interpolated regardless of
        // being surrounded by single quotes
        assertCorrectCountOfToken(3, "DOLLAR_CONSTRUCT",
                code);

    }

    /*
     * for func_select_arg do
     * func_select_args=`$func_select_args +
     * 1` eval
     * func_select_a_$func_select_args=\
     * $func_select_arg done REPLY=""
     */
    @Test
    @DisplayName("for-loop using command-substitution, "
            + "weird assignment-word, empty double-quoted-string")
    public void t5() {
        String code = "for func_select_arg \r\n" //
                + "do\r\n" //
                + "func_select_args=`$(($func_select_args + 1))`\r\n" //
                + "eval func_select_a_$func_select_args=\\$func_select_arg\r\n" //
                + "done\r\n" //
                + "REPLY=\"\"";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertCorrectCountOfToken(3, "DOLLAR_CONSTRUCT",
                code);
        assertExistenceOfToken("BACKTICK_SUBSTITUTION",
                code);
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("ESCAPED_SPECIAL_CHARACTER",
                code);

    }

    /*
     * if read REPLY; then if test -n
     * "${REPLY}"; then case $REPLY in 0* |
     * *[!0-9]* ) eval $func_select_var= ;;
     * ) if test "$REPLY" -ge 1 && test
     * "$REPLY" -le $func_select_args; then
     * eval $func_select_var=\
     * $func_select_a_$REPLY else eval
     * $func_select_var= fi ;; esac return 0
     * fi else eval $func_select_var= return
     * 1 fi $func_select_var=\
     * $func_select_a_$REPLY
     *
     */
    @Test
    @DisplayName("Giant if-case of doom, involving a case-clause and else-part")
    public void t6() {
        String code = "if read REPLY; then\r\n"
                + "if test -n \"${REPLY}\"; then\r\n"
                + "case $REPLY in\r\n"
                + "0* | *[!0-9]* )\r\n"
                + "eval $func_select_var=\r\n" + ";;\r\n"
                + "*)\r\n"
                + "if test \"$REPLY\" -ge 1 && test \"$REPLY\" -le $func_select_args; then\r\n"
                + "eval $func_select_var=\\$func_select_a_$REPLY\r\n"
                + "else\r\n" + "eval $func_select_var=\r\n"
                + "fi\r\n" + ";;\r\n" + "esac\r\n"
                + "return 0\r\n" + "fi\r\n" + "else\r\n"
                + "eval $func_select_var=\r\n"
                + "return 1\r\n" + "fi \r\n"
                + "$func_select_var=\\$func_select_a_$REPLY\r\n";
        assertCorrectCountOfToken(3, "IF_CLAUSE", code);
        assertCorrectCountOfToken(3, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(2, "ELSE_PART", code);
        assertCorrectCountOfToken(11, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(15, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(12, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(2, "CASE_ITEM", code);
        assertCorrectCountOfToken(2,
                "ESCAPED_SPECIAL_CHARACTER", code);
        assertCorrectCountOfToken(2, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(2, "PATTERN", code);
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertExistenceOfToken("AND", code);
    }

    /*
     * func_script() { for i do ( echo
     * "#!/bin/sh"; cat $i ) > $i.out done }
     *
     */
    @Test
    @DisplayName("some function involving a subshell")
    public void t7() {
        String code = "func_script(){ \r\n " + "for i\r\n"
                + "do\r\n"
                + "( echo \"#!/bin/sh\"; cat $i ) > $i.out\r\n"
                + "done\r\n" + "}";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertCorrectCountOfToken(2, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(2, "CMD_SUFFIX", code);
        assertExistenceOfToken("IO_REDIRECT", code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);

    }

    /*
     * #!/bin/bash
     *
     * SP_HOME=/opt/sp-server
     * LOG=$SP_HOME/sp.log
     * PID=$SP_HOME/sp.pid
     *
     * cd $SP_HOME
     *
     * case "$1" in start) java -jar
     * sp-server.jar server config.yml
     * >sp.log & echo $! > $PID ;; status)
     * if echo -f "$PID"; then ps -ax | grep
     * "^ `cat $PID`" fi ;; restart) $0 stop
     * $0 start ;; stop) if echo -f "$PID";
     * then kill `cat $PID` rm -f "$PID" fi
     * ;; ) echo
     * "Usage: $0 {start|stop|restart|status}"
     * exit 1 ;; esac #
     *
     */
    @Test
    @DisplayName("case from schadprotokoll-application")
    public void t8() {
        String code = "#!/bin/bash\r\n"
                + "SP_HOME=/opt/sp-server\r\n"
                + "LOG=$SP_HOME/sp.log\r\n"
                + "PID=$SP_HOME/sp.pid\r\n" + "\r\n"
                + "cd $SP_HOME \r\n" + "case \"$1\" in\r\n"
                + "  start)\r\n"
                + "    java -jar sp-server.jar server config.yml >sp.log &\r\n"
                + "    echo $! > $PID\r\n" + "    ;;\r\n"
                + "  status)\r\n"
                + "    if echo -f \"$PID\"; then\r\n"
                + "        ps -ax | grep \"^ `cat $PID`\"\r\n"
                + "    fi\r\n" + "    ;;\r\n"
                + "  restart)\r\n" + "    $0 stop\r\n"
                + "    $0 start\r\n" + "    ;;\r\n"
                + "  stop)\r\n"
                + "    if  echo -f \"$PID\" ; then\r\n"
                + "        kill `cat $PID`\r\n"
                + "        rm -f \"$PID\"\r\n"
                + "    fi\r\n" + "    ;;\r\n" + "  *)\r\n"
                + "    echo  \"Usage: $0 {start|stop|restart|status}\"\r\n"
                + "    exit 1\r\n" + "    ;;\r\n"
                + "esac\r\n";
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        // ASSIGNMENT_WORDS are also simple
        // commands
        assertCorrectCountOfToken(18, "SIMPLE_COMMAND",
                code);
        /*
         * each command substitution itself
         * contains a CMD_SUFFIX as well. Also,
         * each redirection is being regarded as
         * a CMD_SUFFIX
         */
        assertCorrectCountOfToken(23, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(2, "IO_REDIRECT", code);
        assertCorrectCountOfToken(5, "CASE_ITEM", code);

    }

    /**
     * tries out an actual file containing a
     * deploy-script. Comments inside of the
     * script indicate parts which have been
     * varied in order to POSIXify it
     * (script was actually a bash-script).
     */
    @Test
    @DisplayName("deploySecondHalf.sh --> actual file")
    public void t9() {
        String expectedTopLevelTokens = "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,"
                + "SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,NEWLINE_LIST,"
                + "FUNCTION_DEFINITION,EOL,IF_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND";
        ShellScriptFile sh = FileImporter
                .getFile("file/deploySecondHalf.sh");
        assertCorrectOrderOfTopLevelToken(
                expectedTopLevelTokens, sh.getContent());
    }

    @Test
    @DisplayName("test sequence")
    public void t10() {
        String code = "if [ \"$#\" = 4 ]; then\n echo \"why\"\n fi";
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(1, "EQUALS", code);
        assertCorrectCountOfToken(1, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(1, "IF_CLAUSE", code);

        assertCorrectCountOfToken(1, "DOLLAR_CONSTRUCT",
                code);
    }

    @Test
    @DisplayName("part of deploySecondHalf.sh")
    public void t11() {
        String code = "if[ \"$#\" -eq 4 -o $1 = \"install\" ];\r\n"
                + "then\r\n" + "    environment=$2\r\n"
                + "    #tag of the version\r\n"
                + "    git_version_tag=$3\r\n"
                + "    #version of dockerimage of the service\r\n"
                + "    SERVICE_VERSION=$4\r\n"
                + "  if $1==\"install\";\r\n"
                + "    then\r\n"
                + "        deployment_template_yaml=deployment-template.yaml\r\n"
                + "        deployconfig_effective=deployment-config.yaml\r\n"
                + "fi\r\n" + "fi";
        assertCorrectCountOfToken(2, "IF_CLAUSE", code);
    }

    /*
     * for curr_deployment in $(oc create
     * --dry-run=true -f
     * deployment-config.yaml -o name | grep
     * "deploymentconfig"); do echo
     * "Rollout of $curr_deployment" oc
     * rollout latest $curr_deployment;
     * done;
     *
     */
    @Test
    @DisplayName("for-loop from deploySecondHalf.sh")
    public void t12() {
        String code = "for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep \"deploymentconfig\"); \r\n"
                + "        do\r\n"
                + "          echo \"Rollout of $curr_deployment\" \r\n"
                + "          oc rollout latest $curr_deployment;\r\n"
                + "        done;\r\n";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("PIPE_SEQUENCE", code);
        assertCorrectCountOfToken(11, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(3, "DOLLAR_CONSTRUCT",
                code);

    }

    @Test
    @DisplayName("lengthy command substitution employing a pipe")
    public void t13() {
        String code = "$(oc create --dry-run=true -f deployment-config.yaml -o name | grep \"deploymentconfig\");";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("PIPE_SEQUENCE", code);
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
        assertCorrectCountOfToken(7, "CMD_SUFFIX", code);

    }

    /*
     * if $1 == "install" then
     * deployment_template_yaml=deployment-
     * template.yaml
     * deployconfig_effective=deployment-
     * config.yaml
     * load_environment_specific_deployment_parameters
     * # load_public_deployment_parameters
     * oc_login oc_secret
     *
     * merge_effective_properties
     *
     * echo
     * "install on $environment in $OC_URL in project $OC_PROJECT "
     * echo
     * "input: $environment_file, $service_file"
     *
     * #only generate deployment config oc
     * process -f $deployment_template_yaml
     * --param-file=$effective_file >
     * $deployconfig_effective #oc process
     * -f $deployment_template_yaml
     * --param-file=$effective_file
     * --ignore-unknown-parameters>
     * $deployconfig_effective
     *
     * #add to list that gets deleted when
     * deployment is successful
     * cleanup_list+
     * =" $deployconfig_effective"
     *
     * #execute template and create new
     * would be: #oc process -f
     * $deployment_template_yaml
     * --param-file=$effective_file | oc
     * create -f -
     *
     * #Create or reconfigure deployment oc
     * apply -f $deployconfig_effective
     *
     * # cancel rollout before redeployment
     * - this is a best effort operation and
     * might not take effect immediately (oc
     * rollout cancel $(oc get -f
     * deployment-config.yaml -o name | grep
     * deploymentconfig) && sleep 20) ||
     * true
     *
     * #rollout all existing deployment
     * configs for curr_deployment in $(oc
     * create --dry-run=true -f
     * deployment-config.yaml -o name | grep
     * "deploymentconfig"); do echo
     * "Rollout of $curr_deployment" oc
     * rollout latest $curr_deployment;
     * done; echo "Initial Rollout Done!"
     *
     * #check success of rollout for
     * curr_deployment in $(oc create
     * --dry-run=true -f
     * deployment-config.yaml -o name | grep
     * "deploymentconfig"); do echo
     * "Verify Rollout $curr_deployment!"
     * oc_verify_deployment $curr_deployment
     * || oc_rollback_deployment done;
     *
     * #cleanup after successful deployment
     * rm ${cleanup_list}
     *
     * #only execute template, for debug
     * only #oc process -f
     * $deployment_template_yaml
     * --param-file=$effective_file
     *
     * fi
     *
     */
    @Test
    @DisplayName("if then else containing lots of comments")
    public void t14() {
        String code = "if $1 == \"install\"\r\n"
                + "    then\r\n"
                + "        deployment_template_yaml=deployment-template.yaml\r\n"
                + "        deployconfig_effective=deployment-config.yaml\r\n"
                + "        load_environment_specific_deployment_parameters\r\n"
                + "        # TODO TCK: Auskommentiert (siehe Kommentar der Funktion)\r\n"
                + "        # load_public_deployment_parameters\r\n"
                + "        oc_login\r\n"
                + "        oc_secret\r\n" + "\r\n"
                + "        merge_effective_properties\r\n"
                + "\r\n"
                + "        echo \"install on $environment in $OC_URL in project $OC_PROJECT \"\r\n"
                + "        echo \"input: $environment_file, $service_file\"\r\n"
                + "\r\n"
                + "        #only generate deployment config\r\n"
                + "        oc process -f $deployment_template_yaml --param-file=$effective_file > $deployconfig_effective\r\n"
                + "        #oc process -f $deployment_template_yaml --param-file=$effective_file --ignore-unknown-parameters> $deployconfig_effective\r\n"
                + "\r\n"
                + "        #add to list that gets deleted when deployment is successful\r\n"
                + "        cleanup_list=\" $deployconfig_effective\"\r\n"
                + "\r\n"
                + "        #execute template and create new would be:\r\n"
                + "        #oc process -f $deployment_template_yaml --param-file=$effective_file | oc create -f -\r\n"
                + "\r\n"
                + "        #Create or reconfigure deployment\r\n"
                + "        oc apply -f $deployconfig_effective \r\n"
                + "    \r\n"
                + "        # cancel rollout before redeployment - this is a best effort operation and might not take effect immediately \r\n"
                + "        (oc rollout cancel $(oc get -f deployment-config.yaml -o name | grep deploymentconfig) && sleep 20) || true \r\n"
                + "    \r\n"
                + "        #rollout all existing deployment configs\r\n"
                + "        for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep \"deploymentconfig\"); \r\n"
                + "        do\r\n"
                + "          echo \"Rollout of $curr_deployment\" \r\n"
                + "          oc rollout latest $curr_deployment;\r\n"
                + "        done;\r\n"
                + "        echo \"Initial Rollout Done!\"\r\n"
                + "\r\n"
                + "        #check success of rollout\r\n"
                + "        for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep \"deploymentconfig\");  \r\n"
                + "        do\r\n"
                + "          echo \"Verify Rollout $curr_deployment!\"\r\n"
                + "          oc_verify_deployment $curr_deployment || oc_rollback_deployment\r\n"
                + "        done;  \r\n" + "\r\n"
                + "        #cleanup after successful deployment\r\n"
                + "        rm ${cleanup_list}\r\n" + "\r\n"
                + "        #only execute template, for debug only\r\n"
                + "        #oc process -f $deployment_template_yaml --param-file=$effective_file\r\n"
                + "    \r\n" + "    fi";
        assertCorrectCountOfToken(2, "FOR_CLAUSE", code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(19, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(9, "DOUBLE_QUOTED_WORD",
                code);
    }

    @Test
    @DisplayName("and-or using command substitution")
    public void t15() {
        String code = "(oc rollout cancel $(oc get -f deployment-config.yaml -o name | grep deploymentconfig) && sleep 20) || true \r\n";
        assertExistenceOfToken("SUBSHELL", code);
        assertCorrectCountOfToken(5, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(10, "CMD_SUFFIX", code);
        assertCorrectCountOfToken(1, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(1, "PIPE", code);
        assertCorrectCountOfToken(1, "AND", code);
        assertCorrectCountOfToken(1, "OR", code);
    }

    @Test
    @DisplayName("minus between dollar operators in assignment-word")
    public void t16() {
        String code = "service=${PROJECT_SHORTCUT}-${PROJECT_SERVICENAME}";
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("SUB", code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
    }

    /*
     * setup_static_parameters(){ #service
     * service=${PROJECT_SHORTCUT}-${
     * PROJECT_SERVICENAME}
     *
     * #Konfiguration Repository parameters
     * #gitlab project id can be found in
     * gitlab on project settings
     * git_project_id=1550
     *
     * # Konfiguration Repository von dbb
     * (für Data-Lake-Zugriff benötigt)
     * git_project_id_public=1106 #url of
     * the gitlab api
     * git_api_url=https://git.tech.rz.db.de
     * /api/v4
     *
     * #if Branchname is not set it is
     * assumed that master is used if
     * "${BRANCH_NAME:-}" ; then echo
     * "Branch Name is $BRANCH_NAME"; else
     * BRANCH_NAME=master; fi #KURZ_BRANCH
     * is used to allow the deployment of
     * multiple Branches into one single OC
     * Namespace if $BRANCH_NAME == "master"
     * || $BRANCH_NAME == "develop" then
     * KURZ_BRANCH="" else #length 25 THIS
     * HAS BEEN POSIXED KURZ_BRANCH=$(echo
     * ${BRANCH_NAME}) #THIS HAS BEEN
     * POSIXED KURZ_BRANCH=$(echo
     * "${KURZ_BRANCH}") #extend
     * PROJECT_SHORTCUT with KURZ_BRANCH
     * PROJECT_SHORTCUT=$PROJECT_SHORTCUT-
     * $KURZ_BRANCH fi echo
     * "KURZ_BRANCH=$KURZ_BRANCH" echo
     * "PROJECT_SHORTCUT=$PROJECT_SHORTCUT"
     *
     * #change label for uninstall
     * LABEL_APP=$PROJECT_SHORTCUT-
     * $LABEL_APP echo
     * "LABEL_APP=$LABEL_APP"
     *
     * #effective properties filename
     * timestamp=$(date
     * +"%Y.%m.%d-%H.%M.%S")
     * effective_name=_effective.properties
     * effective_file=
     * $timestamp$effective_name
     *
     * echo $timestamp$effective_name echo
     * "working dir" echo `pwd` }
     *
     *
     */
    @Test
    @DisplayName("another part of deploySecondHalf.sh")
    public void t17() {
        String code = "    setup_static_parameters(){\r\n"
                + "        #service\r\n"
                + "        service=${PROJECT_SHORTCUT}-${PROJECT_SERVICENAME}\r\n"
                + "\r\n"
                + "        #Konfiguration Repository parameters\r\n"
                + "        #gitlab project id can be found in gitlab on project settings\r\n"
                + "        git_project_id=1550\r\n"
                + "        \r\n"
                + "        # Konfiguration Repository von dbb (für Data-Lake-Zugriff benötigt)\r\n"
                + "        git_project_id_public=1106\r\n"
                + "        #url of the gitlab api\r\n"
                + "        git_api_url=https://git.tech.rz.db.de/api/v4\r\n"
                + "\r\n"
                + "        #if Branchname is not set it is assumed that master is used\r\n"
                + "        if \"${BRANCH_NAME:-}\" ; then echo \"Branch Name is $BRANCH_NAME\"; else BRANCH_NAME=master;fi\r\n"
                + "        #KURZ_BRANCH is used to allow the deployment of multiple Branches into one single OC Namespace\r\n"
                + "        if $BRANCH_NAME == \"master\" || $BRANCH_NAME == \"develop\"\r\n"
                + "            then\r\n"
                + "                KURZ_BRANCH=\"\"\r\n"
                + "            else\r\n"
                + "                #length 25 THIS HAS BEEN POSIXED\r\n"
                + "                KURZ_BRANCH=$(echo ${BRANCH_NAME})\r\n"
                + "                #THIS HAS BEEN POSIXED\r\n"
                + "                KURZ_BRANCH=$(echo \"${KURZ_BRANCH}\")\r\n"
                + "                #extend PROJECT_SHORTCUT with KURZ_BRANCH\r\n"
                + "                PROJECT_SHORTCUT=$PROJECT_SHORTCUT-$KURZ_BRANCH\r\n"
                + "        fi\r\n"
                + "        echo \"KURZ_BRANCH=$KURZ_BRANCH\"\r\n"
                + "        echo \"PROJECT_SHORTCUT=$PROJECT_SHORTCUT\"\r\n"
                + "\r\n"
                + "        #change label for uninstall\r\n"
                + "        LABEL_APP=$PROJECT_SHORTCUT-$LABEL_APP\r\n"
                + "        echo \"LABEL_APP=$LABEL_APP\"\r\n"
                + "\r\n"
                + "        #effective properties filename\r\n"
                + "        timestamp=$(date +\"%Y.%m.%d-%H.%M.%S\")\r\n"
                + "        effective_name=_effective.properties\r\n"
                + "        effective_file=$timestamp$effective_name\r\n"
                + "\r\n"
                + "        echo $timestamp$effective_name\r\n"
                + "        echo \"working dir\"\r\n"
                + "        echo `pwd`\r\n" + "    }\r\n"
                + "";
        assertCorrectCountOfToken(27, "SIMPLE_COMMAND",
                code);
        assertCorrectCountOfToken(22, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(13, "ASSIGNMENT_WORD",
                code);
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("FUNCTION_DEFINITION", code);

    }

    @DisplayName("adjoined dollar parameters in assignment-word")
    @Test
    public void t18() {
        String code = "effective_file=$timestamp$effective_name\r\n";
        assertCorrectCountOfToken(1, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(2, "DOLLAR_CONSTRUCT",
                code);
    }

    @DisplayName("dollar-operator that is empty after parameter expansion operator ")
    @Test
    public void t19() {
        String code = "echo ${BRANCH_NAME:-}";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
    }

    @DisplayName("if-then-else using [ for testing")
    @Test
    public void t20() {
        String code = "if [ \"$space\" -ge \"$alertvalue\" ]; then\r\n"
                + "  echo \"At least one of my disks is nearly full!\" | mail -s \"daily diskcheck\" root\r\n"
                + "else\r\n"
                + "  echo \"Disk space normal\" | mail -s \"daily diskcheck\" root\r\n"
                + "fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken(
                "SINGLE_BRACKET_BINARY_TEST_OPERATOR",
                code);
        assertCorrectCountOfToken(6, "DOUBLE_QUOTED_WORD",
                code);
    }

    @DisplayName("if-then-else using [ for testing")
    @Test
    public void t21() {
        String code = "if [ \"${BRANCH_NAME:-}\" ]; then echo \"Branch Name is $BRANCH_NAME\"; else BRANCH_NAME=master;fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken("UNARY_TEST_OPERATION",
                code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
    }

    /*
     * basically t17, but straight from the
     * script and not modified.
     *
     */
    @DisplayName("Some function employing brackets in if-then-else")
    @Test
    public void t22() {
        String code = "setup_static_parameters(){\r\n"
                + "    #service\r\n"
                + "    service=${PROJECT_SHORTCUT}-${PROJECT_SERVICENAME}\r\n"
                + "\r\n"
                + "    #Konfiguration Repository parameters\r\n"
                + "    #gitlab project id can be found in gitlab on project settings\r\n"
                + "    git_project_id=1550\r\n" + "    \r\n"
                + "    # Konfiguration Repository von dbb (für Data-Lake-Zugriff benötigt)\r\n"
                + "    git_project_id_public=1106\r\n"
                + "    #url of the gitlab api\r\n"
                + "    git_api_url=https://git.tech.rz.db.de/api/v4\r\n"
                + "\r\n"
                + "    #if Branchname is not set it is assumed that master is used\r\n"
                + "    if [ \"${BRANCH_NAME:-}\" ]; then echo \"Branch Name is $BRANCH_NAME\"; else BRANCH_NAME=master;fi\r\n"
                + "    #KURZ_BRANCH is used to allow the deployment of multiple Branches into one single OC Namespace\r\n"
                + "    if [ $BRANCH_NAME == \"master\" ] || [ $BRANCH_NAME == \"develop\" ]\r\n"
                + "        then\r\n"
                + "            KURZ_BRANCH=\"\"\r\n"
                + "        else\r\n"
                + "            #length 25\r\n"
                + "            KURZ_BRANCH=$(echo ${BRANCH_NAME})\r\n"
                + "            #convert all to lowercase\r\n"
                + "            KURZ_BRANCH=$(echo \"${KURZ_BRANCH}\")\r\n"
                + "            #extend PROJECT_SHORTCUT with KURZ_BRANCH\r\n"
                + "            PROJECT_SHORTCUT=$PROJECT_SHORTCUT-$KURZ_BRANCH\r\n"
                + "    fi\r\n"
                + "    echo \"KURZ_BRANCH=$KURZ_BRANCH\"\r\n"
                + "    echo \"PROJECT_SHORTCUT=$PROJECT_SHORTCUT\"\r\n"
                + "\r\n"
                + "    #change label for uninstall\r\n"
                + "    LABEL_APP=$PROJECT_SHORTCUT-$LABEL_APP\r\n"
                + "    echo \"LABEL_APP=$LABEL_APP\"\r\n"
                + "\r\n"
                + "    #effective properties filename\r\n"
                + "    timestamp=$(date +\"%Y.%m.%d-%H.%M.%S\")\r\n"
                + "    effective_name=_effective.properties\r\n"
                + "    effective_file=$timestamp$effective_name\r\n"
                + "\r\n"
                + "    echo $timestamp$effective_name\r\n"
                + "    echo \"working dir\"\r\n"
                + "    echo `pwd`\r\n" + "}\r\n" + "";
        assertExistenceOfToken("FUNCTION_DEFINITION", code);
        assertCorrectCountOfToken(22, "DOLLAR_CONSTRUCT",
                code);
    }

    @DisplayName("whitespace after digit")
    @Test
    public void t23() {
        String code = "echo \"three whitespaces 3 \"";
        assertCorrectCountOfToken(3, "WHITE_SPACE", code);
    }

    @DisplayName("whitespace after integer")
    @Test
    public void t24() {
        String code = "echo \"three whitespaces 30 \"";
        assertCorrectCountOfToken(3, "WHITE_SPACE", code);
    }

    @DisplayName("two single bracket tests joined with an OR")
    @Test
    public void t25() {
        String code = "    if [ $BRANCH_NAME == \"master\" ] || [ $BRANCH_NAME == \"develop\" ] \r\n"
                + "        then\r\n"
                + "            KURZ_BRANCH=\"\"\r\n"
                + "        else\r\n"
                + "            #length 25\r\n"
                + "            KURZ_BRANCH=$(echo ${BRANCH_NAME})\r\n"
                + "            #convert all to lowercase\r\n"
                + "            KURZ_BRANCH=$(echo \"${KURZ_BRANCH}\")\r\n"
                + "            #extend PROJECT_SHORTCUT with KURZ_BRANCH\r\n"
                + "            PROJECT_SHORTCUT=$PROJECT_SHORTCUT-$KURZ_BRANCH\r\n"
                + "    fi\r\n"
                + "    echo \"KURZ_BRANCH=$KURZ_BRANCH\"\r\n";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(2, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(9, "DOLLAR_CONSTRUCT",
                code);

    }

    @DisplayName("single bracket test employing negation")
    @Test
    public void t26() {
        String code = "    if [ ! -x \"$JAVACMD\" ] ; then\r\n"
                + "        die \"ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME.\" \r\n"
                + "    fi\r\n" + "";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertExistenceOfToken(
                "SINGLE_BRACKET_NEGATED_EXPRESSION", code);
        assertExistenceOfToken("UNARY_TEST_OPERATION",
                code);

    }

    @Test
    @DisplayName("weird giant test case")
    public void t28() {
        String code = "case $i in\r\n" //
                + "        (0) set -- ;;\r\n"
                + "        (1) set -- \"$args0\" ;;\r\n"
                + "        (2) set -- \"$args0\" \"$args1\" ;;\r\n"
                + "        (3) set -- \"$args0\" \"$args1\" \"$args2\" ;;\r\n"
                + "        (4) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" ;;\r\n"
                + "        (5) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" ;;\r\n"
                + "        (6) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" ;;\r\n"
                + "        (7) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" ;;\r\n"
                + "        (8) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" \"$args7\" ;;\r\n"
                + "        (9) set -- \"$args0\" \"$args1\" \"$args2\" \"$args3\" \"$args4\" \"$args5\" \"$args6\" \"$args7\" \"$args8\" ;;\r\n"
                + "    esac";
        assertExistenceOfToken("CASE_CLAUSE", code);
        assertCorrectCountOfToken(46, "DOLLAR_CONSTRUCT",
                code);
        assertCorrectCountOfToken(10, "CASE_ITEM", code);
    }

    @Test
    public void t29() {
        String code = "        if [ $CHECK -ne 0 ] && [ $CHECK2 -eq 0 ] ; then    # Added a condition\r\n"
                + "            eval `echo args$i`=`cygpath --path --ignore --mixed \"$arg\"`\r\n"
                + "        else\r\n"
                + "            eval `echo args$i`=\"\\\"$arg\\\"\"\r\n"
                + "        fi";
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(2, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(3,
                "BACKTICK_SUBSTITUTION", code);
        assertCorrectCountOfToken(2, "DOUBLE_QUOTED_WORD",
                code);
    }

    @DisplayName("Pipeline inside of a backtick-substitution")
    @Test
    public void t31() {
        String code = "CHECK2=`echo \"$arg\"|egrep -c \"^-\"`\r\n";
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
        assertExistenceOfToken("BACKTICK_SUBSTITUTION",
                code);

    }

    @DisplayName("if-then-else with inline comment")
    @Test
    public void t32() {
        String code = "    for arg in \"$@\" ; do\r\n"
                + "        CHECK=`echo \"$arg\"|egrep -c \"$OURCYGPATTERN\" -`\r\n"
                + "        CHECK2=`echo \"$arg\"|egrep -c \"^-\"` #Comment  \r\n"
                + "\r\n"
                + "        if [ $CHECK -ne 0 ] && [ $CHECK2 -eq 0 ] ; then                    ### Added a condition\r\n"
                + "            eval `echo args$i`=`cygpath --path --ignore --mixed \"$arg\"`\r\n"
                + "        else\r\n"
                + "            eval `echo args$i`=\"\\\"$arg\\\"\"\r\n"
                + "        fi\r\n"
                + "        i=$((i+1))\r\n" + "    done";
        assertExistenceOfToken("FOR_CLAUSE", code);
        assertExistenceOfToken("IF_CLAUSE", code);
        assertCorrectCountOfToken(2, "SINGLE_BRACKET_TEST",
                code);
        assertCorrectCountOfToken(5,
                "BACKTICK_SUBSTITUTION", code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);

    }

    /**
     * tries out an actual file containing
     * some bashrc. Only works when t34 also
     * works.
     */
    @Test
    @DisplayName("some bashrc")
    public void t33() {
        ShellScriptFile sh = FileImporter
                .getFile("file/bashrc1.sh");
        String tokens = ""
                + "CASE_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,NEWLINE_LIST,"
                + "SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,"
                + "NEWLINE_LIST,SIMPLE_COMMAND,NEWLINE_LIST,AND_OR,NEWLINE_LIST,"
                + "IF_CLAUSE,NEWLINE_LIST,CASE_CLAUSE,NEWLINE_LIST,IF_CLAUSE,"
                + "NEWLINE_LIST,IF_CLAUSE,EOL,SIMPLE_COMMAND,NEWLINE_LIST,"
                + "CASE_CLAUSE,NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,"
                + "EOL,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND,"
                + "NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,"
                + "SIMPLE_COMMAND,NEWLINE_LIST,AND_OR,EOL,FUNCTION_DEFINITION,"
                + "NEWLINE_LIST,FUNCTION_DEFINITION,NEWLINE_LIST,SIMPLE_COMMAND,NEWLINE_LIST,"
                + "SIMPLE_COMMAND,NEWLINE_LIST,SIMPLE_COMMAND,EOL,SIMPLE_COMMAND,EOL,"
                + "SIMPLE_COMMAND,NEWLINE_LIST,IF_CLAUSE,NEWLINE_LIST,SIMPLE_COMMAND,"
                + "EOL,SIMPLE_COMMAND";
        assertOk(sh.getContent());
        assertCorrectOrderOfTopLevelToken(tokens,
                sh.getContent());
    }

    @Test
    @DisplayName("Some script found when googling 'devops scripts'")
    public void t35() {
        String code = FileImporter
                .getFile("file/someDevOpsScript.sh")
                .getContent();
        assertCorrectCountOfToken(2, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(2, "FUNCTION_DEFINITION",
                code);
        assertCorrectCountOfToken(21, "ASSIGNMENT_WORD",
                code);
        assertScriptHasBeenFullyParsed(code);
    }

    @Test
    public void t36() {
        String code = "while [ -h \"$PRG\" ] ; do\r\n"
                + "    ls=`ls -ld \"$PRG\"`\r\n"
                + "    if expr \"$link\" '/.*' > /dev/null; then\r\n"
                + "        PRG=\"$link\"\r\n"
                + "    else\r\n"
                + "        PRG=`dirname \"$PRG\"`\"/$link\"\r\n"
                + "    fi\r\n" + "done";
        // assertExistenceOfToken("WHILE_CLAUSE",
        // code);
        assertCorrectCountOfToken(2,
                "BACKTICK_SUBSTITUTION", code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(6, "DOUBLE_QUOTED_WORD",
                code);

    }

    @DisplayName("newline in double quoted string")
    @Test
    public void t37() {
        String code = "echo \"hi, what's \n going on here\"";
        assertExistenceOfToken("DOUBLE_QUOTED_WORD", code);
    }

    @Test
    @DisplayName("command substitution with multiple assignments - faciliated form")
    public void t34() {
        String code = "COMPREPLY=($(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm completion -- \"${COMP_WORDS[@]}\" \\\r\n"
                + "                           2>/dev/null)) || return $?\r\n";
        assertCorrectCountOfToken(4, "DOUBLE_QUOTED_WORD",
                code);
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);

    }

    @DisplayName("assignment word with linebreak prefixing a simple command")
    @Test
    public void t38() {
        String code = "hello=x \\\n echo";
        assertExistenceOfToken("CMD_WORD", code);
        assertExistenceOfToken("ASSIGNMENT_WORD", code);
    }

    @Test
    @DisplayName("command substitution with multiple assignments - no suffixes")
    public void t40() {
        String code = "$(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm)";
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(4, "DOLLAR_CONSTRUCT",
                code);

    }

    @Test
    @DisplayName("command substitution with multiple assignments - employing suffixes and array expansion")
    public void t41() {
        String code = "$(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm completion -- \"${COMP_WORDS[@]}\")";
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(5, "DOLLAR_CONSTRUCT",
                code);

    }

    @Test
    @DisplayName("command substitution with multiple assignments - employing redirect")
    public void t43() {
        String code = "$(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm completion -- \"${COMP_WORDS[@]}\" \\\r\n"
                + "                           2>/dev/null)";
        assertExistenceOfToken("PARENTHESIZED_PARAMETER",
                code);
        assertCorrectCountOfToken(3, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(5, "DOLLAR_CONSTRUCT",
                code);
        assertExistenceOfToken("IO_REDIRECT", code);

    }

    @Test
    @DisplayName("command substitution with multiple assignments - actual form")
    public void t44() {
        String code = "COMPREPLY=($(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm completion -- \"${COMP_WORDS[@]}\" \\\r\n"
                + "                           2>/dev/null)) || return $?\r\n";
        assertCorrectCountOfToken(4, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(6, "DOLLAR_CONSTRUCT",
                code);
        assertExistenceOfToken("SUBSHELL", code);
        assertCorrectCountOfToken(4, "SIMPLE_COMMAND",
                code);
    }

    @Test
    @DisplayName("command substitution with multiple assignments - actual form")
    public void t39() {
        String code = "    IFS=$'\\n' COMPREPLY=($(COMP_CWORD=\"$COMP_CWORD\" \\\r\n"
                + "                           COMP_LINE=\"$COMP_LINE\" \\\r\n"
                + "                           COMP_POINT=\"$COMP_POINT\" \\\r\n"
                + "                           npm completion -- \"${COMP_WORDS[@]}\" \\\r\n"
                + "                           2>/dev/null)) || return $?\r\n";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
    }

    @Test
    @DisplayName("dollar construct employing a single-quoted-word inside")
    public void t45() {
        String code = "IFS=$'\\n'";
        assertExistenceOfToken("DOLLAR_CONSTRUCT", code);
    }

    @Test
    @DisplayName("two assignments white space separated")
    public void t46() {
        String code = "IFS=$'\\n' COMREPLY=XO";
        assertCorrectCountOfToken(2, "ASSIGNMENT_WORD",
                code);
    }

    @Test
    @DisplayName("two assignments white space separated and simple command")
    public void t47() {
        String code = "IFS=$'\\n' COMREPLY=XO echo this is awesome ${LOL}";
        assertCorrectCountOfToken(2, "ASSIGNMENT_WORD",
                code);
        assertExistenceOfToken("CMD_WORD", code);
        assertCorrectCountOfToken(4, "CMD_SUFFIX", code);
    }

    @Test
    @DisplayName("closing brace from escaped dollar operator"
            + " gets incorporated as single suffix")
    public void t48() {
        String code = "echo \\${xo}";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", code);
        assertExistenceOfToken("BRACE_R", code, true);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);

    }

    @Test
    @DisplayName("escaped parenthesises used in suffix")
    public void t52() {
        String code = "echo \\$\\(xo\\)";
        assertAbsenceOfToken("DOLLAR_CONSTRUCT", code);
        assertCorrectCountOfToken(1, "CMD_SUFFIX", code);
    }

    @Test
    @DisplayName("incomplete command suffix indicated by lone single quote")
    public void t50() {
        String code = "echo '+\\\\'+++'";
        assertCorrectCountOfToken(1, "SINGLE_QUOTED_WORD",
                code);
        assertScriptHasNotBeenFullyParsed(code);
    }

    @Test
    @DisplayName("empty subshell in assignment")
    public void t51() {
        String code = "COMPREPLY=()\n";
        assertCorrectCountOfToken(1, "ASSIGNMENT_WORD",
                code);
        assertCorrectCountOfToken(1, "SUBSHELL", code);
        assertScriptHasBeenFullyParsed(code);
    }

}
