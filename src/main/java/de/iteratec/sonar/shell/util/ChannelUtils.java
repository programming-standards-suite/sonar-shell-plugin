package de.iteratec.sonar.shell.util;

import org.sonar.sslr.channel.CodeReader;

public class ChannelUtils {

	private ChannelUtils() {
		//utility class, instantiation is not required
	}

	/**
	 * counts preceding escape-characters of a quote. If the count of
	 * subsequent escape-chars before the quote appears is an even number, then
	 * the quote is unescaped, else it is escaped.
	 *
	 * @param code the codereader
	 * @return true if the count leaves a remainder of -1 when divided through 2
	 */

	public static boolean confirmUnescapedCharacter(CodeReader code) {
		boolean unescaped = false;
		int index = -1;
		int count = 0;
		// Assert that we are not at the very beginning of the buffered code,
		// (e.g. column = 0, line = 0) else code.charAt(-1) leads to null-pointer
		if (code.getColumnPosition() != 0 && code.getLinePosition() != 0) {
			while (code.charAt(index) == '\\') {
				count++;
				index--;
			}

			if (count % 2 == 0) {
				unescaped = true;
			}
		} else {
			return true;
		}
		return unescaped;
	}

}
