#!/bin/bash
#it is necessarry to have the following environment variables:  OC_TOKEN and OC_URL and git_personal_access_token
# uninstall command only works if all OC objects are tagged with app=domain-service-sitzplatz (that is stored in $service)
# variables used from service.properties: PROJECT_SHORTCUT, LABEL_APP
#assure that the service variable is maintained correctly

set -e -x
echo "deploy.sh: $(pwd)"
setup_script_environment
apply_deployment_properties_service
setup_static_parameters

setup_static_parameters(){
    #service
    service=${PROJECT_SHORTCUT}-${PROJECT_SERVICENAME}

    #Konfiguration Repository parameters
    #gitlab project id can be found in gitlab on project settings
    git_project_id=1550
	
	# Konfiguration Repository von dbb (für Data-Lake-Zugriff benötigt)
	git_project_id_public=1106
    #url of the gitlab api
    git_api_url=https://git.tech.rz.db.de/api/v4

    #if Branchname is not set it is assumed that master is used
    if "${BRANCH_NAME:-}" ; then echo "Branch Name is $BRANCH_NAME"; else BRANCH_NAME=master;fi
    #KURZ_BRANCH is used to allow the deployment of multiple Branches into one single OC Namespace
    if $BRANCH_NAME == "master" || $BRANCH_NAME == "develop"
        then
            KURZ_BRANCH=""
        else
            #length 25 THIS HAS BEEN POSIXED
            KURZ_BRANCH=$(echo ${BRANCH_NAME})
            #THIS HAS BEEN POSIXED
            KURZ_BRANCH=$(echo "${KURZ_BRANCH}")
            #extend PROJECT_SHORTCUT with KURZ_BRANCH
            PROJECT_SHORTCUT=$PROJECT_SHORTCUT-$KURZ_BRANCH
    fi
    echo "KURZ_BRANCH=$KURZ_BRANCH"
    echo "PROJECT_SHORTCUT=$PROJECT_SHORTCUT"

    #change label for uninstall
    LABEL_APP=$PROJECT_SHORTCUT-$LABEL_APP
    echo "LABEL_APP=$LABEL_APP"

    #effective properties filename
    timestamp=$(date +"%Y.%m.%d-%H.%M.%S")
    effective_name=_effective.properties
    effective_file=$timestamp$effective_name

    echo $timestamp$effective_name
    echo "working dir"
    echo `pwd`
}

# Process commands
if "$#" -eq 4 && $1 == "install"
then
	environment=$2
	#tag of the version
	git_version_tag=$3
	#version of dockerimage of the service
	SERVICE_VERSION=$4
	if $1 == "install"
	then
		deployment_template_yaml=deployment-template.yaml
		deployconfig_effective=deployment-config.yaml



        load_environment_specific_deployment_parameters
		# TODO TCK: Auskommentiert (siehe Kommentar der Funktion)
        # load_public_deployment_parameters
        oc_login
        oc_secret

        merge_effective_properties

        echo "install on $environment in $OC_URL in project $OC_PROJECT "
        echo "input: $environment_file, $service_file"

		#only generate deployment config
		oc process -f $deployment_template_yaml --param-file=$effective_file > $deployconfig_effective
		#oc process -f $deployment_template_yaml --param-file=$effective_file --ignore-unknown-parameters> $deployconfig_effective

		#add to list that gets deleted when deployment is successful
        cleanup_list+=" $deployconfig_effective"

		#execute template and create new would be:
		#oc process -f $deployment_template_yaml --param-file=$effective_file | oc create -f -

		#Create or reconfigure deployment
		oc apply -f $deployconfig_effective 
	
		# cancel rollout before redeployment - this is a best effort operation and might not take effect immediately 
		(oc rollout cancel $(oc get -f deployment-config.yaml -o name | grep deploymentconfig) && sleep 20) || true 
	
		#rollout all existing deployment configs
		for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep "deploymentconfig"); 
		do
		  echo "Rollout of $curr_deployment" 
		  oc rollout latest $curr_deployment;
		done;
		echo "Initial Rollout Done!"

		#check success of rollout
		for curr_deployment in $(oc create --dry-run=true -f deployment-config.yaml -o name | grep "deploymentconfig");  
		do
		  echo "Verify Rollout $curr_deployment!"
		  oc_verify_deployment $curr_deployment || oc_rollback_deployment
		done;  

        #cleanup after successful deployment
        rm ${cleanup_list}

		#only execute template, for debug only
		#oc process -f $deployment_template_yaml --param-file=$effective_file
	
	fi
elif "$#" -eq 3  &&   $1 == "uninstall" 
	then
		environment=$2
		git_version_tag=$3
        load_environment_specific_deployment_parameters
		echo "uninstall app=$LABEL_APP"
		oc_login
		oc delete all,pvc,secret,sa -l app=$LABEL_APP
else
	echo "Usage deploy.sh install <environment> <Configuration version> <Service version>"
	echo "sample test: ./deploy.sh install Test 0.3.20170919_114831_060a500_20170919_153221 0.3.20170919_114831_060a500_20170919_153221"
	echo "sample int: ./deploy.sh install Integration v1.2 0.3.20170913_162512_39315e8_20170914_093353"
	echo "Usage deploy.sh uninstall <environment> <Configuration version>"
	echo "sample uninstall: ./deploy.sh uninstall Integration 0.3.20170919_114831_060a500_20170919_153221"
	echo "commands [install,uninstall]"
	echo "environments [Test,Integration,Presentation]"
	echo "project : An existing oc project"
	exit 1
fi  


exit 0